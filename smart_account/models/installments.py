import random
import string
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from odoo.exceptions import UserError


class installmentsInherit(models.Model):
    _inherit = 'installment.installment'
    check_details = fields.Many2one('account.check.tracking',string='Check Details')
    @api.multi
    def convert_delivered(self):
        if self.total_due !=0.00:
            print("dsdf")
            broker_comm=self.env['insurance.setup'].search([('setup_key', '=', 'Broker Commission')],limit=1)
            # policy=self.env['policy.broker'].search([('std_id', '=', self.policy_number)],limit=1)
            # print(broker_comm)
            if broker_comm:
                print("dkklflflf")
                due_invoice = self.env['account.invoice'].create({
                    'type': 'out_invoice',
                    'partner_id': self.insurer.id,
                    'name': 'Due Commission  for ' + str(self.insurer.name),
                    'salesperson': self.salesperson.id,
                    # 'team_id': self.salesperson.sales_channel.id,
                    'date_due': self.delv_date_ins,
                    'date_invoice': self.delv_date_ins,
                    'installment_id':self.id,
                    # 'policy_id1': policy.id ,
                    'invoice_line_ids': [(0, 0, {
                        'name': str('Due Commission For  '+self.name),
                        'quantity': 1,
                        'price_unit': self.total_due,
                        'account_id': broker_comm.broker_account.id,
                    })],
                })
                due_invoice.action_invoice_open()


            self.is_have_invoice=True

        if self.staff_ids:
                for rec in self.staff_ids:
                  if rec.total != 0.00:
                      vendor_bill = self.env['account.invoice'].create({
                          'type': 'in_invoice',
                          'partner_id': rec.name.id,
                          'name': 'Due Commission  for ' + str(self.insurer.name),
                          # 'salesperson': self.salesperson.id,
                          # 'team_id': self.salesperson.sales_channel.id,
                          'date_due': self.delv_date_ins,
                          'date_invoice': self.delv_date_ins,
                          'installment_id': self.id,
                          # 'policy_id1': policy.id ,
                          'invoice_line_ids': [(0, 0, {
                              'name': str('Agent Commission For  ' + rec.name.name),
                              'quantity': 1,
                              'price_unit': rec.total,
                              'account_id': rec.name.property_receivable.id,
                          })],
                      })
                      vendor_bill.action_invoice_open()
                self.is_have_bill = True

        return super(installmentsInherit, self).convert_delivered()


    @api.multi
    @api.depends('payment_details_ids')
    def convert_cashier_out(self):
        records=[]
        for rec in self.payment_details_ids:

            if rec.net < 0.00:
                real_value = rec.net * -1
                print(real_value)
                object = (
                    0, 0, {'name': 'Discount Expenses',
                    'account_id': rec.accname1.id,
                    'debit': 0.0 ,
                    'credit': real_value,
                    'journal_id': 1,
                    'partner_id': self.discount_party.id,
                    'currency_id': self.currency_id.id,
                           })
                records.append(object)


            elif rec.net > 0.00:
                object1 = (
                    0, 0, {
                    'name': 'Discount Expenses',
                    'account_id': rec.accname1.id,
                    'debit': rec.net,
                    'credit': 0.0,
                    'journal_id': 1,
                    'partner_id': self.discount_party.id,
                    'currency_id': self.currency_id.id,
                })
                records.append(object1)

        print(records)
        self.is_have_JE = True
            # n=0

        move_vals = {
                'ref': self.policy_number,
                'date': self.date,
                'journal_id': 1,
                'line_ids': records,
                'installment_id': self.id,
            }
        print('sdddffgggdeee')

        self.move_id.create(move_vals)
        print('sdddffgggdeee')
        return super(installmentsInherit, self).convert_cashier_out()



    # @api.multi
    # def convert_staff_comm(self):
    #         self.state = 'commission_rec'
    #         self.commission_date = fields.Date.today()
    #         print(self.staff_ids)
    #         if self.staff_ids:
    #             for rec in self.staff_ids:
    #               if rec.total != 0.00:
    #                 move_line_1 = {
    #                     'name': 'Staff Commission',
    #                     'account_id': rec.name.property_account_receivable_id.id,
    #                     'debit': 0.0,
    #                     'credit': rec.total,
    #                     'journal_id': 1,
    #                     'partner_id': rec.name.id,
    #                     'currency_id': self.currency_id.id,
    #                 }
    #                 move_line_2 = {
    #                     'name': 'Staff Commission',
    #                     'account_id': self.lob.expense_account.id,
    #                     'debit': rec.total,
    #                     'credit': 0.0,
    #                     'journal_id': 1,
    #                     'partner_id': rec.name.id,
    #                     'currency_id': self.currency_id.id,
    #                 }
    #                 move_vals = {
    #                     'ref': self.policy_number,
    #                     'date': self.date,
    #                     'journal_id': 1,
    #                     'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
    #                     'installment_id': self.id,
    #                 }
    #                 self.move_id.create(move_vals)
    #
    #
    #             return super(installmentsInherit, self).convert_staff_comm()
    #         else:
    #             print("lalalal")


class installment_invoice(models.Model):
    _inherit = 'account.invoice'
    installment_id = fields.Many2one(comodel_name="installment.installment", string="", required=False, )

class transactionsJE(models.Model):
    _inherit = 'cashier.transaction'
    move_id = fields.One2many('account.move', 'transaction_id', string='Moves', readonly=True)
    @api.multi
    def confirm_transaction(self):
        if self.transaction_type == 'Cashier In' and self.issue_type == 'Issue':

            records = []
            for rec in self.je_ids:
                if rec.net < 0.00:
                    real_value = rec.net * -1
                    print(real_value)
                    object = (
                        0, 0, {'name': self.cashier_in_no,
                               'account_id': rec.account.id,
                               'debit': 0.0,
                               'credit': real_value,
                               'journal_id': 10,
                               'partner_id': self.customer.id,
                               'currency_id': self.currency_id.id,
                               })
                    records.append(object)


                elif rec.net > 0.00:
                    object1 = (
                        0, 0, {
                            'name': self.cashier_in_no,
                            'account_id': rec.account.id,
                            'debit': rec.net,
                            'credit': 0.0,
                            'journal_id': 10,
                            'partner_id': self.customer.id,
                            'currency_id': self.currency_id.id,
                        })
                    records.append(object1)

            print(records)

            # n=0
            move_vals = {
                'ref': self.cashier_in_no,
                'date': self.transaction_date,
                'journal_id': 10,
                'line_ids': records,
                'transaction_id': self.id,
            }
            print('sdddffgggdeee')

            self.move_id.create(move_vals)
        elif self.transaction_type == 'Cashier In' and self.issue_type == 'Refund':

            records = []
            for rec in self.je_ids:
                if rec.net < 0.00:
                    real_value = rec.net * -1
                    print(real_value)
                    object = (
                        0, 0, {'name': self.cashier_in_no,
                               'account_id': rec.account.id,
                               'debit': 0.0,
                               'credit': real_value,
                               'journal_id': 10,
                               'partner_id': self.insurer.id,
                               'currency_id': self.currency_id.id,
                               })
                    records.append(object)


                elif rec.net > 0.00:
                    object1 = (
                        0, 0, {
                            'name': self.cashier_in_no,
                            'account_id': rec.account.id,
                            'debit': rec.net,
                            'credit': 0.0,
                            'journal_id': 10,
                            'partner_id': self.insurer.id,
                            'currency_id': self.currency_id.id,
                        })
                    records.append(object1)

            print(records)

            # n=0
            move_vals = {
                'ref': self.cashier_in_no,
                'date': self.transaction_date,
                'journal_id': 10,
                'line_ids': records,
                'transaction_id': self.id,
            }
            print('sdddffgggdeee')

            self.move_id.create(move_vals)

        elif self.transaction_type == 'Cashier Out' and self.issue_type == 'Issue':
            records = []
            for rec in self.je_ids:

                    if rec.net < 0.00:
                        real_value = rec.net * -1
                        print(real_value)
                        object = (
                            0, 0, {'name': self.cashier_out_no,
                                   'account_id': rec.account.id,
                                   'debit': 0.0,
                                   'credit': real_value,
                                   'journal_id': 10,
                                   'partner_id': self.insurer.id,
                                   'currency_id': self.currency_id.id,
                                   })
                        records.append(object)


                    elif rec.net > 0.00:
                        object1 = (
                            0, 0, {
                                'name': self.cashier_out_no,
                                'account_id': rec.account.id,
                                'debit': rec.net,
                                'credit': 0.0,
                                'journal_id': 10,
                                'partner_id': self.insurer.id,
                                'currency_id': self.currency_id.id,
                            })
                        records.append(object1)

            print(records)

            # n=0
            move_vals = {
                'ref': self.cashier_out_no,
                'date': self.transaction_date,
                'journal_id': 10,
                'line_ids': records,
                'transaction_id': self.id,
            }
            print('sdddffgggdeee')

            self.move_id.create(move_vals)
        elif self.transaction_type == 'Cashier Out' and self.issue_type == 'Refund':
            records = []
            for rec in self.je_ids:

                    if rec.net < 0.00:
                        real_value = rec.net * -1
                        print(real_value)
                        object = (
                            0, 0, {'name': self.cashier_out_no,
                                   'account_id': rec.account.id,
                                   'debit': 0.0,
                                   'credit': real_value,
                                   'journal_id': 10,
                                   'partner_id': self.customer.id,
                                   'currency_id': self.currency_id.id,
                                   })
                        records.append(object)


                    elif rec.net > 0.00:
                        object1 = (
                            0, 0, {
                                'name': self.cashier_out_no,
                                'account_id': rec.account.id,
                                'debit': rec.net,
                                'credit': 0.0,
                                'journal_id': 10,
                                'partner_id': self.customer.id,
                                'currency_id': self.currency_id.id,
                            })
                        records.append(object1)

            print(records)

            # n=0
            move_vals = {
                'ref': self.cashier_out_no,
                'date': self.transaction_date,
                'journal_id': 10,
                'line_ids': records,
                'transaction_id': self.id,
            }
            print('sdddffgggdeee')

            self.move_id.create(move_vals)

        return super(transactionsJE, self).confirm_transaction()
