import random
import string
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from odoo.exceptions import UserError


class inhertInvoiceTax(models.Model):
    _name = 'account.check.tracking'

    name = fields.Char(string='Check No.')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('paid', 'Paid'),
        ('cancel', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False)
    account_no = fields.Char(string='Account Number')
    bank_id = fields.Many2one('res.bank', string='Bank')
    amount = fields.Float(string='Amount')
    payment_id = fields.Many2one('account.payment', string='Payment', readonly=True)
    invoices_ids = fields.Many2many(related='payment_id.invoice_ids', string='Invoices', readonly=True)
    amount = fields.Monetary(related='payment_id.amount', string='Amount', readonly=True)
    currency_id = fields.Many2one('res.currency', string='Currency', required=True,
                                  default=lambda self: self.env.user.company_id.currency_id)


class inhertAccountInvoice(models.Model):
    _inherit = 'account.invoice'

    check_id = fields.Many2one('account.check.tracking')
    receipt_No = fields.Char('Receipt Number', readonly=True,default=lambda self: self.env['ir.sequence'].next_by_code('receipt'))
    salesperson = fields.Many2one('res.partner', string='Salesperson', domain="[('agent','=',1)]", copy=True)

    @api.multi
    def print_recipet(self):
        return self.env.ref('smart_account.report_tax').report_action(self)


class inhertAccountPayment(models.AbstractModel):
    _inherit = 'account.abstract.payment'

    check_id = fields.Many2one('account.check.tracking', string='Check Details', context="{'default_payment_id': id}",
                               domain=[('payment_id', '=', id)])
    payment_method = fields.Selection([
        ('cash', 'Cash'),
        ('checks', 'Checks'),
    ], string='Payment Method', track_visibility='onchange', copy=False)
