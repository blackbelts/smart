import random
import string
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from odoo.exceptions import UserError


class inhertInvoiceTax(models.Model):
    _inherit = 'account.invoice.tax'

    partner_id = fields.Many2one(string='Partner', related='invoice_id.partner_id', store=True, readonly=True)


class inhertAccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'

    posting_date = fields.Date('Posting Date', compute='_compute_posting_date', index=True)

    @api.one
    def _compute_posting_date(self):
        year = datetime.strptime(self.depreciation_date, "%Y-%m-%d").year
        self.posting_date = str(year) + '-12-31'


class inhertAccountPayments(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def print_policy(self):
        return self.env.ref('smart_account.payment_report').report_action(self)
