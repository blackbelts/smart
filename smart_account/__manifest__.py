# -*- coding: utf-8 -*-
{
    'name': "Smart Accounting",
    'summary': """Accounting""",
    'description': """Insurance Broker System """,
    'author': "Black Belts Egypt",
    'website': "www.blackbelts-egypt.com",
    'category': 'Accounting',
    'version': '0.1',
    'license': 'AGPL-3',
    # any module necessary for this one to work correctly
    'depends': ['base','account','mail','smart_policy','hr'],

    # always loaded
    'data': [
        'security/security.xml',
        'reports/tax_deduction.xml',
        'reports/payment_report.xml',
        'reports/tax_ded.xml',
        'views/installments.xml',
        'views/check.xml',
        'views/invoice_line_vat.xml',
        'views/asset_emp.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
