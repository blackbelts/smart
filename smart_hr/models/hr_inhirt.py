import base64
import logging
import math
from datetime import timedelta
from datetime import datetime

# from numpy import record

from odoo import api, fields, models
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools import float_compare
from odoo.tools.translate import _
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)

HOURS_PER_DAY = 8


# class inhertAttendance(models.Model):
#     _inherit = 'hr.attendance'
#
#     work_hours = fields.Char(string='Work Hours', compute='_total_minutes')
#     id = fields.Char(string='ID')
#     attachment = fields.Binary(string='Excel File')
#
#     @api.multi
#     def import_excel(self):
#         if self.attachment:
#             wb = open_workbook(file_contents=base64.decodestring(self.attachment))
#             values = []
#             for s in wb.sheets():
#                 for row in range(1, s.nrows):
#                     col_value = []
#                     for col in range(s.ncols):
#                         value = (s.cell(row, col).value)
#                         try:
#                             value = str(int(value))
#                         except:
#                             pass
#                         col_value.append(value)
#                     values.append(col_value)
#
#             return values
#         else:
#             raise ValidationError('please import your Excel Sheet !')
#
#     @api.one
#     @api.depends('check_in', 'check_out')
#     def _total_minutes(self):
#         if self.check_in and self.check_out:
#             start_dt = fields.Datetime.from_string(self.check_in)
#             finish_dt = fields.Datetime.from_string(self.check_out)
#             difference = relativedelta(finish_dt, start_dt)
#             days = difference.days
#             hours = difference.hours
#             minutes = difference.minutes
#             seconds = 0
#             self.work_hours = str(days) + ":" + str(hours) + ":" + str(minutes) + ":" + str(seconds)
#
#
# class inhertHolidays(models.Model):
#     _inherit = 'hr.holidays'
#
#     @api.model
#     def create(self, values):
#         """ Override to avoid automatic logging of creation """
#         employee_id = values.get('employee_id', False)
#         if not self._check_state_access_right(values):
#             raise AccessError(
#                 _('You cannot set a leave request as \'%s\'. Contact a human resource manager.') % values.get('state'))
#         if not values.get('department_id'):
#             values.update({'department_id': self.env['hr.employee'].browse(employee_id).department_id.id})
#         holiday = super(inhertHolidays, self.with_context(mail_create_nolog=True, mail_create_nosubscribe=True)).create(
#             values)
#         holiday.add_follower(employee_id)
#         if 'employee_id' in values:
#             holiday._onchange_type()
#             holiday._onchange_employee_id()
#             holiday._onchange_date_from()
#             holiday._onchange_date_to()
#         return holiday
#
#     @api.multi
#     def write(self, values):
#         employee_id = values.get('employee_id', False)
#         if not self._check_state_access_right(values):
#             raise AccessError(
#                 _('You cannot set a leave request as \'%s\'. Contact a human resource manager.') % values.get('state'))
#         result = super(inhertHolidays, self).write(values)
#         self.add_follower(employee_id)
#         if 'employee_id' in values:
#             self._onchange_type()
#             self._onchange_employee_id()
#             self._onchange_date_from()
#             self._onchange_date_to()
#         return result

#
# class inhertEmployee(models.Model):
#     _inherit = 'hr.employee'
#
#     @api.multi
#     def compute_leaves_count(self):
#         return self._compute_leaves_count()


class Contract(models.Model):
    _inherit = 'hr.contract'
    salaryinfo = fields.One2many('contract.data', 'inverse_contract', string='Wage')
    social = fields.Float()

    # @api.one
    @api.onchange('salaryinfo')
    def current_salary(self):
        if self.salaryinfo:
            my_date = datetime.today().date()
            for rec in self.salaryinfo:
                from_date = datetime.strptime(rec.fromdate, '%Y-%m-%d').date()
                to_date = datetime.strptime(rec.todate, '%Y-%m-%d').date()
            if (from_date <= my_date) and (my_date <= to_date):
                if (rec.category == 'BASIC'):
                    self.wage = rec.salary
                elif (rec.category == 'SOCIAL_EMP_SHARE'):
                    self.social = rec.salary
                    print(self.social)
            else:
                pass


class ContractData(models.Model):
    _name = 'contract.data'
    fromdate = fields.Date(string='From')
    todate = fields.Date(string='To')
    category = fields.Selection(
        [('BASIC', 'BASIC'), ('SOCIAL_EMP_SHARE', 'SOCIAL_EMP_SHARE'), ('SOCIAL_COMP_SHARE', 'SOCIAL_COMP_SHARE')])
    salary = fields.Float(string="Amount")
    inverse_contract = fields.Many2one('hr.contract')


# class CreateBills(models.Model):
#     _inherit = 'hr.payslip'
#     invoice_ids = fields.One2many('account.invoice', 'inverse_id', string='Employee Bill')
#     net = fields.Float(string='Net Salary', compute='net_salary_fun')
#     # sal = fields.Float(string='Sal')
#     # sal2 = fields.Float(string='Sal2')
#
#     @api.multi
#     def compute_sheet(self):
#         for payslip in self:
#             for rec in payslip.contract_id.salaryinfo:
#                 if (rec.fromdate <= payslip.date_to) and (rec.todate >= payslip.date_to):
#                     if rec.category == 'BASIC':
#                         payslip.sal = rec.salary
#             for x in payslip.struct_id.rule_ids:
#                 if x.code == 'BASIC':
#                     x.amount_fix = payslip.sal
#             for rec in payslip.contract_id.salaryinfo:
#                 if (rec.fromdate <= payslip.date_to) and (rec.todate >= payslip.date_to):
#                     if rec.category == 'SOCIAL_EMP_SHARE':
#                         payslip.sal2 = rec.salary
#             for x in payslip.struct_id.rule_ids:
#                 if x.code == 'SOCIAL':
#                     x.amount_fix = payslip.sal2
#             number = payslip.number or self.env['ir.sequence'].next_by_code('salary.slip')
#             # delete old payslip lines
#             payslip.line_ids.unlink()
#             # set the list of contract for which the rules have to be applied
#             # if we don't give the contract, then the rules to apply should be for all current contracts of the employee
#             contract_ids = payslip.contract_id.ids or \
#                            self.get_contract(payslip.employee_id, payslip.date_from, payslip.date_to)
#             lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]
#             payslip.write({'line_ids': lines, 'number': number})
#         return True
#
#     @api.one
#     def net_salary_fun(self):
#         self.net = self.line_ids.search([('name', '=', 'Net Salary'), ('slip_id', '=', self.id)]).total
#
#     @api.multi
#     def create_invoices(self):
#         if self.state == 'done':
#             self.invoice_ids = [(0, 0, {
#                 'type': 'in_invoice',
#                 'partner_id': self.employee_id.user_id.partner_id.id,
#                 'contract': self.contract_id,
#                 'origin': self.number,
#                 'user_id': self.env.user.id,
#                 'name': 'Bill  for ' + str(self.employee_id.id),
#                 # 'date_due': self.delv_date_ins,
#                 'invoice_line_ids': [(0, 0, {
#                     'name': self.name,
#                     'quantity': 1,
#                     'price_unit': self.net,
#                     'account_id': self.employee_id.user_id.partner_id.property_account_receivable_id.id,
#                 })],
#             })]
#
#
#
#
# class InheritHrRule(models.Model):
#     _inherit = 'hr.salary.rule'
#     sal = fields.Float("Salary")
#
#
# class InheritInvoice(models.Model):
#     _inherit = 'account.invoice'
#     inverse_id = fields.Many2one('hr.payslip')
#     contract = fields.Char('Contract')
#
# # class CreateBatchBill(models.Model):
# #     _inherit = 'hr.payslip.run'
# #     invoice_id = fields.One2many('account.invoice', 'inverse_id', string='Employee Bill')
# #     net = fields.Float(string='Net Salary', compute='net_salary')
# #
# #     @api.one
# #     def net_salary(self):
# #         self.net = self.line_ids.search([('name', '=', 'Net Salary'), ('slip_id', '=', self.id)]).total
# #
# #     @api.multi
# #     def confirm_batch(self):
# #         if self.slip_ids:
# #             self.slip_ids.state = 'done'
# #
# #     @api.multi
# #     def create_batch_invoices(self):
# #         if self.state == 'done':
# #             self.invoice_id = [(0, 0, {
# #                 'type': 'in_invoice',
# #                 'partner_id': self.employee_id.user_id.partner_id.id,
# #                 'contract': self.contract_id,
# #                 'origin': self.number,
# #                 'user_id': self.env.user.id,
# #                 'name': 'Bill  for ' + str(self.employee_id.id),
# #                 # 'date_due': self.delv_date_ins,
# #                 'invoice_line_ids': [(0, 0, {
# #                     'name': self.name,
# #                     'quantity': 1,
# #                     'price_unit': self.net,
# #                     'account_id': self.employee_id.user_id.partner_id.property_account_receivable_id.id,
# #                 })],
# #                 # 'emp_salary': self.total,
# #             })]
class ExpensesInherit(models.Model):
    _inherit = 'hr.expense.sheet'

    journal_id = fields.Many2one('account.journal', string='Expense Journal',
                                 states={'done': [('readonly', True)], 'post': [('readonly', True)]},
                                 default=lambda self: self.env['account.journal'].search([('name', '=', 'Petty Cash ')]),
                                 help="The journal used when the expense is done.")
