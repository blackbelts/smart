from odoo import models, fields, api
from odoo.exceptions import ValidationError



class emp_docs(models.Model):
    _inherit = 'hr.employee'
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    employee_id = fields.Char(string="Employee ID", required=False, )
    @api.one
    def _compute_attachment_number(self):
        s = self.env['ir.attachment'].search(['&', ('res_id', '=', self.id),('res_model', '=', 'hr.employee')])
        self.attachment_number=len(s)
        # pass

    @api.multi
    def action_get_attachment_tree_view(self):
        print("ssss")
        action = self.env.ref('base.action_attachment').read()[0]
        action['context'] = {
            'default_res_model': self._name,
            'default_res_id': self.ids[0]
        }
        # action['search_view_id'] = (self.env.ref('ir.attachment').id,)
        action['domain'] = [ '&', ('res_model', '=', 'hr.employee'), ('res_id', '=', self.id)]
        return action
class contracts_docs(models.Model):
    _inherit = 'hr.contract'

    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')

    @api.one
    def _compute_attachment_number(self):
        s = self.env['ir.attachment'].search(['&', ('res_id', '=', self.id),('res_model', '=', 'hr.contract')])
        self.attachment_number = len(s)
        # pass

    @api.multi
    def action_get_attachment_tree_view(self):
        print("ssss")
        action = self.env.ref('base.action_attachment').read()[0]
        action['context'] = {
            'default_res_model': self._name,
            'default_res_id': self.ids[0]
        }
        # action['search_view_id'] = (self.env.ref('ir.attachment').id,)
        action['domain'] = ['&', ('res_model', '=', 'hr.contract'), ('res_id', '=', self.id)]
        return action