import base64

from xlrd import open_workbook

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class policyrisks(models.Model):
    _name = "policy.risk"
    _rec_name = 'risk_description'

    # def get_policy(self, cr, uid, view_id=None, view_type='form',
    #                     context=None, toolbar=False, submenu=False):
    #     if context is None:
    #         context = {}
    #     res = super(policyrisks, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type,
    #                                                          context=context, toolbar=toolbar, submenu=False)
    #     return res
    @api.multi
    def get_policy(self):
        print("skdjsjhfjkdshkhjdj5555555555555")
        return {
            'name': ('get policy'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'policy.broker',
            'view_id': [(self.env.ref('smart_policy.policy_form_view').id), 'form'],
            'type': 'ir.actions.act_window',
            'target': 'current',
            # 'domain': [('id', '=', self.policy_risk_id.id)],
            'res_id': self.policy_risk_id.id,

        }

    # @api.multi
    # def set_claim(self):
    #     print("skdjsjhfjkdshkhjdj5555555555555")
    #     return {
    #         'name': ('get policy'),
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'policy.broker',
    #         'view_id': [(self.env.ref('smart_policy.policy_form_view').id), 'form'],
    #         'type': 'ir.actions.act_window',
    #         'target': 'current',
    #         # 'domain': [('id', '=', self.policy_risk_id.id)],
    #         'res_id': self.policy_risk_id.id,
    #
    #     }

    @api.multi
    def create_claim(self):
        return {
            'name': ('Claim'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'insurance.claim',
            'view_id': [(self.env.ref('smart_claim.form_insurance_claim').id), 'form'],
            'type': 'ir.actions.act_window',
            'target': 'current',

            'context': {'default_policy_number': self.policy_risk_id.id,
                        'default_lob': self.policy_risk_id.line_of_bussines.id, 'default_chassis_no': self.chassis_no},
            # 'domain': [('policy_number', '=', self.policy_risk_id.id)]

        }

    @api.one
    @api.depends('policy_risk_id')
    def _compute_risk_descriptionn(self):
        if self.policy_risk_id:
            if self.test == "person":
                self.risk_description = (str(self.job.setup_id) if self.job.setup_id else " " + "_") + "   " + (
                    str(self.name) + " - " if self.name else " " + "_") + "   " + (
                                            str(self.DOB) + " - " if self.DOB else " " + "_") + "   " + "   " + (
                                            str(self.family_member) if self.family_member else " " + "_")

            if self.test == "vehicle":
                self.risk_description = (str(
                    self.car_tybe.setup_id) + ' - ' if self.car_tybe.setup_id else " " + "_") + "   " + (
                                            str(
                                                self.Man.setup_id) + " - " if self.Man.setup_id else " " + "_") + "  " + (
                                            str(
                                                self.model.setup_id) + " - " if self.model.setup_id else " " + "_") + "   " + (
                                            str(
                                                self.country_id.setup_id) + " - " if self.country_id.setup_id else " " + "_") + "   " + (
                                            str(
                                                self.year_of_made) + " - " if self.year_of_made else " " + "_") + "  " + "VCC: " + (
                                            str(self.motor_cc) if self.motor_cc else " " + "_") + "  " + "PN: " + (
                                            str(
                                                self.plate_no) + ' - ' if self.plate_no else " " + "_") + "  " + "CH: " + (
                                            str(
                                                self.chassis_no) + ' - ' if self.chassis_no else " " + "_") + "  " + "EN: " + (
                                            str(
                                                self.engine) + ' - ' if self.engine else " " + "_") + "   " + (
                                            str(
                                                self.license_expire_date) + " - " if self.license_expire_date else " " + "_") + "   " + "Type: " + (
                                            str(
                                                self.state_type) + " - " if self.state_type else " " + "_")

            if self.test == "cargo":
                self.risk_description = (str(
                    self.cargo_type.setup_id) + " - " if self.cargo_type.setup_id else " " + "_") + "  " + "FRM: " + (
                                            str(self.From) + " - " if self.From else " " + "_") + "   " + "TO: " + (
                                            str(self.To) + " - " if self.To else " " + "_")
            if self.test == "location":
                self.risk_description = (
                                            str(
                                                self.type.setup_id) if self.type.setup_id else " " + "_") + "  " + "ADD: " + (
                                            str(self.address) + " - " if self.address else " " + "_")

            if self.test == 'Group':
                self.risk_description = (str(
                    self.group_category) + " - " if self.group_category else " " + "_") + "   " + (
                                            str(self.group_count) + " - " if self.group_count else " " + "_") + (
                                            str(
                                                self.policy_risk_id.line_of_bussines.object) + " - " if self.policy_risk_id.line_of_bussines.object else " " + "_")
            if self.test == 'Project':
                self.risk_description = (str(
                    self.proj_name) + " - " if self.proj_name else " " + "_") + "   " + (
                                            str(self.machine_id) + " - " if self.machine_id else " " + "_")
        else:
            self.risk_description = "something is wrong"

    # premium
    sum_insured = fields.Float(string='Sum Insured')

    limitone = fields.Char('Limit in One')
    limittotal = fields.Char('Limit in Total')
    policy_number = fields.Char(related='policy_risk_id.std_id')

    old_id = fields.Integer()
    old_id_end = fields.Integer()
    policy_risk_id = fields.Many2one("policy.broker", copy=True)
    risks_crm = fields.Many2one("crm.lead", string='Risks')
    test = fields.Char(related="policy_risk_id.check_item")
    get_type = fields.Char(related="policy_risk_id.type_char")
    risk_description = fields.Char("Risk Description", compute="_compute_risk_descriptionn", store=True)
    insured_object_type = fields.Char(compute='_compute_insured')
    customer = fields.Many2one('res.partner', compute='get_policy_info')
    discount_party = fields.Many2one('res.partner', compute='get_policy_info')
    company = fields.Many2one('res.partner', compute='get_policy_info')
    product_policy = fields.Many2one('insurance.product', compute='get_policy_info')
    is_renewal = fields.Boolean(compute='get_policy_info')
    endorsement_no = fields.Char(compute='get_policy_info')
    total_sum_insured = fields.Float(compute='get_policy_info')
    start_date = fields.Date(compute='get_policy_info')
    end_date = fields.Date(compute='get_policy_info')
    gross_perimum = fields.Float(compute='get_policy_info')
    policy_status = fields.Text(compute='get_policy_info')
    source_of_business = fields.Many2one('res.partner', compute='get_policy_info')
    issuing = fields.Boolean(compute='get_policy_info', store=True)
    si_deff = fields.Float('SI +/-')
    active = fields.Boolean(string="Active", compute='get_policy_info')
    line_of_bussines = fields.Many2one('insurance.line.business', compute='get_policy_info')
    phone_number = fields.Char(compute='get_policy_info')
    t_permimum = fields.Float(string="Net Premium",compute='get_policy_info')

    @api.one
    def get_policy_info(self):
        x = self.env["policy.broker"].search(
            ['|', ('active', '=', True), ('active', '=', False), ('id', '=', self.policy_risk_id.id)])
        self.customer = x.customer.id
        self.discount_party = x.discount_party.id
        self.company = x.company.id
        self.product_policy = x.product_policy.id
        self.is_renewal = x.is_renewal
        self.endorsement_no = x.endorsement_no
        self.total_sum_insured = x.total_sum_insured
        self.start_date = x.start_date
        self.end_date = x.end_date
        self.gross_perimum = x.gross_perimum
        self.policy_status = x.policy_status
        self.source_of_business = x.source_of_business.id
        self.issuing = x.issuing
        self.active = x.active
        self.phone_number = x.phone_number
        self.t_permimum = x.t_permimum

    @api.one
    def _compute_insured(self):
        if self.test:
            self.insured_object_type = self.test

    # vechile_risk
    car_tybe = fields.Many2one('insurance.setup', string='Vehicle Type', domain="[('setup_key','=','vehicletype')]")
    Man = fields.Many2one('insurance.setup', string='Maker', domain="[('setup_key','=','man')]")
    model = fields.Many2one('insurance.setup', string='Model',
                            domain="[('setup_key','=','model'),('parent_id','=',Man)]")
    state_type = fields.Selection([('new', 'New'),
                                   ('used', 'Used')],
                                  string='Type', track_visibility='onchange')
    year_of_made = fields.Char("Year of Make")
    country_id = fields.Many2one('insurance.setup', string='Region', domain="[('setup_key','=','region')]")
    motor_cc = fields.Char("Motor cc")
    chassis_no = fields.Char("Chassis Number")
    plate_no = fields.Char("Plate Number")
    engine = fields.Char("Engine Number")
    add_info = fields.Text("Additional Info")
    license_expire_date = fields.Date('License Expiration Date')
    # person_risk
    name = fields.Char('Name', copy=True)
    DOB = fields.Date('Date Of Birth', copy=True)
    job = fields.Many2one('insurance.setup', string='Job Type', domain="[('setup_key','=','jobtype')]")
    family_member = fields.Selection([('principle', 'Principle'),
                                      ('spouse', 'Spouse'),
                                      ('kid', 'Kid'), ],
                                     'Family Member', track_visibility='onchange')

    # cargo_risk
    cargo_type = fields.Many2one('insurance.setup', string='Type of Goods', domain="[('setup_key','=','cargo')]")
    From = fields.Char('From')
    To = fields.Char('To')

    # location
    type = fields.Many2one('insurance.setup', string='Location Type', domain="[('setup_key','=','location')]")
    address = fields.Char('Address')

    # group group
    group_category = fields.Char('Category')
    group_count = fields.Integer('Count')
    # group_premium=fields.Float('')
    group_premium = fields.Float(string="", required=False, )
    rate=fields.Float(related='policy_risk_id.rate')
    # project_risk
    proj_name = fields.Char('Project Name')
    proj_start_date = fields.Date('Start Date')
    proj_end_date = fields.Date('End Date')
    proj_description = fields.Text('Project Description')
    machine_id = fields.Char('Machine ID')

    risk_groups_ids = fields.One2many('risk.groups', 'risk_id', string='Group Members')

    _sql_constraints = [
        ('risk_unique', 'unique(policy_risk_id,risk_description)', 'ID already exists!')]


class groupRisks(models.Model):
    _name = "risk.groups"

    name = fields.Char('Name')
    # lname = fields.Char('Last Name')
    dob = fields.Date(string="Date Of Birth", required=False, )
    member_id = fields.Char('Member ID')
    member_type = fields.Selection(related='risk_id.policy_risk_id.line_of_bussines.object')
    # member_payed = fields.Float('amount')
    card_num = fields.Char(string="Card Number", required=False, )
    employee_code_mic = fields.Text('Employee Code MIC')
    status = fields.Char('Status')
    relation = fields.Char('Relation')
    gender = fields.Char(string="Gender", required=False, )
    risk_id = fields.Many2one('policy.risk')
    policy_id = fields.Many2one("policy.broker")
    crm_id = fields.Many2one("crm.lead")
    group_name = fields.Char("Benefit Name")
    is_current = fields.Boolean(string="", required=False, default=True)
    # staff_number = fields.Integer(string="Staff Number", required=True, )
    end_date = fields.Char(string="", required=False, )
    head_family_name = fields.Char(string="Head Of Family Name", required=False, )
    head_family_id = fields.Integer(string="Head Family ID", required=False, )
    sum_insured = fields.Float(string="", required=False, )


class NewModulecleanrisk(models.Model):
    _name = 'data.clean.setup.risk'

    clean_option_maker = fields.Boolean('Clean Maker')
    clean_option_model = fields.Boolean('Clean Model')

    maker = fields.Many2one('insurance.setup', string='Maker', domain="[('setup_key','=','man')]")
    model = fields.Many2one('insurance.setup', string='Model', domain="[('setup_key','=','model')]")

    @api.multi
    def replace_setup_risk(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        if self.clean_option_model:
            for rec in self.env['policy.risk'].browse(active_ids):
                print(5555555555)
                if self.model:
                    rec.write({'model': self.model.id})
                    rec._compute_risk_descriptionn()
