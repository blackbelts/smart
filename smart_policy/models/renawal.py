from odoo import api, fields, models


class insurancePolicy(models.Model):
    _inherit = 'policy.broker'
    # renwal_check = fields.Boolean(string="", )
    last_policy = fields.Many2one(comodel_name="policy.broker", string="Last Policy Number", required=False, )

    phone_number = fields.Char(related='customer.phone')

    @api.multi
    def create_renewal(self):
        self.active = False
        view = self.env.ref('smart_policy.policy_form_view')

        risk = self.env["policy.risk"].search([('id', 'in', self.new_risk_ids.ids)])
        records_risk = []
        for rec in risk:
            object = (0, 0, {'risk_description': rec.risk_description,
                             'car_tybe': rec.car_tybe.id, 'motor_cc': rec.motor_cc, 'year_of_made': rec.year_of_made,
                             'model': rec.model.id, 'Man': rec.Man.id, 'chassis_no': rec.chassis_no,
                             'plate_no': rec.plate_no, 'engine': rec.engine,
                             'license_expire_date': rec.license_expire_date,
                             'name': rec.name, 'DOB': rec.DOB, 'job': rec.job.id,
                             'From': rec.From, 'To': rec.To, 'cargo_type': rec.cargo_type.id,
                             'address': rec.address, 'type': rec.type.id,
                             'group_category': rec.group_category, 'group_count': rec.group_count,
                             'sum_insured': rec.sum_insured, 'limitone': rec.limitone,
                             'limittotal': rec.limittotal, 'proj_name': rec.proj_name,
                             'machine_id': rec.machine_id,
                             'proj_start_date': rec.proj_start_date,
                             'proj_end_date': rec.proj_end_date, 'proj_description': rec.proj_description,

                             })
            records_risk.append(object)
        return {
            'name': ('Policy'),
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(view.id, 'form')],
            'res_model': 'policy.broker',
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {
                'default_last_policy': self.id,
                # 'default_renwal_check': True,
                # 'default_policy_number': self.std_id,
                'default_company': self.company.id,
                'default_ins_type': self.ins_type,
                'default_line_of_bussines': self.line_of_bussines.id,
                'default_line_related_name': self.line_related_name,

                'default_product_policy': self.product_policy.id,
                'default_insurance_type': self.insurance_type,
                'default_customer': self.customer.id,
                'default_branch': self.branch.id,
                'default_sales_person1': self.sales_person1.id,
                'default_currency_id': self.currency_id.id,
                'default_benefit': self.benefit,
                'default_term': self.term,
                'default_no_years': self.no_years,
                # 'default_gross_perimum': self.gross_perimum,
                'default_new_risk_ids': records_risk,
                'default_is_renewal': True,
                'default_discount_party': self.discount_party.id,
                'default_in_favour': self.in_favour.id,
                'default_source_of_business': self.source_of_business.id,
                'default_state_track': 'Renewal'

            }
        }
