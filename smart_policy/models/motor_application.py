import io
from datetime import timedelta, datetime
import xlsxwriter
import xlwt
from xlsxwriter.workbook import Workbook
import base64
import xlrd
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


class MotorApplication(models.Model):
    _name = "motor.application"

    number_policy = fields.Char(string="Policy Number", copy=True)
    company = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]")
    date1 = fields.Date(string="From", copy=True, default=datetime.today(), required=True)
    date2 = fields.Date(string=" To", default=datetime.today(), required=True)
    discount_party = fields.Many2one('res.partner', 'Discount Party',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     required=True, copy=True)

    @api.multi
    def generate_excel_issue(self):

        if self.company and self.discount_party:
            # active_ids = self.env["policy.broker"].search(
            x = (['|', ('active', '=', True), ('active', '=', False),
                  ('end_date', '>=', self.date1),
                  ('end_date', '<=', self.date2),
                  ('company', '=', self.company.id),
                  ('discount_party', '=', self.discount_party.id)])
        elif self.discount_party and not self.company:
            x = (
                ['|', ('active', '=', True), ('active', '=', False),
                 ('end_date', '>=', self.date1),
                 ('end_date', '<=', self.date2),
                 ('discount_party', '=', self.discount_party.id)])
        else:
            # active_ids = self.env["policy.broker"].search(
            x = (['|', ('active', '=', True), ('active', '=', False), ('end_date', '>=', self.date1),
                  ('end_date', '<=', self.date2)])
        # print(active_ids)
        return {
            'name': ('Motor Application'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'policy.broker',
            'view_id': self.env.ref('smart_policy.motor_application_tree').id,
            'target': 'current',
            'type': 'ir.actions.act_window',
            'domain': x

        }

    @api.multi
    def generate_renewal(self):
        if self.company and self.discount_party:
            dom = (['|', ('policy_risk_id.active', '=', True), ('policy_risk_id.active', '=', False),
                    ('policy_risk_id.end_date', '>=', self.date1),
                    ('policy_risk_id.end_date', '<=', self.date2),
                    ('policy_risk_id.company', '=', self.company.id),
                    ('policy_risk_id.discount_party', '=', self.discount_party.id)])
        elif self.discount_party and not self.company:
            dom = (['|', ('policy_risk_id.active', '=', True), ('policy_risk_id.active', '=', False),
                    ('policy_risk_id.end_date', '>=', self.date1),
                    ('policy_risk_id.end_date', '<=', self.date2),
                    ('policy_risk_id.discount_party', '=', self.discount_party.id)])
        # print(active_ids)
        return {
            'name': ('Motor Application'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'policy.risk',
            'view_id': self.env.ref('smart_policy.risk_motor_application_tree').id,
            'target': 'current',
            'type': 'ir.actions.act_window',
            'domain': dom

        }

    is_confirmed = fields.Boolean(string="", compute='is_confirm')
    my_file = fields.Binary(string="Get Your File")
    excel_sheet_name = fields.Char(string='Name', size=64)

    @api.one
    @api.depends('my_file')
    def is_confirm(self):
        if self.my_file:
            self.is_confirmed = True
        return True

    @api.multi
    def generate_motor_renewal_excel(self):
        if self.company and self.discount_party:
            active_ids = self.env["policy.risk"].search(
                ['|', ('policy_risk_id.active', '=', True), ('policy_risk_id.active', '=', False),
                 ('policy_risk_id.end_date', '>=', self.date1),
                 ('policy_risk_id.end_date', '<=', self.date2),
                 ('policy_risk_id.company', '=', self.company.id),
                 ('policy_risk_id.discount_party', '=', self.discount_party.id)])
        elif self.discount_party and not self.company:
            active_ids = self.env["policy.risk"].search(
                ['|', ('policy_risk_id.active', '=', True), ('policy_risk_id.active', '=', False),
                 ('policy_risk_id.end_date', '>=', self.date1),
                 ('policy_risk_id.end_date', '<=', self.date2),
                 ('policy_risk_id.discount_party', '=', self.discount_party.id)])
        # else:
        #     active_ids = self.env["policy.risk"].search(
        #         ['|', ('policy_risk_id.active', '=', True), ('policy_risk_id.active', '=', False),
        #          ('policy_risk_id.end_date', '>=', self.date1),
        #          ('policy_risk_id.end_date', '<=', self.date2)])
        print(active_ids)

        self.excel_sheet_name = 'report_task.xlsx'
        # s=str(self.path)+'/'+'installment.xlsx'
        # print(s)
        self.excel_sheet_name = 'Motor_Renewal.xlsx'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)

        worksheet = workbook.add_worksheet()
        # Widen the first column to make the text clearer.
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)
        worksheet.set_column('J:J', 15)
        worksheet.set_column('K:K', 15)
        worksheet.set_column('L:L', 15)
        worksheet.set_column('M:M', 15)

        worksheet.write('A1', 'Policy Number')
        worksheet.write('B1', 'Customer')
        worksheet.write('C1', 'Phone')
        worksheet.write('D1', 'Discount Party')
        worksheet.write('E1', 'Insurer')
        worksheet.write('F1', 'Effective To')
        worksheet.write('G1', 'Sum Insured')
        worksheet.write('H1', 'Net Premium')
        worksheet.write('I1', 'Gross Premium')
        worksheet.write('J1', 'Maker')
        worksheet.write('K1', 'Model')
        worksheet.write('L1', 'Year Of Made')
        worksheet.write('M1', 'Chassis Num')

        i = 2
        for record in active_ids:
            worksheet.write('A' + str(i), str(record.policy_number))
            worksheet.write('B' + str(i), str(record.policy_risk_id.customer.name))
            if record.policy_risk_id.phone_number:
                worksheet.write('C' + str(i), str(record.policy_risk_id.phone_number))
            else:
                worksheet.write('C' + str(i), "   ")
            worksheet.write('D' + str(i), str(record.policy_risk_id.discount_party.name))
            worksheet.write('E' + str(i), str(record.policy_risk_id.company.name))
            worksheet.write('F' + str(i), str(record.policy_risk_id.end_date))
            worksheet.write('G' + str(i), str(record.policy_risk_id.total_sum_insured))
            worksheet.write('H' + str(i), str(record.policy_risk_id.t_permimum))
            worksheet.write('I' + str(i), str(record.policy_risk_id.gross_perimum))
            if record.Man:
                worksheet.write('J' + str(i), str(record.Man.id))
            else:
                worksheet.write('J' + str(i), "   ")
            if record.model:
                worksheet.write('K' + str(i), str(record.model.id))
            else:
                worksheet.write('K' + str(i), "   ")
            if record.year_of_made:
                worksheet.write('L' + str(i), str(record.year_of_made))
            else:
                worksheet.write('L' + str(i), "   ")
            if record.chassis_no:
                worksheet.write('C' + str(i), str(record.chassis_no))
            else:
                worksheet.write('C' + str(i), "   ")
            i += 1
        workbook.close()

        output.seek(0)
        self.write({'my_file': base64.encodestring(output.getvalue())})
        return {
            "type": "ir.actions.do_nothing",
        }

    # @api.multi
    # def generate_motor_renewal_excel(self):
    #     if self.company and self.discount_party:
    #         active_ids = self.env["policy.broker"].search(
    #             ['|', ('active', '=', True), ('active', '=', False),
    #              ('end_date', '>=', self.date1),
    #              ('end_date', '<=', self.date2),
    #              ('company', '=', self.company.id),
    #              ('discount_party', '=', self.discount_party.id)])
    #     elif self.company and not self.discount_party:
    #         active_ids = self.env["policy.broker"].search(
    #             ['|', ('active', '=', True), ('active', '=', False),
    #              ('end_date', '>=', self.date1),
    #              ('end_date', '<=', self.date2),
    #              ('company', '=', self.company.id)])
    #     else:
    #         active_ids = self.env["policy.broker"].search(
    #             ['|', ('active', '=', True), ('active', '=', False), ('end_date', '>=', self.date1),
    #              ('end_date', '<=', self.date2)])
    #     print(active_ids)
    #
    #     self.excel_sheet_name = 'report_task.xlsx'
    #     # s=str(self.path)+'/'+'installment.xlsx'
    #     # print(s)
    #     self.excel_sheet_name = 'Motor_Renewal.xlsx'
    #     output = io.BytesIO()
    #     workbook = xlsxwriter.Workbook(output)
    #
    #     worksheet = workbook.add_worksheet()
    #     # Widen the first column to make the text clearer.
    #     worksheet.set_column('A:A', 15)
    #     worksheet.set_column('B:B', 15)
    #     worksheet.set_column('C:C', 15)
    #     worksheet.set_column('D:D', 15)
    #     worksheet.set_column('E:E', 15)
    #     worksheet.set_column('F:F', 15)
    #     worksheet.set_column('G:G', 15)
    #
    #     worksheet.write('A1', 'Policy Number')
    #     worksheet.write('B1', 'Customer')
    #     worksheet.write('C1', 'Phone')
    #     worksheet.write('D1', 'Discount Party')
    #     worksheet.write('E1', 'Insurer')
    #     worksheet.write('F1', 'Effective To')
    #     worksheet.write('G1', 'New Rate')
    #     worksheet.write('H1', 'New Deductible')
    #
    #     i = 2
    #     for record in active_ids:
    #         worksheet.write('A' + str(i), str(record.std_id))
    #         worksheet.write('B' + str(i), str(record.customer.name))
    #         worksheet.write('C' + str(i), str(record.phone_number))
    #         worksheet.write('D' + str(i), str(record.discount_party.name))
    #         worksheet.write('E' + str(i), str(record.company.name))
    #         worksheet.write('F' + str(i), str(record.end_date))
    #         worksheet.write('G' + str(i), str(record.new_rate))
    #         if record.new_deductible:
    #             worksheet.write('H' + str(i), str(record.new_deductible))
    #         else:
    #             worksheet.write('H' + str(i), "   ")
    #
    #         i += 1
    #     workbook.close()
    #
    #     output.seek(0)
    #     self.write({'my_file': base64.encodestring(output.getvalue())})
    #     return {
    #         "type": "ir.actions.do_nothing",
    #     }


# class MotorexcelReport(models.Model):
#     _name = "motor.renewal.excel.report"
#     _rec_name = 'excel_motor_renewal_name'


class issent(models.Model):
    _name = 'motor.renewal.sent'

    @api.multi
    def sent(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        x = len(active_ids)
        for record in self.env['policy.broker'].browse(active_ids):
            record.write({'is_sent': True,
                          'sent_description': (str(record.discount_party.name)) + "  -  " + (
                              str(x)) + "  -  " + (str(datetime.today().strftime('%Y-%m-%d')))})
        return {'type': 'ir.actions.act_window_close'}


class isnotsent(models.Model):
    _name = 'motor.renewal.not.sent'

    @api.multi
    def not_sent(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['policy.broker'].browse(active_ids):
            record.write({'is_sent': False})
        return {'type': 'ir.actions.act_window_close'}
