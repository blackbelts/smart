from datetime import datetime, timedelta
from odoo import models, tools, fields, api
from openerp.exceptions import ValidationError


class kay_account_wizard(models.TransientModel):
    _name = 'key.account.report'
    date_from = fields.Date(string="From Date", required=True, default=datetime.today())
    to_date = fields.Date(string="To Date", required=True, default=datetime.today())
    currency_id = fields.Many2one("res.currency", "Currency", copy=True, required=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    line_of_bussines = fields.Many2one('insurance.line.business', string='Line of business',
                                       copy=True, required=True)

    # @api.multi
    # def generate_report(self):
    #     if self.date_from and self.to_date:
    #         active_ids = self.env["policy.broker"].search(
    #             ['|', ('active', '=', True), ('active', '=', False),
    #              ('start_date', '>=', self.date_from),
    #              ('start_date', '<=', self.to_date)]).ids
    #         print(active_ids)
    #     return self.env['policy.broker'].browse(active_ids)

    @api.multi
    def create_pivot_report(self):
        domain = ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date_from),
                  ('start_date', '<=', self.to_date),
                  ('currency_id', '=', self.currency_id.name),
                  ('line_of_bussines', '=', self.line_of_bussines.line_of_business),
                  ('state_track', 'in', ('New', 'Renewal')), ('policy_status', '=', 'approved')]
        print(domain)
        return {
            'name': ('Report'),
            'view_type': 'pivot',
            'view_mode': 'pivot',
            'res_model': 'policy.broker',
            'view_id': [(self.env.ref('smart_policy.policy_pivot').id), 'pivot'],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': domain,

        }


# @api.multi
# def print_key_account_perf(self, data):
#     data['form'] = self.read()
#     return self.env.ref('smart_policy.key_account_performance_report').report_action(self, data=data)
#


# class kay_account_wizard(models.TransientModel):
#     _name = 'key.account.report'
#     date_from = fields.Date(string="From Date", required=True, )
#     to_date = fields.Date(string="To Date", required=True, )
#
#     @api.multi
#     def print_key_account_perf(self, data):
#         data['form'] = self.read()
#         return self.env.ref('smart_policy.key_account_performance_report').report_action(self, data=data)
#
#
# class kay_account_wizard1(models.AbstractModel):
#     _name = 'report.smart_policy.je_report22'
#
#     @api.multi
#     def get_report_values(self, docids, data=None):
#         print("AAA")
#         all = []
#         s1 = ['|', ('active', '=', True), ('active', '=', False),
#               ('start_date', '>=',  data['form'][0]['date_from']),
#               ('start_date', '<=', data['form'][0]['to_date'])]
#         res = self.env['policy.broker'].search(s1)
#         all.append(res)
#         print(all)
#
#         if not data.get('form') or not self.env.context.get('active_model'):
#             raise ValidationError("Form content is missing, this report cannot be printed.")
#
#         self.model = self.env.context.get('active_model')
#         docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
#         return {
#             'doc_ids': self.ids,
#             'doc_model': self.model,
#             'data': all,
#             'docs': docs,
#         }
class PivotReportCRM(models.Model):
    _name = "policy.pivot.report"
    _table = 'policy_pivot_report'
    _auto = False

    discount_party = fields.Many2one('res.partner', 'Discount Party',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     copy=True, readonly=True)
    company = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]", readonly=True)
    std_id = fields.Char(readonly=True)
    state_track = fields.Char(readonly=True)
    gross_perimum = fields.Float(string="Final Gross Premium", copy=True, readonly=True)
    t_permimum = fields.Float(string="Net Premium", readonly=True)
    total_sum_insured = fields.Float(digits=(12, 2), string='Total Sum Insured', readonly=True)
    date_from = fields.Date(string="From Date", readonly=True, )
    to_date = fields.Date(string="To Date", readonly=True, )

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        query = """CREATE VIEW policy_pivot_report AS ( SELECT id,std_id,discount_party,company,state_track,
        t_permimum,gross_perimum,total_sum_insured,start_date from policy_broker group by id,discount_party,
        company HAVING (state_track) != ' Endorsement') """
        self.env.cr.execute(query)


class kay_account_month_wizard(models.TransientModel):
    _name = 'month.key.account.report'
    date_from = fields.Date(string="From Date", required=True, default=datetime.today())
    to_date = fields.Date(string="To Date", required=True, default=datetime.today())
    discount_party = fields.Many2one('res.partner', 'Discount Party',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     copy=True, required=True)
    currency_id = fields.Many2one("res.currency", "Currency", copy=True, required=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    line_of_bussines = fields.Many2one('insurance.line.business', string='Line of business',
                                       copy=True, required=True)

    @api.multi
    def create_month_pivot_report(self):
        domain = ['|', ('active', '=', True), ('active', '=', False), ('discount_party', '=', self.discount_party.id),
                  ('start_date', '>=', self.date_from),
                  ('start_date', '<=', self.to_date), ('currency_id', '=', self.currency_id.name),
                  ('line_of_bussines', '=', self.line_of_bussines.line_of_business),
                  ('state_track', 'in', ('New', 'Renewal')), ('policy_status', '=', 'approved')]
        tree_id = self.env.ref('smart_policy.policy_tree_view').id

        print(domain)
        return {
            'name': ('Report'),
            'view_type': 'pivot',
            'view_mode': ['pivot', 'tree'],
            'res_model': 'policy.broker',
            'views': [((self.env.ref('smart_policy.month_key_account_pivot').id), 'pivot'), (tree_id, 'tree')],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': domain,

        }


class PivotReportMonthKAccount(models.Model):
    _name = "month.key.account.pivot.report"
    _table = 'month_key_account_pivot_report'
    _auto = False

    discount_party = fields.Many2one('res.partner', 'Discount Party',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     copy=True, readonly=True)
    company = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]", readonly=True)
    std_id = fields.Char(readonly=True)
    state_track = fields.Char(readonly=True)
    gross_perimum = fields.Float(string="Final Gross Premium", copy=True, readonly=True)
    t_permimum = fields.Float(string="Net Premium", readonly=True)
    total_sum_insured = fields.Float(digits=(12, 2), string='Total Sum Insured', readonly=True)
    date_from = fields.Date(string="From Date", readonly=True, )
    to_date = fields.Date(string="To Date", readonly=True, )
    start_date = fields.Date(string="Effective From", copy=True, readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        query = """
                CREATE VIEW month_key_account_pivot_report AS (
                SELECT id,std_id,discount_party,company,state_track,t_permimum,gross_perimum,total_sum_insured,start_date from policy_broker group by id,discount_party,start_date)
             """
        self.env.cr.execute(query)


class kay_account_total_wizard(models.TransientModel):
    _name = 'key.account.total.report'
    date_from = fields.Date(string="From Date", required=True, default=datetime.today())
    to_date = fields.Date(string="To Date", required=True, default=datetime.today())
    currency_id = fields.Many2one("res.currency", "Currency", copy=True, required=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    line_of_bussines = fields.Many2one('insurance.line.business', string='Line of business',
                                       copy=True, required=True)

    @api.multi
    def create_pivot_report(self):
        domain = ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date_from),
                  ('start_date', '<=', self.to_date),
                  ('currency_id', '=', self.currency_id.name),
                  ('line_of_bussines', '=', self.line_of_bussines.line_of_business),
                  ('state_track', 'in', ('New', 'Renewal')), ('policy_status', '=', 'approved')]
        print(domain)
        return {
            'name': ('Report'),
            'view_type': 'pivot',
            'view_mode': 'pivot',
            'res_model': 'policy.broker',
            'view_id': [(self.env.ref('smart_policy.key_total_pivot').id), 'pivot'],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': domain,

        }


class PivotReportKeyAccount(models.Model):
    _name = "key.account.total.pivot.report"
    _table = 'key_account_total_pivot_report'
    _auto = False

    discount_party = fields.Many2one('res.partner', 'Discount Party',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     copy=True, readonly=True)
    company = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]", readonly=True)
    std_id = fields.Char(readonly=True)
    state_track = fields.Char(readonly=True)
    gross_perimum = fields.Float(string="Final Gross Premium", copy=True, readonly=True)
    t_permimum = fields.Float(string="Net Premium", readonly=True)
    total_sum_insured = fields.Float(digits=(12, 2), string='Total Sum Insured', readonly=True)
    date_from = fields.Date(string="From Date", readonly=True, )
    to_date = fields.Date(string="To Date", readonly=True, )

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        query = """CREATE VIEW key_account_total_pivot_report AS ( SELECT id,std_id,discount_party,company,
        state_track, t_permimum,gross_perimum,total_sum_insured,start_date from policy_broker group by id,
        discount_party HAVING (state_track) != ' Endorsement') """
        self.env.cr.execute(query)


class production_tracking_wizard(models.TransientModel):
    _name = 'production.tracking.report'
    date_from = fields.Date(string="From Date", required=True, default=datetime.today())
    to_date = fields.Date(string="To Date", required=True, default=datetime.today())
    # currency_id = fields.Many2one("res.currency", "Currency", copy=True, required=True,
    #                               default=lambda self: self.env.user.company_id.currency_id)
    # line_of_bussines = fields.Many2one('insurance.line.business', string='Line of business',
    #                                    copy=True, required=True)

    @api.multi
    def create_pivot_report(self):
        domain = ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date_from),
                  ('start_date', '<=', self.to_date),
                  ('state_track', 'in', ('New', 'Renewal')), ('policy_status', '=', 'approved')]
        print(domain)
        return {
            'name': ('Report'),
            'view_type': 'pivot',
            'view_mode': 'pivot',
            'res_model': 'policy.broker',
            'view_id': [(self.env.ref('smart_policy.production_tracking_pivot').id), 'pivot'],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': domain,

        }


class PivotReportProductionTracking(models.Model):
    _name = "production.tracking.pivot.report"
    _table = 'production_tracking_pivot_report'
    _auto = False

    # discount_party = fields.Many2one('res.partner', 'Discount Party',
    #                                  domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
    #                                  copy=True, readonly=True)
    # company = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]", readonly=True)
    std_id = fields.Char(readonly=True)
    insurance_type = fields.Selection([('Life', 'Life'),
                                       ('General', 'General'),
                                       ('Health', 'Health'), ],
                                      'Insurance Type', readonlt=True)
    line_of_bussines = fields.Many2one('insurance.line.business', string='Line of business', readonly=True)
    # state_track = fields.Char(readonly=True)
    gross_perimum = fields.Float(string="Final Gross Premium", copy=True, readonly=True)
    t_permimum = fields.Float(string="Net Premium", readonly=True)
    total_sum_insured = fields.Float(digits=(12, 2), string='Total Sum Insured', readonly=True)
    date_from = fields.Date(string="From Date", readonly=True, )
    to_date = fields.Date(string="To Date", readonly=True, )
    currency_id = fields.Many2one("res.currency", "Currency", readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        query = """CREATE VIEW production_tracking_pivot_report AS ( SELECT id,std_id,insurance_type,currency_id,
        line_of_bussines, t_permimum,gross_perimum,total_sum_insured from policy_broker group by id,
        insurance_type,line_of_bussines,currency_id) """
        self.env.cr.execute(query)
