from datetime import timedelta, datetime
import xlsxwriter
import xlwt
from xlsxwriter.workbook import Workbook
import base64
import xlrd
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


class PolicyBroker(models.Model):
    _name = "policy.broker"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    @api.multi
    def write(self, vals):

        if self.rella_installment_id and self.policy_status == "approved" and self.is_editable == False:
            self.rella_installment_id.unlink()

            if self.line_of_bussines.line_of_business == 'Individual Life':
                if self.term == "onetime":
                    self.rella_installment_id = [(0, 0, {
                        "policy_number": self.std_id,
                        "endorsement_no": self.endorsement_no,
                        "is_renewal": self.is_renewal,
                        "customer": self.customer.id,
                        "discount_party": self.discount_party,
                        "net_premium": self.t_permimum,
                        "gross_premium": self.gross_perimum,
                        'rate': self.rate,
                        "insurer": self.company.id,
                        "insurance_type": self.insurance_type,
                        "lob": self.line_of_bussines.id,
                        "ins_product": self.product_policy.id,
                        "date": self.start_date,
                        'start_date': self.start_date,
                        'end_date': self.end_date,
                        'currency_id': self.currency_id,
                        'basic_brokerage': self.basic_brokerage,
                        'complementary_comm': self.complementary_comm,
                        'transportation_comm': self.transportation_comm,
                        'tax': self.tax,
                        'sales_person': self.sales_person1.id,
                        'team_id': self.team_id.id,
                        # 'staff_ids': staff_ids,
                        'source_of_business': self.source_of_business

                    })]
                elif self.term == "year":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(years=1)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(int(self.no_years) - 1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(years=1)
                    self.rella_installment_id = numbers
                elif self.term == "quarter":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(months=3)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(3):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=3)
                    self.rella_installment_id = numbers
                elif self.term == "month":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(months=1)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'tax': self.tax / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(11):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'tax': self.tax / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=1)
                    self.rella_installment_id = numbers
                elif self.term == "semiannual":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(months=6)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'tax': self.tax / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'tax': self.tax / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=6)
                    self.rella_installment_id = numbers
            else:
                if self.term == "onetime":
                    self.rella_installment_id = [(0, 0, {
                        "policy_number": self.std_id,
                        "endorsement_no": self.endorsement_no,
                        "is_renewal": self.is_renewal,
                        "customer": self.customer.id,
                        "discount_party": self.discount_party,
                        "net_premium": self.t_permimum,
                        "gross_premium": self.gross_perimum,
                        'rate': self.rate,
                        "insurer": self.company.id,
                        "insurance_type": self.insurance_type,
                        "lob": self.line_of_bussines.id,
                        "ins_product": self.product_policy.id,
                        "date": self.start_date,
                        'start_date': self.start_date,
                        'end_date': self.end_date,
                        'currency_id': self.currency_id,
                        'basic_brokerage': self.basic_brokerage,
                        'complementary_comm': self.complementary_comm,
                        'transportation_comm': self.transportation_comm,
                        'tax': self.tax,
                        'sales_person': self.sales_person1.id,
                        'team_id': self.team_id.id,
                        # 'staff_ids': staff_ids,
                        'source_of_business': self.source_of_business

                    })]
                elif self.term == "year":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(years=1)
                    net = self.t_permimum + self.charges
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(int(self.no_years) - 1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(years=1)
                    self.rella_installment_id = numbers
                elif self.term == "quarter":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    net = self.t_permimum / 4
                    duration = relativedelta(months=3)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net + self.charges,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(3):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=3)
                    self.rella_installment_id = numbers
                elif self.term == "month":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    net = self.t_permimum / 12
                    duration = relativedelta(months=1)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net + self.charges,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'tax': self.tax / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(11):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=1)
                    self.rella_installment_id = numbers
                elif self.term == "semiannual":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    net = self.t_permimum / 2
                    duration = relativedelta(months=6)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net + self.charges,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=6)
                    self.rella_installment_id = numbers
        res = super(PolicyBroker, self).write(vals)  # res stores the result of the write function
        return res

    @api.one
    def unlink(self):
        if self.rella_installment_id:

            for x in self.rella_installment_id:
                if x.state != ('outstanding') or x.is_sent == True:
                    raise UserError(_(
                        'You cannot delete this policy which its installment is not outstanding or mark as sent'))
        return super(PolicyBroker, self).unlink()

    def send_mail_template_policy(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('smart_policy.policy_email_template')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'policy.broker',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    def send_mail_template_motor(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('smart_policy.motor_email_template')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'policy.broker',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.one
    def _compute_attachment_number(self):
        s = self.env['ir.attachment'].search(['&', ('res_id', '=', self.id), ('res_model', '=', 'policy.broker')])
        self.attachment_number = len(s)
        # pass

    @api.multi
    def action_get_attachment_tree_view(self):
        action = self.env.ref('base.action_attachment').read()[0]
        action['context'] = {
            'default_res_model': self._name,
            'default_res_id': self.ids[0]
        }
        # action['search_view_id'] = (self.env.ref('ir.attachment').id,)
        action['domain'] = ['&', ('res_model', '=', 'policy.broker'), ('res_id', '=', self.id)]
        return action

    @api.onchange('line_of_bussines')
    def _compute_comment_policy(self):
        self.check_item = self.line_of_bussines.object

    @api.multi
    def print_policy(self):
        return self.env.ref('smart_policy.policy_report').report_action(self)

    # @api.depends('t_permimum')
    # def rating_rule(self):
    #     print('sssddddsss')
    #     if self.total_sum_insured != 0:
    #         self.rate = (self.t_permimum / self.total_sum_insured) * 100

    @api.multi
    def print_motor_report(self):
        return self.env.ref('smart_policy.motor_report').report_action(self)

    @api.multi
    def create_new_renewal(self):
        self.active = False
        view = self.env.ref('smart_policy.policy_form_view')

        risk = self.env["policy.risk"].search([('id', 'in', self.new_risk_ids.ids)])
        records_risk = []
        for rec in risk:
            object = (0, 0, {'risk_description': rec.risk_description,
                             'car_tybe': rec.car_tybe.id, 'motor_cc': rec.motor_cc, 'year_of_made': rec.year_of_made,
                             'model': rec.model.id, 'Man': rec.Man.id, 'chassis_no': rec.chassis_no,
                             'plate_no': rec.plate_no, 'engine': rec.engine,
                             'name': rec.name, 'DOB': rec.DOB, 'job': rec.job.id,
                             'From': rec.From, 'To': rec.To, 'cargo_type': rec.cargo_type.id,
                             'address': rec.address, 'type': rec.type.id,
                             'group_category': rec.group_category, 'group_count': rec.group_count,
                             'sum_insured': rec.sum_insured, 'limitone': rec.limitone,
                             'limittotal': rec.limittotal, 'proj_name': rec.proj_name,
                             'machine_id': rec.machine_id,
                             'proj_start_date': rec.proj_start_date,
                             'proj_end_date': rec.proj_end_date, 'proj_description': rec.proj_description,

                             })
            records_risk.append(object)
        return {
            'name': ('Policy'),
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(view.id, 'form')],
            'res_model': 'policy.broker',
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {
                'default_last_policy': self.id,
                'default_company': self.company.id,
                'default_ins_type': self.ins_type,
                'default_line_of_bussines': self.line_of_bussines.id,
                'default_line_related_name': self.line_related_name,
                'default_product_policy': self.product_policy.id,
                'default_insurance_type': self.insurance_type,
                'default_customer': self.customer.id,
                'default_branch': self.branch.id,
                'default_sales_person1': self.sales_person1.id,
                'default_currency_id': self.currency_id.id,
                'default_benefit': self.benefit,
                'default_term': self.term,
                'default_no_years': self.no_years,
                'default_new_risk_ids': records_risk,
                'default_is_renewal': True,
                'default_discount_party': self.discount_party.id,
                'default_in_favour': self.in_favour.id,
                'default_rate': self.new_rate,
                'default_deductible': self.new_deductible,
                'default_source_of_business': self.source_of_business.id,
                'default_state_track': 'Renewal'

            }
        }

    @api.onchange('diff_sum_insured', 't_permimum')
    def rating_rule(self):
        if self.diff_sum_insured != 0:
            fmt = '%Y-%m-%d'
            d1 = datetime.strptime(self.end_date, fmt).date() + timedelta(days=1)
            d2 = datetime.strptime(self.start_date, fmt)
            daysDiff = relativedelta(d1, d2)
            print(daysDiff)
            years_months = daysDiff.years * 12

            print(years_months)
            remaining_per = (((daysDiff.months) + years_months) / (12))

            print(remaining_per)
            if remaining_per != 0:
                self.rate = (self.t_permimum / (self.diff_sum_insured * remaining_per)) * 100
                print(self.rate)
            else:
                # raise ValidationError('Wrong Dates')
                pass
        r = self.env['rating.setup'].search([('customer', '=', self.discount_party.id),
                                             ('line_of_bus', '=', self.line_of_bussines.id),
                                             ('insurer', '=', self.company.id), ])
        if self.check_item == 'vehicle':
            if r.rating_rule == 'SI':
                for rec in r.SI_data:
                    if rec.min <= self.total_sum_insured <= rec.max:
                        self.rate = rec.rate
                        print(self.rate)
            elif r.rating_rule == 'maker':
                for rec in r.maker_data:
                    if rec.maker == self.new_risk_ids.Man:
                        if self.new_risk_ids.model in rec.model:
                            self.rate = rec.rate
            else:
                pass

    # @api.one
    # def _set_rate(self):
    #     if self.rate != 0.00:
    #         return True
    #     else:
    #         return self.rating_rule

    # @api.onchange('end_date')
    @api.one
    def compute_date(self):
        # print(self.renewal_state)
        if self.end_date and self.start_date:
            self.alarmrenew = False

            # end_date = datetime.strptime(self.end_date, '%Y-%m-%d').date()
            # today = datetime.today()
            fmt = '%Y-%m-%d'
            d1 = datetime.strptime(self.end_date, fmt)
            d2 = datetime.strptime(self.today, fmt)
            daysDiff = str((d1 - d2).days)
            self.differnce1 = daysDiff
            if -45 <= self.differnce1 <= 90:
                self.renewal_state2 = True
                return True
                # if self.differnce <= 45:
                #     # self.alarmrenew2 = True
                # if self.differnce == -6:
                #     self.alarmrenew2 = True

            else:
                self.renewal_state2 = False
                return True

                # self.alarmrenew2 = False

        #         print(444444444444444)
        #     if date5 >= end_date >= date3:
        #         self.alarmrenew = True
        #     if self.today >= self.end_date:
        #         self.issuing = True
        print(self.differnce1)

        #
        # date3 = self.end_date.date() - self.today.date()
        # print(date3)
        # date4 = today + timedelta(days=90)
        # date5 = today + timedelta(days=45)
        # print(date3)
        # print(date4)
        # # self.end_date=date3
        # if self.today:
        #     if date4 >= end_date >= date3:
        #         self.renewal_state = True
        #         print(444444444444444)
        #     if date5 >= end_date >= date3:
        #         self.alarmrenew = True
        #     if self.today >= self.end_date:
        #         self.issuing = True

    # net_increase_decrease = fields.Float()
    # gross_increase_decrease = fields.Float()
    # net_after_dec_inc = fields.Float()
    # gross_after_dec_inc = fields.Float()
    @api.one
    def _set_date(self):
        # if self.renewal_state2 != :
        return True

    # else:
    #     return self.rating_rule

    # net_increase_decrease = fields.Float()
    # gross_increase_decrease = fields.Float()
    # net_after_dec_inc = fields.Float()
    # gross_after_dec_inc = fields.Float()

    bool = fields.Boolean()
    std_id = fields.Char(string="Policy Number", copy=True)
    app_date = fields.Date(string="Application Date", copy=True, default=datetime.today())
    issue_date = fields.Date(string="Issue Date", copy=True, default=datetime.today())
    start_date = fields.Date(string="Effective From", copy=True, default=datetime.today())
    end_date = fields.Date(string="Effective To", default=datetime.today(), copy=True)
    last_start_date = fields.Date(string="", required=False, copy=True)
    last_end_date = fields.Date(string="", required=False, copy=True)
    term = fields.Selection(
        [("onetime", "One Time"), ("year", "yearly"), ("semiannual", "Semi Annually"), ("quarter", "Quarterly"),
         ("month", "Monthly")],
        string="Payment Frequency", default='onetime', copy=True)

    is_canceled = fields.Boolean(string="", )
    no_years = fields.Integer(string="No. Years", default=1, copy=True)
    total_sum_insured = fields.Float(digits=(12, 2), string='Total Sum Insured', compute='_compute_total_si',
                                     inverse='_set_total',
                                     store=True)
    diff_sum_insured = fields.Float(digits=(12, 2), string='Sum Insured Difference', compute='_compute_diff_si',
                                    store=True)
    diff_net_premium = fields.Float(digits=(12, 2), string='Net Premium Difference',
                                    compute='_compute_diff_net_premium',
                                    store=True)
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    t_permimum = fields.Float(string="Net Premium", compute="_compute_t_premium", inverse='_set_net', copy=True,
                              store=True)
    rate = fields.Float(string='Rate' )
    new_rate = fields.Float(string='New Rate')
    new_deductible = fields.Char(string='New Deductible')

    create_date = fields.Date("created on")
    is_editable = fields.Boolean(string="", compute="check_edit")
    # rate = fields.Float(string='Rate', compute='rating_rule', inverse='_set_rate', store=True)
    # t_permimum = fields.Float(string="Net Premium", compute="_compute_t_premium", inverse='_set_net',store=True)

    gross_perimum_discountless = fields.Float(string="Gross Premium ", copy=True)
    discounts = fields.Float('Add/Deduct')
    gross_perimum = fields.Float(string="Final Gross Premium", copy=True, compute='compute_gross',
                                 inverse='compute_gross_inverse', default=lambda self: self.gross_perimum_discountless,
                                 store=True)
    sales_person1 = fields.Many2one('res.users', string='Sales Person', copy=True)
    saleschannel = fields.Many2one('crm.team', string='Sales Channel', copy=True)
    # sales_person1 = fields.Many2one('res.users', string='sales_person1', index=True, track_visibility='onchange', default=lambda self: self.env.user)
    team_id = fields.Many2one('crm.team', string='Sales Channel', oldname='section_id',
                              default=lambda self: self.env['crm.team'].sudo()._get_default_team_id(
                                  user_id=self.env.uid),
                              index=True, track_visibility='onchange',
                              help='When sending mails, the default email address is taken from the sales channel.')
    sharecommission_policy = fields.One2many('share', 'share_policy', string='Share Commission')

    commission_per = fields.Float(string="Commission", compute="_compute_commission_per", copy=True)
    share_commission = fields.One2many('insurance.share.commission', 'policy_id', string='Share Commissions', copy=True,
                                       ondelete='cascade')
    duration_no = fields.Integer('Policy Duration Number', copy=True)
    duration_type = fields.Selection([('day', 'Day'),
                                      ('month', 'Month'),
                                      ('year', 'Year'), ],
                                     'Policy Duration Type', track_visibility='onchange', copy=True)
    # staff_ids = fields.One2many(comodel_name="staff.commission", inverse_name="staff_id", string="Staff Commission",
    #                             required=False, ondelete='cascade')
    user = fields.Many2one('res.users', 'Approved By', default=lambda self: self.env.user)
    rella_installment_id = fields.One2many("installment.installment", "policy_id", string='Installments',
                                           ondelete='cascade', copy=False)
    customer = fields.Many2one('res.partner', 'Customer', copy=True,
                               domain="[('insurer_type','=', False),('agent','=', False),('customer','=', True)]")
    phone_number = fields.Char(related='customer.phone')

    discount_party = fields.Many2one('res.partner', 'Discount Party',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     copy=True)
    is_sent = fields.Boolean(string="Sent")
    insurance_type = fields.Selection([('Life', 'Life'),
                                       ('General', 'General'),
                                       ('Health', 'Health'), ],
                                      'Insurance Type', track_visibility='onchange', copy=True, default='General',
                                      required=True)
    ins_type = fields.Selection([('Individual', 'Individual'),
                                 ('Group', 'Group'), ],
                                'I&G', track_visibility='onchange', copy=True, default='Individual', required=True)
    risks_method = fields.Selection([('count', 'Count'),
                                     ('members', 'Members'),
                                     ],
                                    'Risk Method', track_visibility='onchange', copy=True)
    line_of_bussines = fields.Many2one('insurance.line.business', string='Line of business',
                                       domain="[('insurance_type','=',insurance_type)]", copy=True, required=True)
    line_related_name = fields.Char(related='line_of_bussines.line_of_business')
    # today = datetime.today().strftime('%Y-%m-%d')
    today = fields.Date(string="", required=False, compute='todau_comp')
    check_item = fields.Char(compute='_compute_insured_policy')
    type_char = fields.Char()
    group = fields.Boolean()
    deductible = fields.Char(string='Deductible')
    annual_prem = fields.Float(string='Annual Premium', computed='compute_annual_perm')
    charges = fields.Float(string='Charges', compute='compute_charge', store=True)
    basic_brokerage = fields.Float(string="Basic Commission", copy=True, compute='get_transportation_com_basic')
    complementary_comm = fields.Float(string="Complementary Commission", copy=True,
                                      compute='get_transportation_com_basic')
    transportation_comm = fields.Float(string="Transportation Commission", copy=True,
                                       compute='get_transportation_com_basic')
    tax = fields.Float(string="Tax", required=False, compute='get_tax')
    total_brokerages = fields.Float(string='Total Commission', compute='get_transportation_com_basic')
    new_risk_ids = fields.One2many("policy.risk", 'policy_risk_id', string=' Insured', copy=True, ondelete='cascade')
    company = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]")
    in_favour = fields.Many2one('res.partner', string="In Favour of",
                                domain="[('insurer_type','=', False),('agent','=', False),('customer','=', True)]")

    branch = fields.Many2one('insurance.setup', string="Branch",
                             domain="[('setup_key','=','branch'),('partner_id','=',company)]",
                             copy=True)
    product_policy = fields.Many2one('insurance.product',
                                     string="Product", copy=True,

                                     domain="[('insurer','=',company),('line_of_bus','=',line_of_bussines)]")

    active = fields.Boolean(string="Active", default=True)
    name_cover_rel_ids = fields.One2many("covers.lines", "policy_rel_id", string="Covers Details", copy=True)
    currency_id = fields.Many2one("res.currency", "Currency", copy=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    benefit = fields.Char("Beneficiary")
    checho = fields.Boolean()
    approved_date = fields.Date(string="Approved Date", required=False, )
    validate = fields.Selection([('info', 'Info'),
                                 ('risk', 'Risk'),
                                 ('cover', 'Cover'),
                                 ('com', 'Com'),
                                 ('bro', 'Bro'),
                                 ('ins', 'Ins'),
                                 ('financial', 'Financial'),
                                 ('inv', 'Inv'),
                                 ('share_com', 'Share Commission'),
                                 ('staff', 'Staff')],
                                'validate', track_visibility='onchange', default='info')
    policy_status = fields.Selection([('pending', 'Pending'),
                                      ('approved', 'Approved'),
                                      ('canceled', 'Canceled'), ],
                                     'Status', required=True, default='pending', copy=False)
    hide_inv_button = fields.Boolean(copy=False)
    attachment = fields.Binary(string='Excel File')
    attachment_life = fields.Binary(string='Excel File')
    group_ids = fields.One2many('risk.groups', 'policy_id', string='Group Members', ondelete='cascade')
    endorsement_no = fields.Char(string="Endorsement No.")
    endorsement_reason = fields.Text(string='Endorsement Reason')
    endorsement_type = fields.Selection([('increaseSI', 'Increase SI'),
                                         ('decreaseSI', 'DecreaseSI'),
                                         ('addrisk', 'Add Risk'),
                                         ('editerisk', 'Edit Risk'),
                                         ('deleterisk', 'Delete Risk'),
                                         ('canceled', 'Canceled'), ],
                                        string='Endorsement Type')
    endorsement_date = fields.Date(string="Endorsement Date")
    is_endorsement = fields.Boolean(string="", default=False)
    last_policy_id = fields.Many2one('policy.broker', readonly=True)
    renewal_no = fields.Char(string="Renewal No.")
    differnce1 = fields.Integer(compute='compute_date', force_save=True)
    renewal_state = fields.Boolean(default=False, store=True)
    renewal_state2 = fields.Boolean()

    alarmrenew = fields.Boolean()
    issuing = fields.Boolean(store=True)

    life_rate = fields.Float(string="Rate (life)", required=False, )

    is_renewal = fields.Boolean(string="Renewal")
    is_import = fields.Boolean(string="", )
    is_import_life = fields.Boolean(string="", )
    source_of_business = fields.Many2one('res.partner', string=' Source Of Business')
    decrease_net = fields.Float(string="D/I Net Amount", required=False, )
    decrease_gross = fields.Float(string="D/I Gross Amount", required=False, )
    state_track = fields.Char(default='New')
    policy_history = fields.Char(compute='get_policy_history')

    # gross_helper = fields.Float(string="",  required=False, )
    # net_helper = fields.Float(string="",  required=False, )

    @api.multi
    def print_motor_renewal(self):
        return self.env.ref('smart_policy.motor_renewal_report').report_action(self)

    @api.one
    @api.depends('company')
    def get_tax(self):
        if self.company:
            self.tax = self.total_brokerages * self.company.tax

    @api.onchange('term', 't_permimum')
    def compute_annual_perm(self):
        if self.term == 'onetime' or self.term == 'year':
            self.annual_prem = self.t_permimum
        elif self.term == 'semiannual':
            self.annual_prem = self.t_permimum * 2
        elif self.term == 'quarter':
            self.annual_prem = self.t_permimum * 4
        elif self.term == 'month':
            self.annual_prem = self.t_permimum * 12

    @api.one
    @api.depends('t_permimum', 'gross_perimum')
    def compute_charge(self):
        self.charges = self.gross_perimum - self.t_permimum

    @api.one
    def get_policy_history(self):
        for history in self:
            history.policy_history = self.env['endorsement.edit'].search_count([('number_policy', '=', self.id)])
            print(self.policy_history)

    @api.multi
    def show_policy_history(self):
        return {
            'name': ('Policy History'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'endorsement.edit',
            'target': 'current',
            'type': 'ir.actions.act_window',
            'domain': [('number_policy', '=', self.id)],
            'context': {'default_number_policy': self.id},
        }

    @api.onchange('start_date')
    def issue_start_deff(self):
        if self.issue_date and self.start_date:
            d1 = datetime.strptime(str(self.issue_date), '%Y-%m-%d')
            d2 = datetime.strptime(str(self.start_date), '%Y-%m-%d')
            d3 = (d1 - d2).days
            print(d3)
            if d3 >= 90 or d3 <= -90:
                raise ValidationError('Issue date - Effective date difference is too far')
            else:
                pass

    @api.one
    @api.depends('rella_installment_id')
    def check_edit(self):
        if self.rella_installment_id:
            for x in self.rella_installment_id:
                if x.state != ('outstanding') or x.is_sent == True:
                    self.is_editable = True

    @api.one
    @api.depends('total_sum_insured')
    def _compute_diff_si(self):
        if self.is_endorsement == True:
            # last_sum_isured = self.env['policy.broker'].search(['&',('std_id', '=', self.std_id),('id', '!=', self.id)],order='id desc',limit=1)
            last_sum_isured = self.env['policy.broker'].search(
                [('id', '=', self.last_policy_id.id)])
            # print(last_sum_isured)
            if last_sum_isured:
                print("hahahha")
                self.diff_sum_insured = self.total_sum_insured - last_sum_isured.total_sum_insured
                print(last_sum_isured.total_sum_insured)
            else:
                print("12234567")
                self.diff_sum_insured = self.total_sum_insured
        else:
            self.diff_sum_insured = self.total_sum_insured

    @api.one
    @api.depends('t_permimum')
    def _compute_diff_net_premium(self):
        if self.is_endorsement == True:
            # last_sum_isured = self.env['policy.broker'].search(['&',('std_id', '=', self.std_id),('id', '!=', self.id)],order='id desc',limit=1)
            last_net_premium = self.env['policy.broker'].search(
                [('id', '=', self.last_policy_id.id)])
            # print(last_sum_isured)
            if last_net_premium:
                print("hahahha")
                self.diff_net_premium = self.t_permimum - last_net_premium.t_permimum
                print(last_net_premium.t_permimum)
            else:
                print("12234567")
                self.diff_net_premium = self.t_permimum
        else:
            self.diff_net_premium = self.t_permimum

            # print(self.diff_sum_insured)

    # @api.multi
    # def _compute_staff_commission(self):
    #     if self.staff_ids:
    #         total = 0.0
    #         for record in self.staff_ids:
    #             total += record.total
    #             return total

    @api.model
    @api.onchange('gross_perimum_discountless')
    def compute_gross1(self):
        print(55555)
        self.gross_perimum = self.gross_perimum_discountless

    @api.one
    @api.depends('discounts', 'decrease_gross')
    def compute_gross(self):
        if self.gross_perimum_discountless:
            self.gross_perimum = self.gross_perimum_discountless + self.discounts

    @api.one
    def compute_gross_inverse(self):
        return True

    @api.one
    @api.depends('issue_date')
    def set_start_end_date(self):
        # self.start_date = self.issue_date
        if self.issue_date:

            if self.duration_type == 'day':

                date1 = datetime.strptime(self.start_date, '%Y-%m-%d').date()

                date3 = date1 + timedelta(days=self.duration_no)
                self.end_date = date3
            elif self.duration_type == 'month':
                date1 = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                date3 = date1 + relativedelta(months=self.duration_no)
                self.end_date = date3
            elif self.duration_type == 'year':
                date1 = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                date3 = date1 + relativedelta(years=self.duration_no)
                self.end_date = date3
        else:
            pass

    @api.one
    def _compute_commission_per(self):
        self.commission_per = (self.product_policy.commission_per / 100) * self.t_permimum

    # @api.onchange("t_permimum", "term")
    # def onchange_share_commission(self):
    #     if self.sales_person1:
    #         self.share_commission = [(0, 0, {
    #             'agent': self.sales_person1.id,
    #             'commission_per': 100, })]

    @api.model
    def getusr(self):
        context = self._context

        current_uid = context.get('uid')

        self.user = self.env['res.users'].browse(current_uid)
        print(self.user)

    # @api.onchange('customer')
    # def onchange_beneficiary(self):
    #     if not self.benefit:
    #         self.benefit = self.customer.name

    @api.one
    def todau_comp(self):
        self.today = datetime.today().strftime('%Y-%m-%d')

    @api.one
    @api.depends('new_risk_ids')
    def _compute_total_si(self):
        print("hisham mousssss")
        if self.new_risk_ids:
            if self.ins_type == 'Group':
                if self.insurance_type == 'Life':
                    pass
                    # self.total_sum_insured = 0.0
                    # for rec in self.new_risk_ids:
                    #     self.total_sum_insured += rec.sum_insured

                else:
                    self.total_sum_insured = 0.0
                    # for rec in self.new_risk_ids:
                    #     self.total_sum_insured += rec.sum_insured
            else:
                if self.is_canceled == False:
                    self.total_sum_insured = 0.0
                    for rec in self.new_risk_ids:
                        self.total_sum_insured += rec.sum_insured
                else:
                    self.total_sum_insured = 0.0
                    for rec in self.new_risk_ids:
                        self.total_sum_insured -= rec.sum_insured

    @api.one
    def _set_total(self):
        if self.diff_sum_insured != 0.00:
            self.t_permimum = (self.diff_sum_insured * self.rate) / 100

            return True
        else:
            return self._compute_total_si

    @api.one
    @api.depends("rate", 'diff_sum_insured')
    def _compute_t_premium(self):
        if self.diff_sum_insured and not self.rate:
            pass
            # self.t_permimum = 0.0
        elif self.diff_sum_insured and self.rate:
            fmt = '%Y-%m-%d'
            # print(self.last_start_date)
            # print(self.last_end_date)
            d1 = datetime.strptime(self.end_date, fmt).date() + timedelta(days=1)
            d2 = datetime.strptime(self.start_date, fmt)
            daysDiff = relativedelta(d1, d2)
            print(daysDiff)
            years_months = daysDiff.years * 12
            remaining_per = (((daysDiff.months) + years_months) / (12))
            if self.is_endorsement == True:
                self.t_permimum = self.diff_sum_insured * (self.rate / 100) * (remaining_per)
            else:
                self.t_permimum = (self.diff_sum_insured * (self.rate / 100) * (remaining_per))

    @api.one
    def _set_net(self):
        if self.t_permimum != 0.00:
            # print(self.t_permimum)
            # print(self.total_sum_insured)
            # self.rate = (self.t_permimum / self.total_sum_insured) * 100
            return True
        else:
            return self._compute_t_premium

    transportation_rate = fields.Float(string="Transportation Rate", required=False, compute='_compute_all',
                                       inverse='transportation_inverse', store=True)
    basic_rate = fields.Float(string="Basic Rate", required=False, compute='_compute_all', inverse='basic_inverse',
                              store=True)
    complementary_rate = fields.Float(string="Complementary Rate", required=False, compute='_compute_all',
                                      inverse='complementary_inverse', store=True)

    @api.one
    @api.depends('t_permimum')
    def _compute_all(self):
        if self.start_date:
            basic = self.env['product.brokerage'].search(
                [('commission_rule', '=', 'basic'),
                 ('datefrom', '<', datetime.today()), ('dateto', '>', datetime.today())])
            # print(basic)
            for x in basic:
                for rec in x.issue_commission:
                    if self.product_policy in rec.product_id:
                        # self.complementary_comm = self.t_permimum * (rec.complementary / 100)
                        self.transportation_rate = rec.transportation / 100
                        self.basic_rate = rec.basic / 100
                        self.complementary_rate = rec.complementary / 100
                        print( self.transportation_rate)
                        # self.basic_brokerage = self.t_permimum * (rec.basic / 100)
                        # self.total_brokerages = self.complementary_comm + self.transportation_comm + self.basic_brokerage
                    else:
                        pass

    @api.one
    def transportation_inverse(self):
        return True

    @api.one
    def basic_inverse(self):
        return True

    @api.one
    def complementary_inverse(self):
        return True

    @api.one
    @api.depends('transportation_rate', 'basic_rate', 'complementary_rate')
    def get_transportation_com_basic(self):
        self.transportation_comm = self.t_permimum * (self.transportation_rate)
        self.complementary_comm = self.t_permimum * (self.complementary_rate)
        self.basic_brokerage = self.t_permimum * (self.basic_rate)
        self.total_brokerages = self.complementary_comm + self.transportation_comm + self.basic_brokerage

    @api.one
    @api.depends('line_of_bussines', 'ins_type')
    def _compute_insured_policy(self):
        if self.ins_type == 'Group':
            self.check_item = 'Group'
        else:
            self.check_item = self.line_of_bussines.object

    @api.onchange('line_of_bussines', 'ins_type')
    def type_saving(self):
        self.type_char = self.line_of_bussines.object

    @api.multi
    def validate_basic(self):
        self.validate = 'info'

    @api.multi
    def validate_risk(self):
        if self.line_of_bussines and self.company:
            self.validate = 'risk'
            return True

    @api.multi
    def validate_financial(self):
        self.validate = 'financial'

    @api.multi
    def validate_cover(self):
        if self.new_risk_ids:
            self.validate = 'cover'
            if self.renewal_state:
                coverlines = self.env["covers.lines"].search([('id', 'in', self.name_cover_rel_ids.ids)])
                for rec in coverlines:
                    risk_id = self.env["policy.risk"].search([('old_id', '=', rec.old_risk_id)]).id
                    rec.write({'riskk': risk_id, })
            if self.endorsement_no:
                coverlines = self.env["covers.lines"].search([('id', 'in', self.name_cover_rel_ids.ids)])
                for rec in coverlines:
                    risk_id = self.env["policy.risk"].search([('old_id_end', '=', rec.old_risk_id_end)]).id
                    rec.write({'riskk': risk_id, })
            return True

    @api.multi
    def validate_commission(self):
        self.validate = 'com'

    @api.multi
    def validate_share_commission(self):
        self.validate = 'share_com'

    @api.multi
    def validate_brokrage(self):
        self.validate = 'bro'

    @api.multi
    def validate_installment(self):
        self.validate = 'ins'

    @api.multi
    def validate_invoice(self):
        self.validate = 'inv'

    @api.multi
    def generate_covers(self):
        self.checho = True
        return True

    @api.multi
    def name_get(self):
        result = []
        for s in self:
            name = s.std_id
            result.append((s.id, name))
        return result



    @api.multi
    def confirm_policy(self):
        if self.is_endorsement == False and self.t_permimum == 0:
            raise ValidationError('Gross Premium And Net Premium Must not be zero')
        if self.term and self.customer and self.line_of_bussines and self.company and self.insurance_type and self.product_policy:
            self.approved_date = datetime.today().strftime('%Y-%m-%d')
            print(self.is_endorsement)
            if self.is_endorsement == True:
                print("boss boss boss")
                last_confirmed_edit = self.env['policy.broker'].search(
                    ['&', '&', '&', ('std_id', '=', self.std_id), ('active', '=', 'True'), ('id', '!=', self.id),
                     ('policy_status', '=', 'approved')], )
                if last_confirmed_edit:
                    last_confirmed_edit.active = False
            # staff_ids = []
            # for rec in self.staff_ids:
            #     object = (0, 0, {'name': rec.name,
            #                      'total': rec.total,
            #                      })
            #     staff_ids.append(object)
            # print(staff_ids)
            self.policy_status = 'approved'
            self.hide_inv_button = True
            if self.line_of_bussines.line_of_business == 'Individual Life':
                if self.term == "onetime":
                    self.rella_installment_id = [(0, 0, {
                        "policy_number": self.std_id,
                        "endorsement_no": self.endorsement_no,
                        "is_renewal": self.is_renewal,
                        "customer": self.customer.id,
                        "discount_party": self.discount_party,
                        "net_premium": self.t_permimum,
                        "gross_premium": self.gross_perimum,
                        'rate': self.rate,
                        "insurer": self.company.id,
                        "insurance_type": self.insurance_type,
                        "lob": self.line_of_bussines.id,
                        "ins_product": self.product_policy.id,
                        "date": self.start_date,
                        'start_date': self.start_date,
                        'end_date': self.end_date,
                        'currency_id': self.currency_id,
                        'basic_brokerage': self.basic_brokerage,
                        'complementary_comm': self.complementary_comm,
                        'transportation_comm': self.transportation_comm,
                        'tax': self.tax,
                        'sales_person': self.sales_person1.id,
                        'team_id': self.team_id.id,
                        # 'staff_ids': staff_ids,
                        'source_of_business': self.source_of_business

                    })]
                elif self.term == "year":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(years=1)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(int(self.no_years) - 1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(years=1)
                    self.rella_installment_id = numbers
                elif self.term == "quarter":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(months=3)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,

                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(3):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=3)
                    self.rella_installment_id = numbers
                elif self.term == "month":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(months=1)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'tax': self.tax / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(11):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'tax': self.tax / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=1)
                    self.rella_installment_id = numbers
                elif self.term == "semiannual":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(months=6)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.gross_perimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'tax': self.tax / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'tax': self.tax / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=6)
                    self.rella_installment_id = numbers
            else:
                if self.term == "onetime":
                    self.rella_installment_id = [(0, 0, {
                        "policy_number": self.std_id,
                        "endorsement_no": self.endorsement_no,
                        "is_renewal": self.is_renewal,
                        "customer": self.customer.id,
                        "discount_party": self.discount_party,
                        "net_premium": self.t_permimum,
                        "gross_premium": self.gross_perimum,
                        'rate': self.rate,
                        "insurer": self.company.id,
                        "insurance_type": self.insurance_type,
                        "lob": self.line_of_bussines.id,
                        "ins_product": self.product_policy.id,
                        "date": self.start_date,
                        'start_date': self.start_date,
                        'end_date': self.end_date,
                        'currency_id': self.currency_id,
                        'basic_brokerage': self.basic_brokerage,
                        'complementary_comm': self.complementary_comm,
                        'transportation_comm': self.transportation_comm,
                        'tax': self.tax / 2,
                        'sales_person': self.sales_person1.id,
                        'team_id': self.team_id.id,
                        # 'staff_ids': staff_ids,
                        'source_of_business': self.source_of_business

                    })]
                elif self.term == "year":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    duration = relativedelta(years=1)
                    net = self.t_permimum + self.charges
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(int(self.no_years) - 1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": self.t_permimum,
                            "gross_premium": self.t_permimum,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage,
                            'complementary_comm': self.complementary_comm,
                            'transportation_comm': self.transportation_comm,
                            'tax': self.tax,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(years=1)
                    self.rella_installment_id = numbers
                elif self.term == "quarter":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    net = self.t_permimum / 4
                    duration = relativedelta(months=3)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net + self.charges,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(3):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 4,
                            'complementary_comm': self.complementary_comm / 4,
                            'transportation_comm': self.transportation_comm / 4,
                            'tax': self.tax / 4,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=3)
                    self.rella_installment_id = numbers
                elif self.term == "month":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    net = self.t_permimum / 12
                    duration = relativedelta(months=1)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net + self.charges,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'tax': self.tax / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(11):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 12,
                            'complementary_comm': self.complementary_comm / 12,
                            'transportation_comm': self.transportation_comm / 12,
                            'tax': self.tax / 12,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=1)
                    self.rella_installment_id = numbers
                elif self.term == "semiannual":
                    start = datetime.strptime(self.start_date, '%Y-%m-%d').date()
                    net = self.t_permimum / 2
                    duration = relativedelta(months=6)
                    numbers = []
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net + self.charges,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'tax': self.tax / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                    for i in range(1):
                        x = (0, 0, {
                            "policy_number": self.std_id,
                            "endorsement_no": self.endorsement_no,
                            "is_renewal": self.is_renewal,
                            "customer": self.customer.id,
                            "discount_party": self.discount_party,
                            "net_premium": net,
                            "gross_premium": net,
                            'rate': self.rate,
                            "insurer": self.company.id,
                            "insurance_type": self.insurance_type,
                            "lob": self.line_of_bussines.id,
                            "ins_product": self.product_policy.id,
                            'currency_id': self.currency_id,
                            "date": start + duration,
                            'start_date': self.start_date,
                            'end_date': self.end_date,
                            'basic_brokerage': self.basic_brokerage / 2,
                            'complementary_comm': self.complementary_comm / 2,
                            'transportation_comm': self.transportation_comm / 2,
                            'tax': self.tax / 2,
                            'sales_person': self.sales_person1.id,
                            'team_id': self.team_id.id,
                            # 'staff_ids': staff_ids,
                            'source_of_business': self.source_of_business

                        })
                        numbers.append(x)
                        duration = duration + relativedelta(months=6)
                    self.rella_installment_id = numbers
        else:
            raise UserError(_(
                "Customer ,Line of Bussines , Insurer , Insurance Type , Product ,Gross Premium , Net Premium , or Payment Frequency   "
                "should be inserted"))

    @api.multi
    def import_excel(self):
        if self.attachment:
            wb = open_workbook(file_contents=base64.decodestring(self.attachment))
            values = []
            for s in wb.sheets():
                for row in range(1, s.nrows):
                    col_value = []
                    for col in range(s.ncols):
                        value = (s.cell(row, col).value)
                        try:
                            value = str(int(value))
                        except:
                            pass
                        col_value.append(value)
                    values.append(col_value)

            return values
        else:
            raise ValidationError('please import your Excel Sheet !')

    @api.one
    def update_policy(self):
        for rec in self.import_excel():
            print(rec)

    @api.one
    def create_risk(self):
        fmt = '%Y-%m-%d'
        d1 = datetime.strptime(self.end_date, fmt).date() + timedelta(days=1)
        d2 = datetime.strptime(self.start_date, fmt)
        daysDiff = relativedelta(d1, d2)
        years_months = daysDiff.years * 12
        remaining_per = ((daysDiff.months) + years_months) / (12)
        self.is_import = True
        self.total_sum_insured = 0
        dict = {}
        for elem in self.import_excel():
            if elem[0] not in dict:
                dict[elem[0]] = []
            dict[elem[0]].append(elem[0:])
        if not self.new_risk_ids:
            for key, value in dict.items():
                print(int(value[0][12]))
                self.update({
                    'new_risk_ids': [(0, 0, {'group_category': key,
                                             'group_count': len(value),
                                             'group_premium': int(value[0][12])})],
                })
                i = 1
                for item in value:

                    if item[11] != 'D':
                        print("1")
                        book = open_workbook(file_contents=base64.decodestring(self.attachment))
                        sh = book.sheet_by_index(0)
                        a1 = sh.cell_value(rowx=i, colx=6)
                        date = datetime(*xlrd.xldate_as_tuple(a1, book.datemode))
                        i += 1
                        # self.env._cr.execute(
                        #     "INSERT INTO risk_groups (group_name,member_id,name,card_num,employee_code_mic,dob,gender,head_family_id,head_family_name,end_date,status,sum_insured,policy_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                        #     (item[0],item[1x],item[2],item[3],item[4],item[5],item[6],item[7],item[8],item[9],item[10],item[11],item[12],self.id))

                        self.update({
                            'group_ids': [(0, 0, {'group_name': item[0],
                                                  'member_id': item[1],
                                                  'name': item[2],
                                                  'card_num': item[3],
                                                  'employee_code_mic': item[4],
                                                  'relation': item[5],
                                                  'dob': date,
                                                  'gender': item[7],
                                                  'head_family_id': item[8],
                                                  'head_family_name': item[9],
                                                  'end_date': item[10],
                                                  'status': item[11],
                                                  'sum_insured': item[12],
                                                  'policy_id': self.id
                                                  })], })
                        my_risk = self.env['policy.risk'].search(
                            ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                        my_risk.sum_insured += int(item[12])
                    else:
                        print("2")
                        my_risk = self.env['policy.risk'].search(
                            ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                        # print(my_risk)
                        my_risk.group_count -= 1
                        my_risk.risk_description = (str(
                            my_risk.group_category) + " - " if my_risk.group_category else " " + "") + "   " + (str(
                            my_risk.group_count) + " - " if my_risk.group_count else " " + "") + (str(
                            self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
            for rec in self.new_risk_ids:
                self.t_permimum += rec.group_premium * rec.group_count
                #     self.t_permimum += rec.sum_insured

        else:
            listofrisk = []
            for x in self.new_risk_ids:
                listofrisk.append(x.group_category)
            print(listofrisk)
            for key, value in dict.items():
                if key not in listofrisk:
                    print('leeeeh')
                    self.update({
                        'new_risk_ids': [(0, 0, {'group_category': key,
                                                 'group_count': 0})],
                        'group_premium': int(value[0][12])
                    })

                i = 1
                for item in value:
                    book = open_workbook(file_contents=base64.decodestring(self.attachment))
                    sh = book.sheet_by_index(0)
                    a1 = sh.cell_value(rowx=i, colx=6)
                    date = datetime(*xlrd.xldate_as_tuple(a1, book.datemode))
                    i += 1
                    opj = self.env['risk.groups'].search(
                        ['&', '&', '&',
                         ('member_id', '=', item[1]),
                         ('policy_id.std_id', '=', self.std_id),
                         ('status', '!=', 'D'),
                         ('is_current', '=', True)], order='id desc', limit=1)
                    if opj:
                        if opj.group_name == key:
                            if item[11] == 'D':
                                print(opj.group_name)
                                print(item[0])
                                print("3")
                                opj.is_current = False
                                my_risk = self.env['policy.risk'].search(
                                    ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                                my_risk.group_count -= 1
                                my_risk.risk_description = (str(
                                    my_risk.group_category) + " - " if my_risk.group_category else " " + "_") + "   " + (
                                                               str(
                                                                   my_risk.group_count) + " - " if my_risk.group_count else " " + "_") + (
                                                               str(
                                                                   self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
                                # my_risk.sum_insured -= int(item[12])

                                self.update({
                                    'group_ids': [(0, 0, {'group_name': item[0],
                                                          'member_id': item[1],
                                                          'name': item[2],
                                                          'card_num': item[3],
                                                          'employee_code_mic': item[4],
                                                          'relation': item[5],
                                                          'dob': date,
                                                          'gender': item[7],
                                                          'head_family_id': item[8],
                                                          'head_family_name': item[9],
                                                          'end_date': item[10],
                                                          'status': item[11],
                                                          'sum_insured': (-opj.sum_insured * remaining_per),
                                                          'policy_id': self.id
                                                          })], })
                            else:
                                print(opj.group_name)
                                print(item[0])
                                print("4")
                                opj.is_current = False
                                my_risk = self.env['policy.risk'].search(
                                    ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                                self.update({
                                    'group_ids': [(0, 0, {'group_name': item[0],
                                                          'member_id': item[1],
                                                          'name': item[2],
                                                          'card_num': item[3],
                                                          'employee_code_mic': item[4],
                                                          'relation': item[5],
                                                          'dob': date,
                                                          'gender': item[7],
                                                          'head_family_id': item[8],
                                                          'head_family_name': item[9],
                                                          'end_date': item[10],
                                                          'status': item[11],
                                                          # 'sum_insured': (my_risk.group_premium*remaining_per),
                                                          'sum_insured': opj.sum_insured,
                                                          'policy_id': self.id
                                                          })], })

                                # my_risk.sum_insured -= opj.sum_insured
                                # my_risk.sum_insured += int(item[12])
                        else:
                            print(opj.group_name)
                            print(item[0])
                            print("5")
                            opj.is_current = False
                            last_risk = self.env['policy.risk'].search(
                                ['&', ('group_category', '=', opj.group_name),
                                 ('policy_risk_id', '=', self.id)], )
                            last_risk.group_count -= 1
                            last_risk.risk_description = (str(
                                last_risk.group_category) + " - " if last_risk.group_category else " " + "_") + "   " + (
                                                             str(
                                                                 last_risk.group_count) + " - " if last_risk.group_count else " " + "_") + (
                                                             str(
                                                                 self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
                            current_risk = self.env['policy.risk'].search(
                                ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                            current_risk.group_count += 1
                            # current_risk.sum_insured += int(item[12])
                            current_risk.risk_description = (str(
                                current_risk.group_category) + " - " if current_risk.group_category else " " + "_") + "   " + (
                                                                str(
                                                                    current_risk.group_count) + " - " if current_risk.group_count else " " + "_") + (
                                                                str(
                                                                    self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
                            self.update({
                                'group_ids': [(0, 0, {'group_name': item[0],
                                                      'member_id': item[1],
                                                      'name': item[2],
                                                      'card_num': item[3],
                                                      'employee_code_mic': item[4],
                                                      'relation': item[5],
                                                      'dob': date,
                                                      'gender': item[7],
                                                      'head_family_id': item[8],
                                                      'head_family_name': item[9],
                                                      'end_date': item[10],
                                                      'status': item[11],
                                                      'sum_insured': (current_risk.group_premium * remaining_per),
                                                      'policy_id': self.id
                                                      })], })

                    else:
                        if item[11] != 'D':
                            print("6")
                            print(item[0])
                            # print("ana hena ya jack")
                            my_risk = self.env['policy.risk'].search(
                                ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                            # print(my_risk)
                            my_risk.group_count += 1
                            # my_risk.sum_insured += int(item[12])
                            my_risk.risk_description = (str(
                                my_risk.group_category) + " - " if my_risk.group_category else " " + "_") + "   " + (
                                                           str(
                                                               my_risk.group_count) + " - " if my_risk.group_count else " " + "_") + (
                                                           str(
                                                               self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
                            print(my_risk.group_premium * remaining_per)
                            self.update({
                                'group_ids': [(0, 0, {'group_name': item[0],
                                                      'member_id': item[1],
                                                      'name': item[2],
                                                      'card_num': item[3],
                                                      'employee_code_mic': item[4],
                                                      'relation': item[5],
                                                      'dob': date,
                                                      'gender': item[7],
                                                      'head_family_id': item[8],
                                                      'head_family_name': item[9],
                                                      'end_date': item[10],
                                                      'status': item[11],
                                                      'sum_insured': (my_risk.group_premium * remaining_per),
                                                      'policy_id': self.id
                                                      })], })
            members = self.env['risk.groups'].search(
                ['&', '&', ('status', '!=', 'D'), ('policy_id.std_id', '=', self.std_id), ('is_current', '=', True)], )
            for rec in members:
                self.t_permimum += rec.sum_insured

    @api.multi
    def import_life_excel(self):
        if self.attachment_life:
            wb = open_workbook(file_contents=base64.decodestring(self.attachment_life))
            values = []
            for s in wb.sheets():
                for row in range(1, s.nrows):
                    col_value = []
                    for col in range(s.ncols):
                        value = (s.cell(row, col).value)
                        try:
                            value = str(int(value))
                        except:
                            pass
                        col_value.append(value)
                    values.append(col_value)

            return values
        else:
            raise ValidationError('please import your Excel Sheet !')

    @api.one
    def create_life_risk(self):
        fmt = '%Y-%m-%d'
        d1 = datetime.strptime(self.end_date, fmt).date() + timedelta(days=1)
        d2 = datetime.strptime(self.start_date, fmt)
        daysDiff = relativedelta(d1, d2)
        years_months = daysDiff.years * 12
        remaining_per = ((daysDiff.months) + years_months) / (12)
        self.is_import_life = True
        self.total_sum_insured = 0
        dict = {}
        for elem in self.import_life_excel():
            if elem[0] not in dict:
                dict[elem[0]] = []
            dict[elem[0]].append(elem[0:])
        if not self.new_risk_ids:
            for key, value in dict.items():
                self.update({
                    'new_risk_ids': [(0, 0, {'group_category': key,
                                             'group_count': len(value),
                                             'group_premium': int(value[0][12])})],
                })
                i = 1
                for item in value:

                    if item[11] != 'D':

                        print("1")
                        book = open_workbook(file_contents=base64.decodestring(self.attachment_life))
                        sh = book.sheet_by_index(0)
                        a1 = sh.cell_value(rowx=i, colx=6)
                        date = datetime(*xlrd.xldate_as_tuple(a1, book.datemode))
                        i += 1
                        self.update({
                            'group_ids': [(0, 0, {'group_name': item[0],
                                                  'member_id': item[1],
                                                  'name': item[2],
                                                  'card_num': item[3],
                                                  'employee_code_mic': item[4],
                                                  'relation': item[5],
                                                  'dob': date,
                                                  'gender': item[7],
                                                  'head_family_id': item[8],
                                                  'head_family_name': item[9],
                                                  'end_date': item[10],
                                                  'status': item[11],
                                                  'sum_insured': item[12],
                                                  'policy_id': self.id
                                                  })], })
                        my_risk = self.env['policy.risk'].search(
                            ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                        my_risk.sum_insured += int(item[12])
                    else:
                        print("2")
                        my_risk = self.env['policy.risk'].search(
                            ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                        # print(my_risk)
                        my_risk.group_count -= 1
                        my_risk.risk_description = (str(
                            my_risk.group_category) + " - " if my_risk.group_category else " " + "") + "   " + (str(
                            my_risk.group_count) + " - " if my_risk.group_count else " " + "") + (str(
                            self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")

            for rec in self.new_risk_ids:
                self.total_sum_insured += rec.sum_insured

        else:
            listofrisk = []
            for x in self.new_risk_ids:
                listofrisk.append(x.group_category)
            print(listofrisk)
            for key, value in dict.items():
                if key not in listofrisk:
                    print('leeeeh')
                    self.update({
                        'new_risk_ids': [(0, 0, {'group_category': key,
                                                 'group_count': 0,
                                                 'group_premium': int(value[0][12])})],
                    })
                i = 1
                for item in value:
                    book = open_workbook(file_contents=base64.decodestring(self.attachment_life))
                    sh = book.sheet_by_index(0)
                    a1 = sh.cell_value(rowx=i, colx=6)
                    date = datetime(*xlrd.xldate_as_tuple(a1, book.datemode))
                    i += 1
                    opj = self.env['risk.groups'].search(
                        ['&', '&', '&',
                         ('member_id', '=', item[1]),
                         ('policy_id.std_id', '=', self.std_id),
                         ('status', '!=', 'D'),
                         ('is_current', '=', True)], order='id desc', limit=1)
                    if opj:
                        if opj.group_name == key:
                            if item[11] == 'D':
                                print(opj.group_name)
                                print(item[0])
                                print("3")
                                opj.is_current = False
                                my_risk = self.env['policy.risk'].search(
                                    ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                                my_risk.group_count -= 1
                                my_risk.risk_description = (str(
                                    my_risk.group_category) + " - " if my_risk.group_category else " " + "_") + "   " + (
                                                               str(
                                                                   my_risk.group_count) + " - " if my_risk.group_count else " " + "_") + (
                                                               str(
                                                                   self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")

                                my_risk.sum_insured -= int(item[12])
                                self.update({
                                    'group_ids': [(0, 0, {'group_name': item[0],
                                                          'member_id': item[1],
                                                          'name': item[2],
                                                          'card_num': item[3],
                                                          'employee_code_mic': item[4],
                                                          'relation': item[5],
                                                          'dob': date,
                                                          'gender': item[7],
                                                          'head_family_id': item[8],
                                                          'head_family_name': item[9],
                                                          'end_date': item[10],
                                                          'status': item[11],
                                                          'policy_id': self.id,
                                                          'sum_insured': (-opj.sum_insured * remaining_per),

                                                          })], })
                            else:
                                print(opj.group_name)
                                print(item[0])
                                print("4")
                                opj.is_current = False
                                self.update({
                                    'group_ids': [(0, 0, {'group_name': item[0],
                                                          'member_id': item[1],
                                                          'name': item[2],
                                                          'card_num': item[3],
                                                          'employee_code_mic': item[4],
                                                          'relation': item[5],
                                                          'dob': date,
                                                          'gender': item[7],
                                                          'head_family_id': item[8],
                                                          'head_family_name': item[9],
                                                          'end_date': item[10],
                                                          'status': item[11],
                                                          'sum_insured': opj.sum_insured,
                                                          'policy_id': self.id
                                                          })], })
                                my_risk = self.env['policy.risk'].search(
                                    ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                                my_risk.sum_insured -= opj.sum_insured
                                my_risk.sum_insured += int(item[12])
                        else:
                            print(opj.group_name)
                            print(item[0])
                            print("5")
                            opj.is_current = False
                            last_risk = self.env['policy.risk'].search(
                                ['&', ('group_category', '=', opj.group_name),
                                 ('policy_risk_id', '=', self.id)], )
                            last_risk.group_count -= 1
                            last_risk.sum_insured -= opj.sum_insured
                            last_risk.risk_description = (str(
                                last_risk.group_category) + " - " if last_risk.group_category else " " + "_") + "   " + (
                                                             str(
                                                                 last_risk.group_count) + " - " if last_risk.group_count else " " + "_") + (
                                                             str(
                                                                 self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
                            current_risk = self.env['policy.risk'].search(
                                ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                            current_risk.group_count += 1
                            current_risk.sum_insured += int(item[12])
                            current_risk.risk_description = (str(
                                current_risk.group_category) + " - " if current_risk.group_category else " " + "_") + "   " + (
                                                                str(
                                                                    current_risk.group_count) + " - " if current_risk.group_count else " " + "_") + (
                                                                str(
                                                                    self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
                            self.update({
                                'group_ids': [(0, 0, {'group_name': item[0],
                                                      'member_id': item[1],
                                                      'name': item[2],
                                                      'card_num': item[3],
                                                      'employee_code_mic': item[4],
                                                      'relation': item[5],
                                                      'dob': date,
                                                      'gender': item[7],
                                                      'head_family_id': item[8],
                                                      'head_family_name': item[9],
                                                      'end_date': item[10],
                                                      'status': item[11],
                                                      'sum_insured': (float(item[12]) * remaining_per),
                                                      'policy_id': self.id
                                                      })], })

                    else:
                        if item[11] != 'D':
                            print("6")
                            print(item[0])
                            # print("ana hena ya jack")
                            my_risk = self.env['policy.risk'].search(
                                ['&', ('group_category', '=', item[0]), ('policy_risk_id', '=', self.id)], )
                            # print(my_risk)
                            my_risk.group_count += 1
                            my_risk.sum_insured += int(item[12])
                            my_risk.risk_description = (str(
                                my_risk.group_category) + " - " if my_risk.group_category else " " + "_") + "   " + (
                                                           str(
                                                               my_risk.group_count) + " - " if my_risk.group_count else " " + "_") + (
                                                           str(
                                                               self.line_of_bussines.object) + " - " if self.line_of_bussines.object else " " + "")
                            self.update({
                                'group_ids': [(0, 0, {'group_name': item[0],
                                                      'member_id': item[1],
                                                      'name': item[2],
                                                      'card_num': item[3],
                                                      'employee_code_mic': item[4],
                                                      'relation': item[5],
                                                      'dob': date,
                                                      'gender': item[7],
                                                      'head_family_id': item[8],
                                                      'head_family_name': item[9],
                                                      'end_date': item[10],
                                                      'status': item[11],
                                                      'sum_insured': (float(item[12]) * remaining_per),
                                                      'policy_id': self.id
                                                      })], })

            # date_format = '%Y-%m-%d'
            # d1 = datetime.strptime(self.start_date, date_format).date()
            # d2 = datetime.strptime(self.end_date, date_format).date()
            # r = relativedelta(d2, d1)
            # numofmonths = r.months
            for rec in self.new_risk_ids:
                self.total_sum_insured += rec.sum_insured

    @api.onchange('endorsement_type')
    def _onchange_policy_status(self):
        if self.endorsement_type == 'canceled':
            self.policy_status = 'canceled'

    @api.multi
    def generate_task(self):
        # Create an new Excel file and add a worksheet.
        print(555555555555555555555555555555555555555)
        workbook = xlsxwriter.Workbook('xx.xlsx')
        worksheet = workbook.add_worksheet()

        # Widen the first column to make the text clearer.
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)

        # Insert an image.
        worksheet.write('A1', 'رقم الوثيقة ')
        worksheet.write('A2', self.std_id)
        # worksheet.insert_image('B2', self.name  )

        # Insert an image offset in the cell.
        worksheet.write('B1', 'وسيط التامين  ')
        worksheet.write('B2', str(self.discount_party.name))

        # worksheet.insert_image('B12', self.start)

        # Insert an image with scaling.
        worksheet.write('C1', 'شركة التامين   ')
        worksheet.write('C2', str(self.company.name))

        worksheet.write('D1', 'نوع التامين ')
        worksheet.write('D2', str(self.insurance_type))

        worksheet.write('E1', ' تاريخ الاصدار')
        worksheet.write('E2', str(self.start_date))

        worksheet.write('F1', ' اسم العميل ')
        worksheet.write('F2', str(self.customer.name))

        worksheet.write('G1', ' اجمالي القسط')
        worksheet.write('G2', str(self.total_sum_insured))

        worksheet.write('H1', ' صافي القسط')
        worksheet.write('H2', str(self.t_permimum))

        worksheet.write('J1', ' بيانات اخرى ')

        worksheet.write('I1', ' مبلغ العمولة ')
        worksheet.write('I2', str(self.total_brokerages))

        # worksheet.insert_image('B23', self.end)

        workbook.close()

    def create_endo(self):

        return {
            'name': ('Endorsement'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'endorsement.edit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': {
                'default_number_policy': self.id,

            }
        }

    # def duplicate_policy(self):
    #

    # if self.clean_option_customer:
    #     for rec in active_ids:
    #        for record in self.env['policy.broker'].search( [('discount_party', '=', rec)]):
    #            if self.discount_part:
    #                record.write({'discount_party': self.discount_part.id})
    #

    #     return {
    #         'name': ('Endorsement'),
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'policy.broker',
    #         'view_id': False,
    #         'type': 'ir.actions.act_window',
    #         'target': 'current',
    #
    #     }

    class Extra_Covers(models.Model):
        _name = "covers.lines"
        _rec_name = 'name1'

        riskk = fields.Many2one("policy.risk", "Risk", domain="[('id','in',new_risk_ids)]")

        old_risk_id = fields.Integer()
        old_risk_id_end = fields.Integer()

        insurerd = fields.Many2one(related="policy_rel_id.company")
        prod_product = fields.Many2one(related="policy_rel_id.product_policy", domain="[('insurer','=',insurerd)]")
        name1 = fields.Many2one("insurance.product.coverage", string="Cover",
                                domain="[('product_id', '=' , prod_product)]")
        readonly = fields.Boolean(related="name1.readonly")
        sum_insure = fields.Float(related="name1.defaultvalue", string="Sum Insured")
        deductible = fields.Char(related="name1.deductible", string='Deductible')
        limitone = fields.Float(related="name1.limitone", string='Limit in One')
        limittotal = fields.Float(related="name1.limittotal", string='Limit in Total')
        rate = fields.Float(string="Rate")
        net_perimum = fields.Float(string="Net Premium", compute='_compute_net_premium')
        policy_rel_id = fields.Many2one("policy.broker", ondelete='cascade', index=True)
        new_risk_ids = fields.One2many(related='policy_rel_id.new_risk_ids', ondelete='cascade')

        _sql_constraints = [
            ('cover_unique', 'unique(policy_rel_id,riskk,name1)', 'Cover already exists!')]

        @api.one
        @api.depends('sum_insure', 'rate')
        def _compute_net_premium(self):
            if self.sum_insure and self.rate and self.readonly == False:
                self.net_perimum = self.sum_insure * (self.rate / 1000)
            elif self.readonly == True:
                self.rate = 0.0
                self.net_perimum = self.sum_insure

class ShareCommition(models.Model):
        _name = "insurance.share.commission"

        agent = fields.Many2one("res.partner", string="Agent")
        commission_per = fields.Float(string="Percentage")
        amount = fields.Float(string="Amount", compute='_compute_amount')
        policy_id = fields.Many2one("policy.broker", ondelete='cascade', index=True)

        @api.one
        @api.depends('commission_per')
        def _compute_amount(self):
            self.amount = self.policy_id.commission_per * (self.commission_per / 100)





class inheritCollectionTracking(models.Model):
    _inherit = 'installment.installment'

    policy_id = fields.Many2one("policy.broker", ondelete='cascade', index=True)
    bonus = fields.Float(string="Collection Bonus", required=False, compute='claculateBoun')
    check_date = fields.Boolean()
    hide_inst_button = fields.Boolean(string="", default=False)

    @api.one
    @api.depends('date', 'delv_date_ins')
    def claculateBoun(self):
        if self.delv_date_ins and self.ins_product:
            today = datetime.today().strftime('%Y-%m-%d')
            opj = self.env['product.brokerage'].search(
                ['&', '&', ('product_id', '=', self.ins_product.id), ('datefrom', '<=', today),
                 ('dateto', '>=', today)], order='id desc',
                limit=1)
            if opj:
                duration = []
                date1 = fields.Date.from_string(self.date)
                date2 = fields.Date.from_string(self.delv_date_ins)
                daysDiff = (date2 - date1).days
                print(daysDiff)
                for x in opj.collection_bonus_data:
                    if x.duration >= daysDiff:
                        duration.append(x.duration)
                # print(duration)
                # print(daysDiff)
                if duration:
                    min_duration = min(duration)
                    print(min_duration)

                    for rec in opj.collection_bonus_data:
                        # print(rec.duration)
                        # print(daysDiff)
                        if rec.duration == min_duration:
                            self.check_date = True
                            self.hide_inst_button = True
                            pre = rec.commission
                            self.bonus = self.net_premium * (pre / 100)

    @api.multi
    def convert_received(self):
        if self.bonus:
            move_line_1 = {
                'name': 'Collection Bonus Commission',
                'account_id': self.lob.income_account.id,
                'debit': 0.0,
                'credit': self.bonus,
                'journal_id': 1,
                'partner_id': self.insurer.id,
                'currency_id': self.currency_id.id,
            }
            move_line_2 = {
                'name': 'Collection Bonus Commission',
                'account_id': self.insurer.property_account_payable_id.id,
                'debit': self.bonus,
                'credit': 0.0,
                'journal_id': 1,
                'partner_id': self.insurer.id,
                'currency_id': self.currency_id.id,
            }
            move_vals = {
                'ref': self.policy_number,
                'date': self.date,
                'journal_id': 1,
                'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
            }
            self.move_id.create(move_vals)
            return super(inheritCollectionTracking, self).convert_received()

    @api.multi
    def generate(self):
        # Create an new Excel file and add a worksheet.
        print(555555555555555555555555555555555555555)
        workbook = xlsxwriter.Workbook('xx.xlsx')
        worksheet = workbook.add_worksheet()

        # Widen the first column to make the text clearer.
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)

        # Insert an image.
        worksheet.write('A1', 'رقم الوثيقة ')
        worksheet.write('A2', self.std_id)
        # worksheet.insert_image('B2', self.name  )

        # Insert an image offset in the cell.
        worksheet.write('B1', 'وسيط التامين  ')
        worksheet.write('B2', str(self.discount_party.name))

        # worksheet.insert_image('B12', self.start)

        # Insert an image with scaling.
        worksheet.write('C1', 'شركة التامين   ')
        worksheet.write('C2', str(self.company.name))

        worksheet.write('D1', 'نوع التامين ')
        worksheet.write('D2', str(self.insurance_type))

        worksheet.write('E1', ' تاريخ الاصدار')
        worksheet.write('E2', str(self.start_date))

        worksheet.write('F1', ' اسم العميل ')
        worksheet.write('F2', str(self.customer.name))

        worksheet.write('G1', ' اجمالي القسط')
        worksheet.write('G2', str(self.total_sum_insured))

        worksheet.write('H1', ' صافي القسط')
        worksheet.write('H2', str(self.t_permimum))

        worksheet.write('J1', ' بيانات اخرى ')

        worksheet.write('I1', ' مبلغ العمولة ')
        worksheet.write('I2', str(self.total_brokerages))

        # worksheet.insert_image('B23', self.end)

        workbook.close()


class NewModule(models.Model):
    _name = 'data.clean.lob'

    clean_option_lob = fields.Many2one('insurance.line.business', string='Line of business')
    attachment1 = fields.Binary(string='Excel File')

    # clean_option_customer=fields.Boolean('Clean Discount Party')
    #
    # insurer = fields.Many2one("res.partner", string="correct Insurer", required=False, domain="[('insurer_type','=',1)]" )
    # discount_part=fields.Many2one("res.partner", string="correct discount Party", required=False,  domain="[('insurer_type','=', False),('agent','=', False),('customer','=', True)]")

    @api.multi
    def replace_lob(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for record in self.env['policy.broker'].search(
                ['&', '|', ('line_of_bussines', '=', False)('active', '=', True), ('active', '=', False)]):
            if self.clean_option_lob:
                record.write({'line_of_bussines': self.clean_option_lob.id})

        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def set_state_track(self):
        for record in self.env['policy.broker'].search(['|', ('active', '=', True), ('active', '=', False)]):
            if (record.is_renewal == True) and (record.is_endorsement == False):
                record.state_track = 'Renewal'
            elif (record.is_renewal == False) and (record.is_endorsement == False):
                record.state_track = 'New'
            else:
                record.state_track = 'Endorsement'
            print(record.active)
        for record in self.env['policy.broker'].search([('active', '=', True)]):
            record.diff_sum_insured = record.total_sum_insured

    @api.multi
    def import_excel(self):
        if self.attachment1:
            wb = open_workbook(file_contents=base64.decodestring(self.attachment1))
            values = []
            for s in wb.sheets():
                for row in range(1, s.nrows):
                    col_value = []
                    for col in range(s.ncols):
                        value = (s.cell(row, col).value)
                        try:
                            value = str(int(value))
                        except:
                            pass
                        col_value.append(value)
                    values.append(col_value)

            return values
        else:
            raise ValidationError('please import your Excel Sheet !')

    @api.multi
    def update_policy(self):
        for rec in self.import_excel():
            if rec[1] == '':
                rec[1] = False
            for pol in self.env['policy.broker'].search(
                    [('active', '=', False), ('active', '=', True), ('endorsement_no', '=', rec[0])]):
                print(pol.std_id)
                pol.write({'diff_sum_insured': 0})

    @api.multi
    def update_policy_insurer(self):
        for rec in self.import_excel():
            for pol in self.env['policy.broker'].search([('std_id', '=', rec[0]), ('active', '=', False)]):
                print(pol.std_id)
                pol.write({'t_permimum': rec[1]})
            for pol in self.env['policy.broker'].search([('std_id', '=', rec[0]), ('active', '=', True)]):
                print(pol.std_id)
                pol.write({'t_permimum': rec[1]})

    @api.multi
    def update_policy_insurer_motor(self):
        for rec in self.import_excel():
            for pol in self.env['policy.broker'].search(
                    [('std_id', '=', rec[0]), ('line_of_bussines.line_of_business', '=', 'Motor'),
                     ('active', '=', False)]):
                print(pol.std_id)
                pol.write(
                    {'company': self.env['res.partner'].search([('name', '=', rec[1]), ('insurer_type', '=', 1)]).id})
            for pol in self.env['policy.broker'].search(
                    [('std_id', '=', rec[0]), ('line_of_bussines.line_of_business', '=', 'Motor'),
                     ('active', '=', True)]):
                pol.write(
                    {'company': self.env['res.partner'].search([('name', '=', rec[1]), ('insurer_type', '=', 1)]).id})

    @api.multi
    def update_policy_uit(self):
        for pol in self.env['policy.broker'].search(['|', ('state_track', '=', 'New'), ('state_track', '=', 'Renewal'),
                                                     ('line_of_bussines.line_of_business', '=', 'Motor'),
                                                     ('start_date', '>=', '1/4/2019'), ('active', '=', True)]):
            print(pol.std_id)
            pol.write(
                {'diff_sum_insured': pol.total_sum_insured})
        for pol in self.env['policy.broker'].search(['|', ('state_track', '=', 'New'), ('state_track', '=', 'Renewal'),
                                                     ('line_of_bussines.line_of_business', '=', 'Motor'),
                                                     ('start_date', '>=', '1/4/2019'), ('active', '=', False)]):
            print(pol.std_id)
            pol.write(
                {'diff_sum_insured': pol.total_sum_insured})

        # for pol in self.env['policy.broker'].search( [('benefit', '=', 'UIT'), ('line_of_bussines.line_of_business', '=', 'Motor'),('issue_date', '>=', '1/5/2018'), ('active', '=', True)]):
        #     print(pol.std_id)
        #     pol.write(
        #         {'company': self.env['res.partner'].search([('name', '=', 'ESIH'), ('insurer_type', '=', 1)]).id})
        #
        # for pol in self.env['policy.broker'].search(
        #         [('benefit', '=', 'UIT'), ('line_of_bussines.line_of_business', '=', 'Motor'),
        #          ('issue_date', '>=', '1/5/2018'), ('active', '=', False)]):
        #     print(pol.std_id)
        #     pol.write(
        #         {'company': self.env['res.partner'].search([('name', '=', 'ESIH'), ('insurer_type', '=', 1)]).id})


class NewModule(models.Model):
    _name = 'cancel.policy'

    @api.multi
    def cancel_policy(self):
        cancel = False
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for rec in self.env['policy.broker'].search(
                ['|', ('active', '=', True), ('active', '=', False), ('id', 'in', active_ids)]):
            notcancel = False
            # rec.write({'policy_status': 'canceled','is_editable':False})

            for record in rec.rella_installment_id:
                if record.state != ('outstanding'):
                    notcancel = True
                    break
            if notcancel == False:
                rec.write({'policy_status': 'canceled', 'is_editable': False, 'active': False})
                for record in rec.rella_installment_id:
                    record.write({'state': 'canceled'})
            else:
                raise UserError(_(
                    'You cannot cancel this policy which its installment is not outstanding or mark as sent or Policy not Active'))

        return {'type': 'ir.actions.act_window_close'}


class ShareCommissionPolicy(models.Model):
    _name = 'share'
    # _rec_name = 'share_commission_policy'

    share_policy = fields.Many2one('policy.broker')
    seller_commission = fields.Float(string="Commission(%)")
    sales_person1 = fields.Many2one(comodel_name="res.users", string="Sales Person", required=False, )
