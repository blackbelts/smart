from odoo import api, fields, models


class Endorsement_edit(models.Model):
    _name = "endorsement.edit"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = "endorsement_no"

    number_policy = fields.Many2one("policy.broker", string="Edit policy number",
                                    domain="[('policy_status', '=','approved')]")
    endorsement_no = fields.Char(string="Endorsement Number")
    last_policy = fields.Many2one('policy.broker')
    reasonedit = fields.Text(string="Endorsement Discribtion", required=True)
    end_date = fields.Date(string="End Date")
    endorsement_date = fields.Date(string="Endorsement Date")
    converted = fields.Boolean(default=False)
    endorsement_type = fields.Selection([('increaseSI', 'Increase SI'),
                                         ('decreaseSI', 'DecreaseSI'),
                                         ('addrisk', 'Add Risk'),
                                         ('editerisk', 'Edit Risk'),
                                         ('deleterisk', 'Delete Risk'),
                                         ('canceled', 'Canceled'), ],
                                        string='Endorsement Type', required=True)
    is_canceled = fields.Boolean(string="", )
    customer = fields.Many2one('res.partner', compute='get_policy_info')
    company = fields.Many2one('res.partner', compute='get_policy_info')
    policy_no = fields.Char(compute='get_policy_info')
    state_track = fields.Char(compute='get_policy_info')
    total_sum_insured = fields.Float(compute='get_policy_info')
    diff_sum_insured = fields.Float(compute='get_policy_info')
    t_permimum = fields.Float(compute='get_policy_info')
    gross_perimum = fields.Float(compute='get_policy_info')
    rate = fields.Float(compute='get_policy_info')

    @api.one
    def get_policy_info(self):
        x = self.env["policy.broker"].search([('id', '=', self.number_policy.id)])
        self.customer = x.customer.id
        self.company = x.company.id
        self.policy_no = x.std_id
        self.state_track = x.state_track
        self.total_sum_insured = x.total_sum_insured
        self.diff_sum_insured = x.diff_sum_insured
        self.gross_perimum = x.gross_perimum
        self.t_permimum = x.t_permimum
        self.rate = x.rate

    @api.onchange('number_policy')
    def onchange_risk_id_pol(self):
        # last_confirmed_edit = self.env['policy.broker'].search(
        #     ['&','&',('std_id', '=', self.number_policy.std_id),('active','=','True'),('policy_status', '=','approved')],
        # )
        self.last_policy = self.number_policy.id
        print(self.last_policy.gross_perimum, "dkkkkkkkkkkdkdkdk")

    @api.multi
    def create_endorsement(self):
        form_view = self.env.ref('smart_policy.policy_form_view')
        risk = self.env["policy.risk"].search([('id', 'in', self.last_policy.new_risk_ids.ids)])
        records_risks = []
        for rec in risk:
            object = (
                0, 0, {'risk_description': rec.risk_description,
                       'car_tybe': rec.car_tybe.id, 'motor_cc': rec.motor_cc, 'year_of_made': rec.year_of_made,
                       'model': rec.model.id, 'Man': rec.Man.id, 'chassis_no': rec.chassis_no, 'plate_no': rec.plate_no,
                       'engine': rec.engine, 'license_expire_date': rec.license_expire_date,
                       'name': rec.name, 'DOB': rec.DOB, 'job': rec.job.id,
                       'From': rec.From, 'To': rec.To, 'cargo_type': rec.cargo_type.id,
                       'address': rec.address, 'type': rec.type.id,
                       'group_category': rec.group_category, 'group_count': rec.group_count,
                       'group_premium': rec.group_premium,
                       'sum_insured': rec.sum_insured, 'limitone': rec.limitone,
                       'limittotal': rec.limittotal, 'proj_name': rec.proj_name,
                       'proj_start_date': rec.proj_start_date,
                       'proj_end_date': rec.proj_end_date, 'proj_description': rec.proj_description,

                       })
            records_risks.append(object)
            print(records_risks)
        if self.endorsement_type == 'canceled':
            print("hena hena hena")
            self.is_canceled = True
        self.converted = True
        if self.last_policy.ins_type == 'Group':
            return {
                'name': ('Policy'),
                'view_type': 'form',
                'view_mode': 'form',
                'views': [(form_view.id, 'form')],
                'res_model': 'policy.broker',
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {
                    'default_endorsement_no': self.endorsement_no,
                    'default_endorsement_type': self.endorsement_type,
                    'default_endorsement_reason': self.reasonedit,
                    'default_end_date': self.last_policy.end_date,
                    'default_std_id': self.last_policy.std_id,
                    'default_company': self.last_policy.company.id,
                    "default_discount_party": self.last_policy.discount_party.id,
                    'default_ins_type': self.last_policy.ins_type,
                    'default_line_of_bussines': self.last_policy.line_of_bussines.id,
                    'default_line_related_name': self.line_related_name,
                    'default_product_policy': self.last_policy.product_policy.id,
                    'default_insurance_type': self.last_policy.insurance_type,
                    'default_customer': self.last_policy.customer.id,
                    'default_issue_date': self.last_policy.issue_date,
                    'default_start_date': self.last_policy.start_date,
                    'default_app_date': self.last_policy.app_date,
                    'default_branch': self.last_policy.branch.id,
                    'default_sales_person1': self.last_policy.sales_person1.id,
                    'default_currency_id': self.last_policy.currency_id.id,
                    'default_benefit': self.last_policy.benefit,
                    'default_term': self.last_policy.term,
                    'default_no_years': self.last_policy.no_years,
                    'default_gross_perimum_discountless': self.last_policy.gross_perimum_discountless,
                    'default_discounts': self.last_policy.discounts,
                    'default_last_start_date': self.last_policy.start_date,
                    'default_last_end_date': self.last_policy.end_date,
                    'default_risks_method': self.last_policy.risks_method,
                    'default_new_risk_ids': records_risks,
                    'default_is_endorsement': True,
                    'default_last_policy_id': self.last_policy.id,
                    # 'default_t_permimum': self.last_policy.t_permimum,
                    # 'default_gross_helper': self.last_policy.gross_perimum_discountless,
                    # 'default_net_helper': self.last_policy.t_permimum,
                    'default_state_track': 'Endorsement'

                },
            }
        else:
            return {
                'name': ('Policy'),
                'view_type': 'form',
                'view_mode': 'form',
                'views': [(form_view.id, 'form')],
                'res_model': 'policy.broker',
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {
                    "default_discount_party": self.last_policy.discount_party.id,
                    'default_end_date': self.last_policy.end_date,
                    'default_in_favour': self.last_policy.in_favour.id,
                    'default_endorsement_no': self.endorsement_no,
                    'default_endorsement_type': self.endorsement_type,
                    'default_endorsement_reason': self.reasonedit,
                    'default_std_id': self.last_policy.std_id,
                    'default_company': self.last_policy.company.id,
                    'default_ins_type': self.last_policy.ins_type,
                    'default_line_of_bussines': self.last_policy.line_of_bussines.id,
                    'default_product_policy': self.last_policy.product_policy.id,
                    'default_insurance_type': self.last_policy.insurance_type,
                    'default_customer': self.last_policy.customer.id,
                    'default_issue_date': self.last_policy.issue_date,
                    'default_start_date': self.last_policy.start_date,
                    'default_last_start_date': self.last_policy.start_date,
                    'default_last_end_date': self.last_policy.end_date,
                    'default_app_date': self.last_policy.app_date,
                    'default_branch': self.last_policy.branch.id,
                    'default_sales_person1': self.last_policy.sales_person1.id,
                    'default_currency_id': self.last_policy.currency_id.id,
                    'default_benefit': self.last_policy.benefit,
                    'default_term': self.last_policy.term,
                    'default_no_years': self.last_policy.no_years,
                    'default_is_endorsement': True,
                    'default_is_canceled': self.is_canceled,
                    'default_new_risk_ids': records_risks,
                    'default_last_policy_id': self.last_policy.id,
                    'default_state_track': 'Endorsement'

                },
            }
