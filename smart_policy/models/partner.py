from odoo import models, fields, api
from odoo.exceptions import ValidationError


class inhertResPartner(models.Model):
    _inherit = 'res.partner'
    tax = fields.Float(string="Withheld Tax",  required=False, )
    policy_count = fields.Integer(compute='_compute_policy_count')
    brok_inv_count = fields.Integer(compute='_compute_brok_inv_count')
    prem_bill_count = fields.Integer(compute='_compute_prem_bill_count')
    branch_ids = fields.One2many('insurance.setup', 'partner_id', string='Branches')
    key_account = fields.Boolean(string='Key Account')

    manager = fields.Many2one('res.partner', string='Manager', domain="[('agent','=',1)]")
    layer_type = fields.Selection(string="Layer",
                                  selection=[('l1', 'L1'), ('l2', 'L2'), ('l3', 'L3'), ('l4', 'L4'), ('l5', 'L5'),
                                             ('l6', 'L6'), ], required=False, )

    # @api.one
    # def _get_channel(self):
    #     user = self.env["res.users"].search([('partner_id', '=', self.id)])
    #     sales_channel = self.env["crm.team"].search([('member_ids', '=', user.id)])
    #     self.sales_channel = sales_channel

    @api.one
    def _set_channel(self):

        return True

    @api.one
    def _compute_policy_count(self):
        if self.customer == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.policy_count = self.env['policy.broker'].search_count(
                    [('customer', '=', self.id)])

        elif self.insurer_type == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.policy_count = self.env['policy.broker'].search_count(
                    [('company', '=', self.id)])
        elif self.agent == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.policy_count = self.env['policy.broker'].search_count(
                    [('salesperson', '=', self.id)])
        elif self.key_account == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.policy_count = self.env['policy.broker'].search_count(
                    [('discount_party', '=', self.id)])

    @api.multi
    def show_partner_policies(self):
        if self.customer == 1:
            return {
                'name': ('Policy'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'policy.broker',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_customer': self.id},
                'domain': [('customer', '=', self.id)]
            }
        elif self.insurer_type == 1:
            return {
                'name': ('Policy'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'policy.broker',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_company': self.id},
                'domain': [('company', '=', self.id)]
            }
        elif self.agent == 1:
            return {
                'name': ('Policy'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'policy.broker',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                # 'view_id': [(self.env.ref('smart_policy.policy_tree_view').id), 'tree'],
                'context': {'default_salesperson': self.id, 'create': False},
                'domain': [('salesperson', '=', self.id)]
            }
        elif self.key_account == 1:
            return {
                'name': ('Policy'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'policy.broker',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                # 'view_id': [(self.env.ref('smart_policy.policy_tree_view').id), 'tree'],
                'context': {'default_salesperson': self.id, 'create': False},
                'domain': [('discount_party', '=', self.id)]
            }

    @api.one
    def _compute_brok_inv_count(self):
        if self.insurer_type == 1:
            for partner in self:
                operator = 'child_of' if partner.is_company else '='
                partner.brok_inv_count = self.env['account.invoice'].search_count(
                    [('partner_id', operator, partner.id), ('insured_invoice', '=', 'brokerage')])

    @api.one
    def _compute_prem_bill_count(self):
        if self.insurer_type == 1:
            for partner in self:
                operator = 'child_of' if partner.is_company else '='
                partner.prem_bill_count = self.env['account.invoice'].search_count(
                    [('partner_id', operator, partner.id), ('insured_invoice', '=', 'insurer_bill')])

    @api.multi
    def show_brok_inv(self):
        if self.insurer_type == 1:
            return {
                'name': ('Brokerage Invoices'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.invoice',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_partner_id': self.id},
                'domain': [('type', '=', 'out_invoice'), ('partner_id', '=', self.id),
                           ('insured_invoice', '=', 'brokerage')]
            }

    @api.multi
    def show_prem_bill(self):
        if self.insurer_type == 1:
            return {
                'name': ('Premium Bills'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.invoice',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_partner_id': self.id},
                'domain': [('partner_id', '=', self.id), ('type', '=', 'in_invoice'),
                           ('insured_invoice', '=', 'insurer_bill')]
            }

    @api.multi
    def partner_report_policy(self):
        if self.insurer_type:
            # proposal = self.env['proposal.opp.bb'].search([('Company', '=', self.id)]).ids
            policy = self.env['policy.broker'].search([('company', '=', self.id)])
            return policy
        elif self.customer:
            # proposal = self.env['proposal.opp.bb'].search([('Company', '=', self.id)]).ids
            policy = self.env['policy.broker'].search([('customer', '=', self.id)])
            return policy
        elif self.agent:
            print("***************")
            policy = self.env['policy.broker'].search([('salesperson', '=', self.id)])
            return policy
