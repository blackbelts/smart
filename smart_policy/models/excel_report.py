from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import xlsxwriter
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from io import BytesIO
import io


class excelReport(models.Model):
    _name = "excel.report.task"
    _rec_name = 'excel_sheet_name'
    is_confirmed = fields.Boolean(string="", compute='is_confirm')
    my_file = fields.Binary(string="Get Your File")
    excel_sheet_name = fields.Char(string='Name', size=64)

    @api.one
    @api.depends('my_file')
    def is_confirm(self):
        if self.my_file:
            self.is_confirmed = True
        return True

    # @api.multi
    # def generate_task(self):
    #     context = dict(self._context or {})
    #     active_ids = context.get('active_ids', []) or []
    #     self.excel_sheet_name = 'report_task.xlsx'
    #     # s=str(self.path)+'/'+'installment.xlsx'
    #     # print(s)
    #     self.excel_sheet_name = 'Policies.xlsx'
    #     output = io.BytesIO()
    #     workbook = xlsxwriter.Workbook(output)
    #
    #     worksheet = workbook.add_worksheet()
    #     # Widen the first column to make the text clearer.
    #     worksheet.set_column('A:A', 15)
    #     worksheet.set_column('B:B', 15)
    #     worksheet.set_column('C:C', 15)
    #     worksheet.set_column('D:D', 15)
    #     worksheet.set_column('E:E', 15)
    #     worksheet.set_column('F:F', 15)
    #     worksheet.set_column('G:G', 15)
    #     worksheet.set_column('H:H', 15)
    #     worksheet.set_column('I:I', 15)
    #     worksheet.set_column('J:J', 15)
    #     worksheet.set_column('K:K', 15)
    #     worksheet.set_column('L:L', 15)
    #     worksheet.set_column('M:M', 15)
    #     worksheet.set_column('N:N', 15)
    #     worksheet.set_column('O:O', 15)
    #     worksheet.set_column('P:P', 15)
    #     worksheet.set_column('Q:Q', 15)
    #     worksheet.set_column('R:R', 15)
    #
    #     worksheet.write('A1', '  اسم العميل')
    #     worksheet.write('B1', 'العنوان')
    #     worksheet.write('C1', 'رقم الوثيقة')
    #     worksheet.write('D1', 'نوع التامين')
    #     worksheet.write('E1', ' تاريخ الاصدار')
    #     worksheet.write('F1', 'شركة التامين')
    #     worksheet.write('G1', 'مبلغ العمولة')
    #     worksheet.write('H1', 'تاريخ التجديد')
    #     worksheet.write('I1', 'اسم وسيط التامين')
    #     worksheet.write('J1', 'مبلغ التأمين')
    #     worksheet.write('K1', ' صافي القسط')
    #     worksheet.write('L1', 'اجمالي القسط')
    #     worksheet.write('M1', 'نوع القسط')
    #     worksheet.write('N1', 'ملحق')
    #     # worksheet.write('O1', 'رقم الشاسية')
    #     worksheet.write('O1', ' بيانات اخرى')
    #     worksheet.write('P1', 'الماركة')
    #     worksheet.write('Q1', 'الموديل')
    #
    #     # file_name = 'temp'
    #     i = 2
    #     for record in self.env['policy.broker'].browse(active_ids):
    #         worksheet.write('A' + str(i), str(record.customer.name))
    #         worksheet.write('B' + str(i), str(record.customer.street))
    #         worksheet.write('C' + str(i), str(record.std_id))
    #         worksheet.write('D' + str(i), str(record.insurance_type))
    #         worksheet.write('E' + str(i), str(record.start_date))
    #         worksheet.write('F' + str(i), str(record.company.name))
    #         worksheet.write('G' + str(i), str(record.total_brokerages))
    #         worksheet.write('H' + str(i), str(record.end_date))
    #         worksheet.write('I' + str(i), str(record.discount_party.name))
    #         worksheet.write('J' + str(i), str(record.total_sum_insured))
    #         worksheet.write('K' + str(i), str(record.t_permimum))
    #         worksheet.write('L' + str(i), str(record.gross_perimum))
    #         worksheet.write('M' + str(i), str(record.term))
    #         worksheet.write('N' + str(i), str(record.endorsement_no))
    #         worksheet.write('O' + str(i), str(record.new_risk_ids.add_info))
    #         worksheet.write('P' + str(i), str(record.new_risk_ids.Man.setup_id))
    #         worksheet.write('Q' + str(i), str(record.new_risk_ids.model.setup_id))
    #         if record.check_item == "vehicle":
    #             worksheet.write('R1', 'رقم الشاسية')
    #             worksheet.write('R' + str(i), str(record.new_risk_ids.chassis_no))
    #
    #         # worksheet.write('I' + str(i), str(record.ins_product.product_name))
    #         i += 1
    #     workbook.close()
    #
    #     output.seek(0)
    #     self.write({'my_file': base64.encodestring(output.getvalue())})
    #     print(record.std_id)
    #     return {
    #         "type": "ir.actions.do_nothing",
    #     }


class excelReportIssue(models.Model):
    _name = "excel.report.issue"
    # _rec_name = 'excel_sheet_name'
    # is_confirmed = fields.Boolean(string="", compute='is_confirm')
    # my_file = fields.Binary(string="Get Your File")
    # excel_sheet_name = fields.Char(string='Name', size=64)
    company = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]")
    date1 = fields.Date(string="Insurance Effective From", copy=True, default=datetime.today(), required=True)
    date2 = fields.Date(string="Insurance Effective To", default=datetime.today(), required=True)
    # app_from = fields.Date(string="Issued From", copy=True, default=datetime.today(), required=True)
    # app_to = fields.Date(string="Issued To", default=datetime.today(), required=True)
    post_date_from = fields.Date(string="Approved From", copy=True, default=datetime.today(), required=True)
    post_date_to = fields.Date(string="Approved To", default=datetime.today(), required=True)
    policy_status = fields.Selection([('pending', 'Pending'),
                                      ('approved', 'Approved'),
                                      ('canceled', 'Canceled'), ],
                                     'Status')
    discount_party = fields.Many2one('res.partner', 'Key Account',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     copy=True)
    line_of_bussines = fields.Many2one('insurance.line.business', string='Line of business', copy=True)
    insurance_type = fields.Selection([('Life', 'Life'),
                                       ('General', 'General'),
                                       ('Health', 'Health'), ],
                                      'Insurance Type', track_visibility='onchange', copy=True, default='General',
                                      )
    currency_id = fields.Many2one("res.currency", "Currency", copy=True,
                                  default=lambda self: self.env.user.company_id.currency_id)

    @api.one
    @api.depends('my_file')
    def is_confirm(self):
        if self.my_file:
            self.is_confirmed = True
        return True

    @api.multi
    def generate_production_preview(self):
        if self.line_of_bussines:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('write_date', '>=', self.post_date_from),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('write_date', '<=', self.post_date_to)]).ids
        else:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('write_date', '>=', self.post_date_from),
                 # ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('write_date', '<=', self.post_date_to)]).ids

        return self.env['policy.broker'].browse(active_ids)


    @api.multi
    def generate_excel_issue(self):
        if self.company and self.policy_status and self.discount_party and self.line_of_bussines and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 ('company', '=', self.company.id),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('policy_status', '=', self.policy_status), ('discount_party', '=', self.discount_party.id)]).ids

        elif self.policy_status and self.discount_party and self.line_of_bussines and not self.company and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('currency_id', '=', self.currency_id.id),
                 ('ins_type', '=', 'Individual'),
                 ('policy_status', '=', self.policy_status), ('discount_party', '=', self.discount_party.id)]).ids
        elif self.company and self.discount_party and self.line_of_bussines and not self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('currency_id', '=', self.currency_id.id),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 ('company', '=', self.company.id), ('discount_party', '=', self.discount_party.id)]).ids
        elif self.company and self.policy_status and self.line_of_bussines and not self.discount_party and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 ('company', '=', self.company.id), ('policy_status', '=', self.policy_status)]).ids
        elif self.company and self.policy_status and not self.line_of_bussines and self.discount_party and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('discount_party', '=', self.discount_party.id),
                 ('currency_id', '=', self.currency_id.id),
                 ('ins_type', '=', 'Individual'),
                 ('company', '=', self.company.id), ('policy_status', '=', self.policy_status)]).ids

        elif self.company and self.line_of_bussines and not self.policy_status and not self.discount_party and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 ('company', '=', self.company.id)]).ids
        elif self.policy_status and self.line_of_bussines and not self.company and not self.discount_party and self.currency_id :
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('currency_id', '=', self.currency_id.id),
                 ('ins_type', '=', 'Individual'),
                 ('policy_status', '=', self.policy_status)]).ids
        elif self.discount_party and self.line_of_bussines and not self.policy_status and not self.company and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.discount_party and self.company and not self.policy_status and not self.line_of_bussines and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.discount_party and self.policy_status and not self.line_of_bussines and not self.company and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('currency_id', '=', self.currency_id),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status),
                 ('ins_type', '=', 'Individual'),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.policy_status and self.company and not self.line_of_bussines and not self.discount_party and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('policy_status', '=', self.policy_status)]).ids

        elif self.policy_status and not self.company and not self.line_of_bussines and not self.discount_party and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status)]).ids
        elif self.company and not self.policy_status and not self.line_of_bussines and not self.discount_party and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id)]).ids
        elif self.discount_party and not self.company and not self.line_of_bussines and not self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 # ('issue_date', '<=', self.app_to),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.line_of_bussines and not self.company and not self.policy_status and not self.discount_party and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id)]).ids

        elif self.company and self.policy_status and self.discount_party and self.line_of_bussines and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 ('company', '=', self.company.id),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('policy_status', '=', self.policy_status), ('discount_party', '=', self.discount_party.id)]).ids

        elif self.policy_status and self.discount_party and self.line_of_bussines and not self.company and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 # ('currency_id', '=', self.currency_id),
                 ('ins_type', '=', 'Individual'),
                 ('policy_status', '=', self.policy_status), ('discount_party', '=', self.discount_party.id)]).ids
        elif self.company and self.discount_party and self.line_of_bussines and not self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 ('company', '=', self.company.id), ('discount_party', '=', self.discount_party.id)]).ids
        elif self.company and self.policy_status and self.line_of_bussines and not self.discount_party and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 ('company', '=', self.company.id), ('policy_status', '=', self.policy_status)]).ids
        elif self.company and self.policy_status and not self.line_of_bussines and self.discount_party and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('discount_party', '=', self.discount_party.id),
                 # ('currency_id', '=', self.currency_id),
                 ('ins_type', '=', 'Individual'),
                 ('company', '=', self.company.id), ('policy_status', '=', self.policy_status)]).ids

        elif self.company and self.line_of_bussines and not self.policy_status and not self.discount_party and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 ('company', '=', self.company.id)]).ids
        elif self.policy_status and self.line_of_bussines and not self.company and not self.discount_party and not self.currency_id :
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 # ('currency_id', '=', self.currency_id),
                 ('ins_type', '=', 'Individual'),
                 ('policy_status', '=', self.policy_status)]).ids
        elif self.discount_party and self.line_of_bussines and not self.policy_status and not self.company and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.discount_party and self.company and not self.policy_status and not self.line_of_bussines and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.discount_party and self.policy_status and not self.line_of_bussines and not self.company and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status),
                 ('ins_type', '=', 'Individual'),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.policy_status and self.company and not self.line_of_bussines and not self.discount_party and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('policy_status', '=', self.policy_status)]).ids

        elif self.policy_status and not self.company and not self.line_of_bussines and not self.discount_party and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status)]).ids
        elif self.company and not self.policy_status and not self.line_of_bussines and not self.discount_party and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id)]).ids
        elif self.discount_party and not self.company and not self.line_of_bussines and not self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '<=', self.app_to),
                 ('discount_party', '=', self.discount_party.id)]).ids
        elif self.line_of_bussines and not self.company and not self.policy_status and not self.discount_party and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('ins_type', '=', 'Individual'),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('line_of_bussines', '=', self.line_of_bussines.id)]).ids
        elif not self.line_of_bussines and not self.company and not self.policy_status and not self.discount_party and  self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('ins_type', '=', 'Individual'),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ]).ids
        else:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 ('ins_type', '=', 'Individual'),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to)
                 ]).ids


        print(active_ids)
        return self.env['policy.broker'].browse(active_ids)



    @api.multi
    def corporate_business_issue(self):
        if self.company and self.line_of_bussines and self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('ins_type', '=', 'Group'),
                 ('policy_status', '=', self.policy_status),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ]).ids

        elif self.line_of_bussines and not self.company and self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 ('currency_id', '=', self.currency_id.id),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status),
                 ('line_of_bussines', '=', self.line_of_bussines.id)]).ids
        elif not self.line_of_bussines and self.company and self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('currency_id', '=', self.currency_id.id),
                 ('policy_status', '=', self.policy_status),
                 ('company', '=', self.company.id)]).ids
        elif self.company and self.line_of_bussines and not self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('ins_type', '=', 'Group'),
                 # ('policy_status', '=', self.policy_status),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ]).ids

        elif self.line_of_bussines and not self.company and not self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('currency_id', '=', self.currency_id),
                 # ('policy_status', '=', self.policy_status),
                 ('line_of_bussines', '=', self.line_of_bussines.id)]).ids
        elif not self.line_of_bussines and self.company and not self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 # ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('policy_status', '=', self.policy_status),
                 ('company', '=', self.company.id)]).ids
        elif not self.line_of_bussines and not self.company and not self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 ('currency_id', '=', self.currency_id.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('policy_status', '=', self.policy_status),
                 # ('company', '=', self.company.id)
                 ]).ids


        elif not self.line_of_bussines and self.company and not self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('policy_status', '=', self.policy_status),
                 ('company', '=', self.company.id)]).ids
        elif self.company and self.line_of_bussines and self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('ins_type', '=', 'Group'),
                 ('policy_status', '=', self.policy_status),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ]).ids

        elif self.line_of_bussines and not self.company and self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status),
                 ('line_of_bussines', '=', self.line_of_bussines.id)]).ids
        elif not self.line_of_bussines and self.company and self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('currency_id', '=', self.currency_id),
                 ('policy_status', '=', self.policy_status),
                 ('company', '=', self.company.id)]).ids
        elif self.company and self.line_of_bussines and not self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('ins_type', '=', 'Group'),
                 # ('policy_status', '=', self.policy_status),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('company', '=', self.company.id),
                 ('line_of_bussines', '=', self.line_of_bussines.id),
                 ]).ids

        elif self.line_of_bussines and not self.company and not self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('currency_id', '=', self.currency_id),
                 # ('policy_status', '=', self.policy_status),
                 ('line_of_bussines', '=', self.line_of_bussines.id)]).ids
        elif not self.line_of_bussines and self.company and not self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 # ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('policy_status', '=', self.policy_status),
                 ('company', '=', self.company.id)]).ids
        elif not self.line_of_bussines and not self.company and not self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('policy_status', '=', self.policy_status),
                 # ('company', '=', self.company.id)
                 ]).ids


        elif not self.line_of_bussines and self.company and not self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '<=', self.date2),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 # ('policy_status', '=', self.policy_status),
                 ('company', '=', self.company.id)]).ids


        print(active_ids)
        return self.env['policy.broker'].browse(active_ids)
    @api.multi
    def print_policy1(self):
        return self.env.ref('smart_policy.policy_report_issue').report_action(self)

    @api.multi
    def print_policy2(self):
        return self.env.ref('smart_policy.policy_corporate').report_action(self)


    # //////Sql  ////////
    # produits = self._cr.execute(
    #     "SELECT std_id FROM policy_broker WHERE start_date >=%s AND end_date <= %s AND discount_party=%d",
    #     (self.date1, self.date2, self.discount_party.id))
    # print(self._cr.fetchall())

    # @api.multi
    # def close(self):
    #     return 1
    @api.multi
    def individual_live_business(self):


        if self.company and self.policy_status and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=',  'Individual Life'),
                 ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 ('currency_id', '=', self.currency_id.id),
                 ('company', '=', self.company.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status),
                 ]).ids

        elif self.policy_status and not self.company and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=', 'Individual Life'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 ('currency_id', '=', self.currency_id.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status),]).ids
        elif not self.policy_status and self.company and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=', 'Individual Life'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 ('currency_id', '=', self.currency_id.id),
                 ('company', '=', self.company.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ]).ids
        elif not self.policy_status and not self.company and self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=',  'Individual Life'),
                 ('currency_id', '=', self.currency_id.id),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('company', '=', self.company.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ]).ids
        elif self.company and self.policy_status and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=', 'Individual Life'),
                 ('ins_type', '=', 'Group'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('currency_id', '=', self.currency_id),
                 ('company', '=', self.company.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status),
                 ]).ids

        elif self.policy_status and not self.company and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=', 'Individual Life'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('currency_id', '=', self.currency_id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ('policy_status', '=', self.policy_status), ]).ids
        elif not self.policy_status and self.company and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=', 'Individual Life'),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('currency_id', '=', self.currency_id),
                 ('company', '=', self.company.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ]).ids
        elif not self.policy_status and not self.company and not self.currency_id:
            active_ids = self.env["policy.broker"].search(
                ['|', ('active', '=', True), ('active', '=', False),
                 ('line_of_bussines', '=', 'Individual Life'),
                 # ('currency_id', '=', self.currency_id),
                 ('start_date', '>=', self.date1),
                 ('start_date', '<=', self.date2),
                 # ('company', '=', self.company.id),
                 # ('issue_date', '>=', self.app_from),
                 # ('issue_date', '<=', self.app_to),
                 ]).ids
            print("ssssssssssss")
        print(active_ids)
        return self.env['policy.broker'].browse(active_ids)


    # @api.multi
    # def print_policy1(self):
    #     return self.env.ref('smart_policy.policy_report_issue').report_action(self)
    #
    # @api.multi
    # def print_policy2(self):
    #     return self.env.ref('smart_policy.policy_corporate').report_action(self)

    @api.multi
    def print_policy3(self):
        return self.env.ref('smart_policy.policy_individual_life').report_action(self)


    @api.multi
    def print_production(self):
        return self.env.ref('smart_policy.policy_report_production').report_action(self)
