from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import xlsxwriter
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models,_
from odoo.exceptions import UserError
from io import BytesIO

class excelReport(models.TransientModel):
    _name = "report.policy.excel"

    @api.multi
    def generate_excel(self):

        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        workbook = xlsxwriter.Workbook('policies.xlsx')
        worksheet = workbook.add_worksheet()
        worksheet.write('A1', 'Policy Number')
        worksheet.write('B1', 'Customer')
        worksheet.write('C1', 'Discount Party')
        worksheet.write('D1', 'Insurer')
        worksheet.write('E1', 'Issue Date')
        worksheet.write('F1', 'Effective From')
        worksheet.write('G1', 'Effective To')
        worksheet.write('H1', 'Product')
        worksheet.write('I1', 'Total Sum Insured')
        worksheet.write('J1', 'Rate')
        worksheet.write('K1', 'Net Premium')
        worksheet.write('L1', 'Gross Premium')
        worksheet.write('M1', 'State')

        i=2
        for record in self.env['policy.broker'].browse(active_ids):
            worksheet.set_column('A:A', 30)
            worksheet.write('A'+str(i), str(record.std_id))
            worksheet.write('B'+str(i), str(record.customer.name))
            worksheet.write('C'+str(i), str(record.discount_party.name))
            worksheet.write('D'+str(i), str(record.company.name))
            worksheet.write('E'+str(i), str(record.issue_date))
            worksheet.write('F'+str(i), str(record.start_date))
            worksheet.write('G'+str(i), str(record.end_date))
            worksheet.write('H'+str(i), str(record.product_policy.product_name))
            worksheet.write('I'+str(i), str(record.total_sum_insured))
            worksheet.write('J'+str(i), str(record.rate))
            worksheet.write('K'+str(i), str(record.t_permimum))
            worksheet.write('L'+str(i), str(record.gross_perimum))
            worksheet.write('M'+str(i), str(record.policy_status))
            i+=1
        workbook.close()