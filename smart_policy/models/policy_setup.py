import datetime
from datetime import timedelta, datetime

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class Policy_Info(models.Model):
    _name = "insurance.line.business"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'line_of_business'
    claim_action = fields.One2many('product.claim.action', 'product')
    insurance_type = fields.Selection([('Life', 'Life'),
                                       ('General', 'General'),
                                       ('Health', 'Health'), ],
                                      'Insurance Type', track_visibility='onchange', required=True)
    line_of_business = fields.Char(string='Line of Business', required=True)
    object = fields.Selection([('person', 'Person'),
                               ('vehicle', 'Vehicle'),
                               ('cargo', 'Cargo'),
                               ('location', 'Location'),
                               ('Project', 'Project')],
                              'Insured Object', track_visibility='onchange', required=True)
    desc = fields.Char(string='Description')
    # income_account = fields.Many2one('account.account', 'Income Account', required=True,
    #                                  default=lambda self: self.env['account.account'].search(
    #                                      [('name', '=', 'Brokerage Commission')]))
    # expense_account = fields.Many2one('account.account', 'Expense Account', required=True,
    #                                   default=lambda self: self.env['account.account'].search(
    #                                       [('name', '=', 'Agent Commission')]))

    _sql_constraints = [
        ('business_unique', 'unique(insurance_type,line_of_business,object)', 'Line of Business already exists!')]


class Product(models.Model):
    _name = 'insurance.product'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'product_name'

    product_name = fields.Char('Product Name', required=True)
    insurer = fields.Many2one('res.partner', string="Insurer", domain="[('insurer_type','=',1)]")
    line_of_bus = fields.Many2one('insurance.line.business', 'Line of Business')
    brokerage = fields.Many2many('product.brokerage', string='Brokerage',
                                 domain="[('insurer','=',insurer)]")
    commission_per = fields.Float(string='Commission Percentage')
    name_cover_id = fields.Many2one("name.cover")

    _sql_constraints = [
        ('product_unique', 'unique(insurer,product_name,line_of_bus)', 'Product already exists!')]


class claimAction(models.Model):
    _name = 'product.claim.action'

    action = fields.Char('Claim Action')
    product = fields.Many2one('insurance.product', ondelete='cascade', index=True)


class coverage(models.Model):
    _name = 'insurance.product.coverage'
    _rec_name = "Name"

    Name = fields.Char('Cover Name')
    defaultvalue = fields.Float('Sum Insured')
    required = fields.Boolean('Required')
    deductible = fields.Char('Deductible')
    limitone = fields.Float('Limit in One')
    limittotal = fields.Float('Limit in Total')
    readonly = fields.Boolean('Read Only')
    product_id = fields.Many2one('insurance.product', ondelete='cascade', index=True)
    lop_id = fields.Many2one('insurance.line.business', string='Line of Business')

    _sql_constraints = [
        ('Name_unique', 'unique(Name)', 'Cover Name already exists!')]


class Brokerage(models.Model):
    _name = 'insurance.product.brokerage'

    datefrom = fields.Date('Date from')
    dateto = fields.Date('Date to')
    basic_commission = fields.Float('Commission')
    complementary_commission = fields.Float('Complementary Commission')
    early_collection = fields.Float('Early Collection Commission')
    fixed_commission = fields.Monetary(default=0.0, currency_field='company_currency_id', string='Fixed Commission')
    company_currency_id = fields.Many2one('res.currency', related='product_id.insurer.currency_id',
                                          string="Company Currency", readonly=True, store=True)
    product_id = fields.Many2one('insurance.product')

    @api.constrains('datefrom')
    def _constrain_date(self):
        for record in self:
            if record.dateto < record.datefrom:
                raise ValidationError('Error! Date to Should be After Date from')


class insuranceSetup(models.Model):
    _name = 'insurance.setup'
    _rec_name = 'setup_id'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    setup_id = fields.Char(string='ID')
    setup_key = fields.Selection([('closs', 'Cause of Loss'),
                                  ('nloss', 'Nature of Loss'),
                                  ('goods', 'Type of Goods'),
                                  ('setltype', 'Settlement Type'),
                                  ('state', 'Claim Status'),
                                  ('clmitem', 'Claim Item'),
                                  ('branch', 'Insurer Branch'),
                                  ('vehicletype', 'Vehicle Type'),
                                  ('industry', 'Industry'),
                                  ('man', 'Maker'),
                                  ('model', 'Car Model'),
                                  ('jobtype', 'Jobs'),
                                  ('cargo', 'Cargo Type'),
                                  ('location', 'Location Type'),
                                  ('region', 'Region'),
                                  ('leadsource', 'Lead Source'),
                                  ('Broker Commission', 'Broker Commission'),
                                  ],
                                 'KEY', track_visibility='onchange', required=True)

    @api.onchange('setup_key')
    def broker_comm_name(self):
        if self.setup_key == 'Broker Commission':
            self.setup_id = 'Broker Commission'

    broker_account = fields.Many2one(comodel_name="account.account", string="Account", required=False, )
    parent_id = fields.Many2one(
        'insurance.setup',
        string='Parent Maker',
        ondelete='restrict',
        index=True, domain="[('setup_key','=','man')]")
    child_ids = fields.One2many(
        'insurance.setup', 'parent_id',
        string='Child Categories')

    partner_id = fields.Many2one('res.partner', string='Insurer', domain="[('insurer_type','=',1)]")

    @api.constrains('parent_id')
    def _check_hierarchy(self):
        if not self._check_recursion():
            raise ValidationError(
                'Error! You cannot create recursive categories.')

    _sql_constraints = [
        ('setup_id_unique', 'unique(setup_key,setup_id)', 'ID already exists!')]


class NewModuleclean(models.Model):
    _name = 'data.clean.setup'

    clean_option_maker=fields.Boolean('Clean Maker')
    clean_option_model=fields.Boolean('Clean Model')

    maker = fields.Many2one('insurance.setup', string='Maker', domain="[('setup_key','=','man')]")
    model=fields.Many2one('insurance.setup', string='Model',domain="[('setup_key','=','model'),('parent_id','=',maker)]")

    @api.multi
    def replace_setup(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        if self.clean_option_maker:
            for rec in active_ids:
               print(8888888888888888888888888888888888888)

               for record in self.env['policy.risk'].search( [('Man', '=', rec)]):
                   if self.maker:
                       record.write({'Man': self.maker.id})
                       record._compute_risk_descriptionn()

               for record in self.env['insurance.setup'].search( [('parent_id', '=', rec)]):
                   if self.maker:
                       record.write({'parent_id': self.maker.id})


        if self.clean_option_model:
                for rec in active_ids:
                    print(5555555555)
                    for record in self.env['policy.risk'].search([('model', '=', rec)]):
                        print(record)
                        if self.model:
                            record.write({'model': self.model.id})
                            record._compute_risk_descriptionn()




        # if self.clean_option_customer:
        #     for rec in active_ids:
        #        for record in self.env['policy.broker'].search( [('discount_party', '=', rec)]):
        #            if self.discount_part:
        #                record.write({'discount_party': self.discount_part.id})
        #


        return {'type': 'ir.actions.act_window_close'}

class Brokerage(models.Model):
    _name = 'product.brokerage'
    _rec_name = 'rule'

    datefrom = fields.Date('Date from', required=True)
    dateto = fields.Date('Date to', required=True)
    rule = fields.Selection(string="Rule",
                            selection=[('staff', 'Staff Commission'), ('brokerage', 'Broker Commission'), ],
                            required=True,default='brokerage' )
    # basic_commission = fields.Float('Basic Brokerage')
    issue_commission = fields.One2many(comodel_name="issue.commission", inverse_name="basic_ids", string="",
                                       required=False, )
    production_commission = fields.Float('Production Commission')
    company_currency_id = fields.Many2one('res.currency', related='product_id.insurer.currency_id',
                                          string="Company Currency", readonly=True, store=True)
    product_id = fields.Many2many('insurance.product',
                                  domain="[('insurer','=',insurer)]")
    product_id_staff = fields.Many2many('insurance.product', )
    insurer = fields.Many2one('res.partner', string="Insurer", required=False, domain="[('insurer_type','=',1)]")
    commission_rule = fields.Selection([('basic', 'Commission (Issue)'),
                                        ('collection', 'Collection Commission (Collection)'),
                                        ('production', 'Production Commission'), ], string='Commission Rule',
                                       track_visibility='onchange', required=False, )
    pro_commission_data = fields.One2many('production.brokerage', 'inverse', string='Production Commission')
    amount = fields.Float(string="Percentage", required=False, compute='amount_calculate')
    # amount = fields.Float(string="Percentage",  required=False,compute='amount_calculate' )
    collection_bonus_data = fields.One2many(comodel_name="bonus.data", inverse_name="brokerage_id", string="",
                                            required=False, )
    staff_comm_ids = fields.One2many(comodel_name="layer.data", inverse_name="staff_comm_id", required=False, )
    is_true = fields.Boolean(string="", )

    @api.multi
    def validate_issue(self):
        self.is_true = True

    # @api.constrains('collection_bonus_data')
    # def _constrain_collection_bonus_data(self):
    #     count = 0
    #     for record in self.collection_bonus_data:
    #         count += record.duration
    #         datefrom = fields.Date.from_string(self.datefrom)
    #         dateto = fields.Date.from_string(self.dateto)
    #         difference = dateto.day - datefrom.day
    #     print(difference)
    #     print(count)
    #     if difference + 1 != count:
    #         raise ValidationError('total duration must be equal to the number of days of brokerage !')

    @api.one
    def amount_calculate(self):
        if self.commission_rule == 'basic':

            self.amount = self.basic_commission
        elif self.commission_rule == 'collection':
            for record in self.collection_bonus_data:
                count = 0
                count += record.duration
            self.amount = count
        else:
            self.amount = self.production_commission

    # @api.one
    # def amount_calculate(self):
    #     if self.commission_rule == 'basic':
    #         self.amount=self.basic_commission
    #     elif self.commission_rule == 'collection':
    #         for record in self.collection_bonus_data:
    #             count = 0
    #             count += record.duration
    #         self.amount=count
    #     else:
    #         self.amount = self.production_commission
    @api.constrains('datefrom')
    def _constrain_date(self):
        for record in self:
            if record.dateto < record.datefrom:
                raise ValidationError('Error! Date to Should be After Date from')

    @api.onchange('rule')
    def staff_set(self):
        if self.rule == 'staff':
            self.commission_rule = ''

    @api.constrains('commission_rule')
    def _comm_rule_constraint(self):
        if self.rule == 'brokerage' and not self.commission_rule:
            raise ValidationError('Please Select Brokerage Rule')

    # @api.model
    # def create(self, vals):
    #     if self.commission_rule == 'basic':
    #         self.is_basic=True
    #     elif self.commission_rule == 'collection':
    #         self.is_collbou = True
    #     else:
    #         self.is_prodcomm = True
    #
    #
    #     return super(Brokerage, self).create(vals)

    @api.constrains('product_id', 'datefrom', 'commission_rule')
    def _constrain_brokerage(self):
        print('sssssssss')
        objects = self.env['product.brokerage'].search([('id', '!=', self.id)])
        print('dddddddd')
        print(objects)
        if self.product_id:
            if objects:

                for rec in objects:
                    if rec.rule == 'brokerage':

                        for n in self.product_id:

                            if n.id in rec.product_id.ids:

                                if rec.commission_rule == self.commission_rule:
                                    # if rec.rule == self.rule:
                                    if self.datefrom:
                                        # this is the last step date filter
                                        if (rec.dateto > self.datefrom):
                                            raise ValidationError(
                                                'You have another brokerage for one of your products with the same rule and same commission rule and not expired yet')

                                        else:
                                            # here there's no objects conflict
                                            pass
                                    else:
                                        pass
                                # else:
                                #     pass
                                else:
                                    pass
                            else:
                                pass
        else:
            pass

    # @api.one
    @api.constrains('product_id_staff', 'datefrom')
    def _constrain_staff_brokerage(self):

        objects = self.env['product.brokerage'].search([('id', '!=', self.id)])
        if self.product_id:
            if objects:

                for rec in objects:
                    if rec.rule == 'staff':

                        for n in self.product_id_staff:

                            if n.id in rec.product_id_staff.ids:

                                if rec.rule == self.rule:
                                    if self.datefrom:
                                        # this is the last step date filter
                                        if (rec.dateto > self.datefrom):
                                            raise ValidationError(
                                                'You have another staff commission for one of your products with the same sales channel and not expired yet')

                                        else:
                                            # here there's no objects conflict
                                            pass
                                    else:
                                        pass
                                else:
                                    pass
                            else:
                                pass
                        else:
                            pass
        else:
            pass
        # @api.one

    @api.constrains('datefrom', 'issue_commission')
    def _constrain_issue_brokerage(self):

        objects = self.env['product.brokerage'].search([('id', '!=', self.id)])
        if objects:
            print(objects)
            print('dddddddddddddddddd')
            for rec in objects:
                if rec.commission_rule == self.commission_rule:
                    print('ssss')
                    for n in self.issue_commission:
                        for x in rec.issue_commission:
                            for y in n.product_id:
                                for z in x.product_id:
                                    if y == z:
                                        # if n.product_id == x.product_id:

                                        # if rec.rule == self.rule:
                                        #     if rec.sales_channel == self.sales_channel:
                                        if self.datefrom:
                                            # this is the last step date filter
                                            if (rec.dateto > self.datefrom):
                                                raise ValidationError(
                                                    'You have another issue commission for one of your product and not expired yet')

                                            else:
                                                # here there's no objects conflict
                                                pass
                                        else:
                                            pass
                            #         else:
                            #             pass
                            # #     else:
                            #         pass
                            # else:
                            #     pass


class Data(models.Model):
    _name = 'production.brokerage'

    min = fields.Float(string="Min", required=False)
    max = fields.Float(string="Max", required=False)
    percentage = fields.Float(string="Percentage", required=False)
    inverse = fields.Many2one('product.brokerage', ondelete='cascade', index=True)


class bonus_data(models.Model):
    _name = 'bonus.data'
    duration = fields.Integer(string='Duration', )
    commission = fields.Float(string="Commission %", required=False, )
    collection_fees = fields.Float(string="Collection Fees", required=False, )
    # date_from = fields.Date(string="Date From", required=False, )
    # date_to = fields.Date(string="Date To", required=False, )
    brokerage_id = fields.Many2one(comodel_name="collection_bonus_data", string="", required=False, )


class layer_data(models.Model):
    _name = 'layer.data'

    layer = fields.Selection(string="Layer",
                             selection=[('l1', 'L1'), ('l2', 'L2'), ('l3', 'L3'), ('l4', 'L4'), ('l5', 'L5'),
                                        ('l6', 'L6'), ], required=True, )
    commission = fields.Float(string="Commission", required=True, )
    staff_comm_id = fields.Many2one(comodel_name="product.brokerage", string="", required=False, )
    override_comm = fields.Float(string="Override Commission", required=True, )


class Rating(models.Model):
    _name = 'rating.setup'

    datefrom = fields.Date('Date from')
    dateto = fields.Date('Date to')
    line_of_bus = fields.Many2one('insurance.line.business', 'Line of Business')

    insurer = fields.Many2one('res.partner', string="Insurer", required=True, domain="[('insurer_type','=',1)]")

    customer = fields.Many2one('res.partner', domain="[('customer','=',1)]", string='Customer')

    rating_rule = fields.Selection([('SI', 'SI'),
                                    ('YOM', 'Year Of Made'), ('maker', 'Maker'), ], string='Rating Rule',
                                   track_visibility='onchange', required=True)
    maker_data = fields.One2many('type.maker', 'inverse_maker', string='Motor Maker')
    YOM_data = fields.One2many('type.year', 'inverse_yom', string='Year of made')
    SI_data = fields.One2many('type.sum.insured', 'inverse_si', string='Sum Insured')


class TypeMaker(models.Model):
    _name = 'type.maker'

    maker = fields.Many2one('insurance.setup', string='Maker', domain="[('setup_key','=','man')]")
    model = fields.Many2many('insurance.setup', string='Model',
                             domain="['|',('setup_id','=','model'),('parent_id','=',maker)]")
    rate = fields.Float('Model rate')
    inverse_maker = fields.Many2one('rating.setup')


class TypeYearOfMade(models.Model):
    _name = 'type.year'
    yom = fields.Date('Year of made ')
    rate = fields.Float('Rate')
    inverse_yom = fields.Many2one('rating.setup')


class SumInsuredType(models.Model):
    _name = 'type.sum.insured'
    min = fields.Float('Min Sum Insured')
    max = fields.Float('Max Sum Insured')
    rate = fields.Float('Rate')
    inverse_si = fields.Many2one('rating.setup')


class issueCommission(models.Model):
    _name = 'issue.commission'
    product_id = fields.Many2many("insurance.product", string="Product", required=True,
                                  domain="[('insurer','=',insurer)]")

    basic = fields.Float(string="Basic %", required=False, )
    complementary = fields.Float(string="Complementary %", required=False, )
    transportation = fields.Float(string="Transportation %", required=False, )
    basic_ids = fields.Many2one(comodel_name="product.brokerage", string="", required=False, )
    insurer = fields.Many2one('res.partner', string="Insurer", related='basic_ids.insurer', compute='get_insurer')

    def get_insurer(self):
        objects = self.env['product.brokerage'].search([('id', '=', self.basic_ids)])
        self.insurer = objects.insurer
