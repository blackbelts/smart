import base64
import io

import xlsxwriter

from odoo import api, fields, models
from datetime import datetime
from dateutil.relativedelta import relativedelta
from io import BytesIO
from xlrd import open_workbook


class CreateBills(models.Model):
    _inherit = 'hr.payslip'
    invoice_ids = fields.One2many('account.invoice', 'inverse_id', string='Employee Bill')
    net = fields.Float(string='Net Salary', compute='net_salary_fun')
    basic = fields.Float(string='Basic Salary', compute='basic_salary_fun')
    social = fields.Float(string='Social Insurance', compute='social_fun')
    tax = fields.Float(string='Tax', compute='tax_fun')
    bon_ded = fields.Float(string='Bonus/Deduction')
    salary_advance = fields.Float(string=' Salary Advance')
    comp_share = fields.Float(compute='compute_comp_social')
    target = fields.Float(compute='get_target')
    commission = fields.Float(compute='get_commission')

    # journal_id = fields.Many2one('account.journal', 'Salary Journal', readonly=True, required=True,
    #                              states={'draft': [('readonly', False)]},
    #                              default=lambda self: self.env['account.journal'].search([('name', '=', 'Payroll')],
    #                                                                                      limit=1))

    @api.one
    def compute_comp_social(self):
        for rec in self:
            for x in rec.contract_id.salaryinfo:
                if (x.fromdate <= rec.date_to) and (x.todate >= rec.date_to):
                    if x.category == 'SOCIAL_COMP_SHARE':
                        self.comp_share = x.salary

    @api.one
    def compute_sheet(self):
        for payslip in self:
            opj_s = self.env['crm.team'].search([('member_ids', '=', payslip.employee_id.user_id.id)])
            for rec in opj_s.rules_ids:
                if (rec.date_from >= payslip.date_from) and (rec.date_to <= payslip.date_to):

                    if rec.rule == 'Monthly Target Member':

                        if payslip.target >= rec.value:
                            for x in opj_s.rules_ids:
                                if (x.date_from >= payslip.date_from) and (x.date_to <= payslip.date_to):
                                    if x.rule == 'Target Bonus':
                                        payslip.bonus_target = self.target * x.value
                        else:
                            for x in opj_s.rules_ids:
                                if (x.date_from >= payslip.date_from) and (x.date_to <= payslip.date_to):
                                    if x.rule == 'Target Deduction':
                                        payslip.bonus_target = self.target * x.value
            for x in payslip.struct_id.rule_ids:
                if x.code == 'TARGET':
                    x.amount_fix = payslip.bonus_target

            for rec in payslip.contract_id.salaryinfo:
                if (rec.fromdate >= payslip.date_from) and (rec.todate <= payslip.date_to):
                    if rec.category == 'BASIC':
                        payslip.sal = rec.salary
                    elif rec.category != 'BASIC' and rec.category != 'SOCIAL_EMP_SHARE' and rec.category != 'SOCIAL_COMP_SHARE':
                        payslip.sal = 0.0
            for x in payslip.struct_id.rule_ids:
                if x.code == 'BASIC':
                    x.amount_fix = payslip.sal
            for rec in payslip.contract_id.salaryinfo:
                if (rec.fromdate <= payslip.date_from) and (rec.todate <= payslip.date_to):
                    if rec.category == 'SOCIAL_EMP_SHARE':
                        payslip.sal2 = rec.salary
                    elif rec.category != 'BASIC' and rec.category != 'SOCIAL_EMP_SHARE' and rec.category != 'SOCIAL_COMP_SHARE':
                        payslip.sal2 = 0.0
            for x in payslip.struct_id.rule_ids:
                if x.code == 'SOCIAL':
                    x.amount_fix = payslip.sal2
            for x in payslip.struct_id.rule_ids:
                if x.code == 'ATT':
                    x.amount_fix = self.bon_ded
                else:
                    pass
            for x in payslip.struct_id.rule_ids:
                if x.code == 'ADV':
                    x.amount_fix = self.salary_advance

            for x in payslip.struct_id.rule_ids:
                if x.code == 'COMMISSION':
                    x.amount_fix = self.commission

                else:
                    pass
            number = payslip.number or self.env['ir.sequence'].next_by_code('salary.slip')
            # delete old payslip lines
            payslip.line_ids.unlink()
            # set the list of contract for which the rules have to be applied
            # if we don't give the contract, then the rules to apply should be for all current contracts of the employee
            contract_ids = payslip.contract_id.ids or \
                           self.get_contract(payslip.employee_id, payslip.date_from, payslip.date_to)
            lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]
            payslip.write({'line_ids': lines, 'number': number})
        return True

    @api.multi
    def get_target(self):
        opj_s = self.env['policy.broker'].search(
            [('sales_person1', '=', self.employee_id.user_id.id), ('policy_status', '=', 'approved')])
        channel = self.env['crm.team'].search([('member_ids', '=', self.employee_id.user_id.id)])
        for x in opj_s:
            if (self.date_from <= x.start_date) and (self.date_to >= x.start_date):
                for y in channel.rules_ids:
                    if y.rule == 'Monthly Target Member':
                        if y.line_of_business == x.line_of_bussines or not y.line_of_business:
                            if y.insurers == x.company or not y.insurers:
                                self.target += x.t_permimum

    @api.multi
    def get_commission(self):
        opj_s = self.env['installment.staff.commission'].search([('name', '=', self.employee_id.user_id.id)])
        for x in opj_s:

            if x.installment_staff.state == 'delivered':
                if (x.installment_staff.date >= self.date_from) and (x.installment_staff.date <= self.date_to):
                    if x.installment_staff.brokerage_net + x.installment_staff.tax == 0.00:
                        self.commission += x.total

    @api.one
    def net_salary_fun(self):
        self.net = self.line_ids.search([('name', '=', 'Net Salary'), ('slip_id', '=', self.id)]).total

    @api.one
    def basic_salary_fun(self):
        self.basic = self.line_ids.search([('name', '=', 'Basic Salary'), ('slip_id', '=', self.id)]).total

    @api.one
    def social_fun(self):
        self.social = self.line_ids.search([('name', '=', 'Social Insurance'), ('slip_id', '=', self.id)]).total

    @api.one
    def tax_fun(self):
        self.tax = self.line_ids.search([('name', '=', 'Tax'), ('slip_id', '=', self.id)]).total

    @api.multi
    def create_invoices(self):
        if self.state == 'done':
            self.invoice_ids = [(0, 0, {
                'type': 'in_invoice',
                'partner_id': self.employee_id.user_id.partner_id.id,
                'contract': self.contract_id,
                'origin': self.number,
                'user_id': self.env.user.id,
                'name': 'Bill  for ' + str(self.employee_id.id),
                # 'date_due': self.delv_date_ins,
                'invoice_line_ids': [(0, 0, {
                    'name': self.name,
                    'quantity': 1,
                    'price_unit': self.net,
                    'account_id': self.employee_id.user_id.partner_id.property_account_receivable_id.id,
                })],
            })]


class InheritHrRule(models.Model):
    _inherit = 'hr.salary.rule'
    sal = fields.Float("Salary")


class InheritInvoice(models.Model):
    _inherit = 'account.invoice'
    inverse_id = fields.Many2one('hr.payslip')
    contract = fields.Char('Contract')


class InheritInvoiceMove(models.Model):
    _inherit = 'account.move'
    inverse_ids = fields.Many2one('hr.payslip.run')


class CreateBatchBill(models.Model):
    _inherit = 'hr.payslip.run'

    state = fields.Selection([
        ('draft', 'Draft'),
        ('close', 'Close'),
        ('done', 'Done'),
    ], string='Status', index=True, readonly=True, copy=False, default='draft')
    journal_id = fields.Many2one('account.journal', 'Salary Journal', readonly=True, required=True,
                                 states={'draft': [('readonly', False)]},
                                 default=lambda self: self.env['account.journal'].search([('name', '=', 'Payroll')],
                                                                                         limit=1))
    journal_entry_id = fields.One2many('account.move', 'inverse_ids', string='Batch Journal Entries')
    total_net = fields.Float(string='Total Payable', compute='net_salary')
    total_emp_social = fields.Float(string='Social Emp', compute='net_salary')
    total_comp_social = fields.Float(string='Social Comp', compute='compute_comp_social')
    total_tax = fields.Float(string='Tax', compute='net_salary')
    # total = fields.Float(string='Total', compute='net_salary')
    total = fields.Float(string='Net Salary', compute='net_salary')
    posted_journal = fields.Boolean(string="")
    total_social = fields.Float(string='Total Social Insurance', compute='net_salary')

    @api.multi
    def compute_comp_social(self):
        for rec in self.slip_ids:
            for x in rec.contract_id.salaryinfo:
                if (x.fromdate <= rec.date_to) and (x.todate >= rec.date_to):
                    if x.category == 'SOCIAL_COMP_SHARE':
                        self.total_comp_social += x.salary

    @api.multi
    def net_salary(self):
        for rec in self.slip_ids:
            if rec.line_ids.search([('code', '=', 'NET'), ('slip_id', '=', rec.id)]).total:
                self.total_net += rec.line_ids.search([('code', '=', 'NET'), ('slip_id', '=', rec.id)]).total
        for rec in self.slip_ids:
            if rec.line_ids.search([('code', '=', 'SOCIAL'), ('slip_id', '=', rec.id)]).total:
                self.total_emp_social += rec.line_ids.search([('code', '=', 'SOCIAL'), ('slip_id', '=', rec.id)]).total
        for rec in self.slip_ids:
            if rec.line_ids.search([('code', '=', 'TAX'), ('slip_id', '=', rec.id)]).total:
                self.total_tax += rec.line_ids.search([('code', '=', 'TAX'), ('slip_id', '=', rec.id)]).total
        self.total = self.total_net + self.total_emp_social + self.total_tax + self.total_comp_social
        self.total_social = self.total_comp_social + self.total_emp_social

    @api.multi
    def confirm_payslip_batch(self):
        for rec in self.slip_ids:
            rec.action_payslip_done()
        return self.write({'state': 'done'})

    @api.multi
    def create_batch_invoices(self):
        values = []
        # move = self.env["account.account"]
        for record in self.slip_ids:
            if record.state == 'done':
                object = (0, 0, {
                    'account_id': record.journal_id.default_credit_account_id.id,
                    'partner_id': record.employee_id.user_id.partner_id.id, 'name': record.name,
                    'credit': record.line_ids.search([('code', '=', 'NET'), ('slip_id', '=', record.id)]).total})
                values.append(object)
        tax = (0, 0, {'account_id': self.env["account.account"].search([('code', '=', '2207')]).id,
                      'credit': self.total_tax,
                      'name': 'Total Emps Tax'
                      })
        values.append(tax)
        social = (0, 0, {'account_id': self.env["account.account"].search([('code', '=', '2208')]).id,
                         'credit': self.total_social,
                         'name': 'Total  Social Insurance'
                         })
        values.append(social)
        # comp = (0, 0, {'account_id': self.journal_id.default_credit_account_id.id,
        #                'credit': self.total_comp_social,
        #                'name': 'Total Comp Social'
        #                })
        # values.append(comp)
        val = (0, 0, {'debit': self.total,
                      'account_id': self.journal_id.default_debit_account_id.id

                      })
        values.append(val)
        self.journal_entry_id.create({
            'journal_id': self.journal_id.id,
            'ref': self.name,
            'line_ids': values,
        })
        self.posted_journal = True

    @api.multi
    def create_journal_for_salaries(self):
        data = []
        for record in self.slip_ids:
            if record.state == 'done':
                z = (0, 0, {
                    'account_id': record.journal_id.default_credit_account_id.id,
                    'partner_id': record.employee_id.user_id.partner_id.id, 'name': record.name,
                    'debit': record.line_ids.search([('code', '=', 'NET'), ('slip_id', '=', record.id)]).total})
                data.append(z)
        y = (0, 0, {'credit': self.total_net,
                    'account_id': self.env["account.account"].search([('code', '=', '1202005')]).id

                    })
        data.append(y)
        self.journal_entry_id.create({
            'journal_id': self.journal_id.id,
            'ref': self.name,
            'line_ids': data,
        })
        self.posted_journal = True

    @api.multi
    def print_batch(self):
        return self.env.ref('smart_payroll.batch_report').report_action(self)

    # Excel Sheet for Payslip Batch

    is_confirmed = fields.Boolean(string="", compute='is_confirm')
    my_file = fields.Binary(string="Get Your File", )
    excel_sheet_name = fields.Char(string='Name', size=64)

    @api.one
    @api.depends('my_file')
    def is_confirm(self):
        if self.my_file:
            self.is_confirmed = True

    @api.multi
    def generate_batch_excel(self):
        # try:
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        self.excel_sheet_name = 'payroll.xlsx'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)

        worksheet = workbook.add_worksheet()
        worksheet.write('A1', 'Employee')
        worksheet.write('B1', 'Basic Salary')
        worksheet.write('C1', 'Social Emp Share')
        worksheet.write('D1', 'Social Comp Share')
        worksheet.write('E1', 'Taxes')
        worksheet.write('F1', 'Net Salary')

        # file_name = 'temp'
        i = 2
        for record in self.slip_ids:
            print(self.slip_ids)
            worksheet.write('A' + str(i), str(record.employee_id.name))
            worksheet.write('B' + str(i), str(record.basic))
            worksheet.write('C' + str(i), str(record.social))
            worksheet.write('D' + str(i), str(record.comp_share))
            worksheet.write('E' + str(i), str(record.tax))
            worksheet.write('F' + str(i), str(record.net))
            i += 1
        workbook.close()

        output.seek(0)
        self.write({'my_file': base64.encodestring(output.getvalue())})

        return {
            "type": "ir.actions.do_nothing",
        }
