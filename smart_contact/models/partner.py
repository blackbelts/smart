from odoo import models, fields, api
from odoo.exceptions import ValidationError


class inhertResPartner(models.Model):
    _inherit = 'res.partner'

    insurer_type = fields.Boolean('Insurer')
    agent = fields.Boolean('Agent')
    holding_type = fields.Boolean("Holding")
    holding_company = fields.Many2one("res.partner", string="Holding Company")
    numberofchildren = fields.Integer('Number of Children')
    # C_industry = fields.Many2one('insurance.setup.item', string='Industry',
    #                              domain="[('setup_id.setup_key','=','industry')]")
    DOB = fields.Date('Date of Birth')
    martiual_status = fields.Selection([('Single', 'Single'),
                                        ('Married', 'Married'), ],
                                       'Marital Status', track_visibility='onchange')
    last_time_insure = fields.Date('Last Time Insure')
    # property_payable = fields.Many2one('account.account', company_dependent=True,
    #     string="Account Payable", oldname="property_account_payable",
    #     domain="[('internal_type', '=', 'payable'), ('deprecated', '=', False)]",
    #     help="This account will be used instead of the default one as the payable account for the current partner",
    #     required=True, default=lambda self: self.env['account.account'].search([('name', '=', 'Agents Commission')]))
    # property_receivable = fields.Many2one('account.account', company_dependent=True,
    #                                       string="Account Receivable", oldname="property_account_receivable",
    #                                       domain="[('internal_type', '=', 'receivable'), ('deprecated', '=', False)]",
    #                                       help="This account will be used instead of the default one as the receivable account for the current partner",
    #                                       required=True, default=lambda self: self.env['account.account'].search(
    #         [('name', '=', 'Brokerage Commission')]))


    property_payable = fields.Many2one('account.account', 'Account Payable',
                                       domain="[('internal_type', '=', 'payable'), ('deprecated', '=', False)]",
                                       default=lambda self: self.env['account.account'].search(
                                           [('name', '=', 'Account Payable')]))
    property_receivable = fields.Many2one('account.account', 'Account Receivable',
                                          domain="[('internal_type', '=', 'receivable'), ('deprecated', '=', False)]",

                                          default=lambda self: self.env['account.account'].search(
                                              [('name', '=', 'Account Receivable')]))
    property_account_receivable_id = fields.Many2one('account.account',
        string="Account Receivable",
        domain="[('internal_type', '=', 'receivable'), ('deprecated', '=', False)]",
        help="This account will be used instead of the default one as the receivable account for the current partner",
        required=True)
    # @api.model
    # def getids(self):
    #     grs = self.env['res.groups'].search([('category_id.name','=','Sales'),('name', '=', 'Manager')]).users.ids
    #     return grs


    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        # self.getids()
        # grs = self.env['res.groups'].search([('category_id.name','=','Sales'),('name', '=', 'Manager')]).users.ids

        # ids = self.env['res.users'].search([('groups_id.category_id.name','=','Sales'),('groups_id.name', '=', 'Manager')])
        print(555555555)
        # print(ids)
        # for rec in grs:
        #        print(rec.name)
        if self._context.get('own_customer_only'):
            if self.env.user.id != 1 :
                args += [('user_id', '=', self.env.user.id)]
        print(args)
        return super(inhertResPartner, self).search(args, offset=offset, limit=limit, order=order, count=count)

class NewModule(models.Model):
    _name = 'data.clean'

    clean_option_insurer=fields.Boolean('Clean Insurer')
    clean_option_customer=fields.Boolean('Clean Discount Party')

    insurer = fields.Many2one("res.partner", string="correct Insurer", required=False, domain="[('insurer_type','=',1)]" )
    discount_part=fields.Many2one("res.partner", string="correct discount Party", required=False,  domain="[('insurer_type','=', False),('agent','=', False),('customer','=', True)]")

    @api.multi
    def replace(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        if self.clean_option_insurer:
            for rec in active_ids:
                for record in self.env['policy.broker'].search([('company', '=', rec), ('active', '=', False)]):
                    print(record.std_id)
                    if self.insurer:
                        record.write({'company': self.insurer.id})
                for record in self.env['policy.broker'].search(
                        [('company', '=', rec), ('active', '=', True)]):
                    print(record.std_id)
                    if self.insurer:
                        record.write({'company': self.insurer.id})

        if self.clean_option_customer:
            for rec in active_ids:
                for record in self.env['policy.broker'].search(
                        ['|', ('discount_party', '=', rec), ('source_of_business', '=', rec), ('active', '=', True)]):
                    if self.discount_part:
                        record.write({'discount_party': self.discount_part.id})
                for record in self.env['policy.broker'].search(
                        ['|', ('discount_party', '=', rec), ('source_of_business', '=', rec), ('active', '=', False)]):
                    if self.discount_part:
                        record.write({'discount_party': self.discount_part.id})


        return {'type': 'ir.actions.act_window_close'}