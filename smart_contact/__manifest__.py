# -*- coding: utf-8 -*-
{
    'name': "Smart Contact",
    'summary': """Claim Management & Operations""",
    'description': """Insurance Broker System """,
    'author': "Black Belts Egypt",
    'website': "www.blackbelts-egypt.com",
    'category': 'Claim',
    'version': '0.1',
    'license': 'AGPL-3',
    # any module necessary for this one to work correctly

    'depends': ['base','contacts','account'],


    # always loaded
    'data': [
        'views/partner.xml',
        'views/clean.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
