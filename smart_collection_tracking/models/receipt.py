from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ReceiptNumber(models.TransientModel):
    _name = "installments.receipt"

    @api.multi
    def generate_receipt(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            record.write({'receipt_No': self.env['ir.sequence'].next_by_code('receipt') or 'New'})
        return {'type': 'ir.actions.act_window_close'}
