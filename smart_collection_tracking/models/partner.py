from odoo import models, fields, api
from odoo.exceptions import ValidationError


class inhertResPartner(models.Model):
    _inherit = 'res.partner'

    collection_count = fields.Integer(compute='_compute_collection_count')

    @api.one
    def _compute_collection_count(self):
        if self.customer == 1:
            for partner in self:
                operator = 'child_of' if partner.is_company else '='
                partner.collection_count = self.env['installment.installment'].search_count(
                    [('customer', operator, partner.id)])
        elif self.insurer_type == 1:
            for partner in self:
                operator = 'child_of' if partner.is_company else '='
                partner.collection_count = self.env['installment.installment'].search_count(
                    [('insurer', operator, partner.id)])
        elif self.key_account == 1:
            for partner in self:
                operator = 'child_of' if partner.is_company else '='
                partner.collection_count = self.env['installment.installment'].search_count(
                    [('discount_party', operator, partner.id)])

    @api.multi
    def partner_report_collection(self):
        if self.insurer_type:
            claim = self.env['insurance.claim'].search([('insurer', '=', self.id)])
            return claim
        elif self.customer:
            claim = self.env['insurance.claim'].search([('customer_policy', '=', self.id)])
            return claim

    @api.multi
    def show_partner_collection(self):
        if self.customer == 1:
            return {
                'name': ('Collection'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'installment.installment',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_customer': self.id},
                'domain': [('customer', '=', self.id)]
            }
        elif self.insurer_type == 1:
            return {
                'name': ('Collection'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'installment.installment',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_insurer': self.id},
                'domain': [('insurer', '=', self.id)]
            }
        elif self.key_account == 1:
            return {
                'name': ('Collection'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'installment.installment',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_insurer': self.id},
                'domain': [('discount_party', '=', self.id)]
            }
