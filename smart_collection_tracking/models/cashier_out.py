from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class CollectedBy(models.TransientModel):
    _name = "installments.cashier.out.confirm.assign"
    _description = "Cashier out Confirm"
    cashier_out_emp = fields.Many2one('hr.employee', string='Collected By')

    @api.multi
    def assign_cashier(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            if record.state != 'collected':
                raise UserError(_(
                    "Selected installment(s) cannot be confirmed as they are not in 'cashier in' state.ins number %s" % record.name))
            record.write({'cashier_out_to': self.cashier_out_emp.id, 'delivered_by': self.cashier_out_emp.id,
                          'cashier_out_date': fields.Date.today()})
        return self.env.ref('smart_collection_tracking.cashier_out_report').report_action(active_ids)


class issent(models.Model):
    _name = 'installments.sent'

    @api.multi
    def sent(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        x = len(active_ids)
        for record in self.env['installment.installment'].browse(active_ids):
            record.write({'is_sent': True,
                          'sent_description': (str(record.discount_party.name)) + "  -  " + (
                              str(x)) + "  -  " + (str(datetime.today().strftime('%Y-%m-%d')))})
        return {'type': 'ir.actions.act_window_close'}


class isnotsent(models.Model):
    _name = 'installments.not.sent'

    @api.multi
    def not_sent(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            record.write({'is_sent': False})
        return {'type': 'ir.actions.act_window_close'}


class cashier_outConfirm(models.TransientModel):
    _name = "installments.cashier.out.confirm"

    @api.multi
    def confirm_cachier(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            if record.state != 'collected' or not record.payment_details_ids:
                raise UserError(_(
                    "Selected installment(s) cannot be confirmed as they are not in 'cashier in'  state And you must insert payment details before cashier Out.ins number %s" % record.name))
            record.convert_cashier_out()
        return {'type': 'ir.actions.act_window_close'}
