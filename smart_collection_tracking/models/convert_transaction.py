from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class CollectionConfirm(models.TransientModel):
    _name = "convert.transaction"
    # payment_date = fields.Date(string="Payment Date", required=True, )

    @api.multi
    def convert(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        form_view = self.env.ref('smart_collection_tracking.form_cashier_transaction')
        policy_ids = []
        insurer=[]
        curr=[]
        for record in self.env['installment.installment'].browse(active_ids):
            insurer.append(record.insurer.id)
            curr.append(record.currency_id.id)

        x=set(insurer)
        y=set(curr)
        z=[]
        n=[]
        f=0
        l=0
        for rec in x:
            z.append(rec)
            f+=1
        for rec in y:
            n.append(rec)
            l+=1
        print(z,n)
        if f > 1 or l > 1 :
            raise ValidationError('Selected Installment Must have the same Insurer and Currency')
        # print (self.env['installment.installment'].browse(active_ids[0]).insurer)
        for record in self.env['installment.installment'].browse(active_ids):
            if  record.state != 'outstanding' :
                raise UserError(_("Selected installment(s) cannot convert as they are not in  Outstanding State ins number %s" % record.name))
            else:
                object = (
                    0, 0, {'installment_transaction': record.id,'customer': record.customer.id,'discount_party': record.discount_party.id,
                     'insurer': record.insurer.id,'gross_premium': record.gross_premium,'payable_gross_premium': record.new_gross_premium,'broker_comm': record.total_broker_commissions,
                           'staff_commissions': record.staff_commissions,'currency_id': record.currency_id.id,'state': record.state,'due_comm': record.due_comm,
                           'net': record.brokerage_net,'tax': record.tax,
                           })
                policy_ids.append(object)
        return {
            'name': ('Transaction'),
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(form_view.id, 'form')],
            'res_model': 'cashier.transaction',  # model name ?yes true ok
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {'default_policy_ids': policy_ids,
                        'default_insurer': z[0],
                        'default_currency_id': n[0]},
        }