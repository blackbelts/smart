
from datetime import datetime, timedelta
from odoo import models, tools, fields, api
from openerp.exceptions import ValidationError



class kay_account_month_wizard(models.TransientModel):
    _name = 'installment.pivot'
    date_from = fields.Date(string="From Date", required=True, default=datetime.today())
    to_date = fields.Date(string="To Date", required=True, default=datetime.today())
    insurer = fields.Many2one('res.partner', string='Insurer',)

    @api.multi
    def create_pivot(self):
        if self.insurer:
            domain = [  ('date', '>=', self.date_from),
                        ('date', '<=', self.to_date),
                        ('insurer', '<=', self.insurer.id),
                       ]
        else:
            domain = [('date', '>=', self.date_from),
                      ('date', '<=', self.to_date), ]

        tree_id=self.env.ref('smart_collection_tracking.tree_collections').id
        return {
            'name': ('Report'),
            'view_type': 'pivot',
            'view_mode': ['pivot','tree'],
            'res_model': 'installment.installment',
            'views': [((self.env.ref('smart_collection_tracking.installment_pivot').id), 'pivot'),(tree_id,'tree')],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': domain,

        }


class PivotReportMonthKAccount(models.Model):
    _name = "installment.pivot.report"
    _table = 'installment_pivot_report'
    _auto = False
    net_premium = fields.Float(string="Net Premium", copy=True)
    gross_premium = fields.Float(string="Gross Premium", copy=True)
    state = fields.Selection([('outstanding', 'Outstanding'),
                              ('collected', 'Cashier In'), ('cashier', 'Cashier Out'), ('delivered', 'Paid'),
                              ('canceled', 'Canceled')],string='State' , readonly=True)
    insurer = fields.Many2one('res.partner', string='Insurer',)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        query = """
                CREATE VIEW installment_pivot_report AS (
                SELECT id,state,net_premium,gross_premium,insurer from installment_installment GROUP BY id)
             """
        # query = """
        #                 CREATE VIEW installment_pivot_report AS (
        #                 SELECT id from insurance_line_business group by id)
        #              """
        self.env.cr.execute(query)