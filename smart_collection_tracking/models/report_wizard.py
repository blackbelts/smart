from odoo import models, fields, api
from openerp.exceptions import ValidationError


class je_wizard(models.TransientModel):
    _name = 'je.item.report'
    # customerW = fields.Many2one(comodel_name="customer.customer", string="Customer", required=False, )
    account = fields.Many2one(comodel_name="account.account", string="Account", required=True, )
    date_from = fields.Date(string="From Date", required=True, )
    to_date = fields.Date(string="To Date", required=True, )

    # room = fields.Many2one(comodel_name="working.room", string="Room", required=False, )
    # state = fields.Selection(string="", selection=[('Customer', 'Customer'), ('Date', 'Date'),('Room','Room') ], required=True, )

    @api.multi
    def prints(self, data):
        data['form'] = self.read()
        return self.env.ref('smart_collection_tracking.je_report').report_action(self, data=data)


class reportje(models.AbstractModel):
    _name = 'report.smart_collection_tracking.je_report1'

    @api.multi
    def get_report_values(self, docids, data=None):
        print("AAA")
        all = []
        s1 = [('transaction_date', '>=', data['form'][0]['date_from']),
              ('transaction_date', '<=', data['form'][0]['to_date']), ('account', '=', data['form'][0]['account'][0])]
        res = self.env['je.details'].search(s1)
        all.append(res)
        print(all)

        if not data.get('form') or not self.env.context.get('active_model'):
            raise ValidationError("Form content is missing, this report cannot be printed.")

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
        return {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': all,
            'docs': docs,
        }
