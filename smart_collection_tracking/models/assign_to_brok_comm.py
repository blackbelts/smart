from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class BrokerageConfirm(models.TransientModel):
    _name = "installments.brokerage.confirm"

    @api.multi
    def confirm_brokerage(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            if record.state != 'delivered':
                raise UserError(_("Selected installment(s) cannot be confirmed as they are not in 'Delivered' state."))
            record.convert_received()
            record.commission_date = fields.Date.today()
        return {'type': 'ir.actions.act_window_close'}

