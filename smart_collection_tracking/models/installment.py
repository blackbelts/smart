from datetime import timedelta, datetime
import base64

import xlsxwriter
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class installmentCollection(models.Model):
    _name = "installment.installment"
    _rec_name = 'inst_description'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    inst_description = fields.Char("Description", compute="_compute_description", store=True)
    sent_description = fields.Char()
    @api.one
    @api.depends('policy_number','endorsement_no','date')
    def _compute_description(self):
            self.inst_description = (str(self.policy_number)) + "  -  " + (
                    str(self.endorsement_no) if self.endorsement_no else " " + "_") + "  -  " + (
                                            str(self.date)  if self.date else " " + "_")

    @api.one
    def unlink(self):

        if self.state != ('outstanding') or self.is_sent == True:
            raise UserError(_(
                'You cannot delete this installment which is not out standing or mark as sent'))
        return super(installmentCollection, self).unlink()
    # policy fields
    policy_number = fields.Char(string='Policy No.')
    endorsement_no = fields.Char(string='Endorsement No.')
    is_renewal = fields.Boolean(string="R", )
    customer = fields.Many2one('res.partner', string='Customer',
                               domain="[('insurer_type','=', False),('agent','=', False)]")
    discount_party = fields.Many2one('res.partner', 'Discount Party', copy=True)
    insurer = fields.Many2one('res.partner', string='Insurer')
    insurance_type = fields.Selection([('Life', 'Life'),
                                       ('General', 'General'),
                                       ('Health', 'Health'), ],
                                      'Insurance Type', track_visibility='onchange', copy=True)
    lob = fields.Many2one('insurance.line.business', string='Line of Business',
                          domain="[('insurance_type','=',insurance_type)]")
    # line_ob = fields.Char(compute ='get_lob_pivot',store=True)
    object_type = fields.Selection([('person', 'Person'),
                                    ('vehicle', 'Vehicle'),
                                    ('cargo', 'Cargo'),
                                    ('location', 'Location'), ],
                                   'Insured Object', track_visibility='onchange')
    ins_product = fields.Many2one('insurance.product', string='Product',
                                  domain="[('insurer','=',insurer),('line_of_bus','=',lob)]")
    staff_ids = fields.One2many(comodel_name="installment.staff.commission", inverse_name="installment_staff",
                                string="Staff Commission",
                                required=False, )
    start_date = fields.Date(string="Effective From", default=datetime.today())
    end_date = fields.Date(string="Effective To", default=datetime.today())
    policy_desc = fields.Char(string='Description')
    date = fields.Date(string="Date", default=datetime.today())
    write_date=fields.Date("Updated On")
    name = fields.Char('Number', required=True, copy=False, readonly=True, index=True,
                       default=lambda self: _('New'))
    receipt_No = fields.Char('Receipt Number', readonly=True)
    state = fields.Selection([('outstanding', 'Outstanding'),
                              ('collected', 'Cashier In'), ('cashier', 'Cashier Out'), ('delivered', 'Paid'),

                              ('canceled', 'Canceled')],
                             'State', required=True, default='outstanding')
    source_of_business = fields.Many2one('res.partner', string=' Source Of Business')
    collected_by = fields.Many2one('hr.employee', string='Collected By')
    brok_collected_by = fields.Many2one('hr.employee', string='Brok Collected By')
    cashier_out_to = fields.Many2one('hr.employee', string='Cashier Out To')

    pay_date = fields.Date(string='Collection Date', default=datetime.today())
    pay_method = fields.Selection([('cash', 'Cash'), ('check', 'Check'), ], string='Collection Method')

    delivered_by = fields.Many2one('hr.employee', string='Delivered By', )
    delv_date_ins = fields.Date(string='Paid On', )
    cashier_out_date = fields.Date(string='Cashier out Date', default=datetime.today())
    currency_id = fields.Many2one("res.currency", "Currency", copy=True)
    gross_premium = fields.Float(string="Gross Premium", copy=True)
    net_premium = fields.Float(string="Net Premium", copy=True)
    rate = fields.Float(string="Rate", readonly=True)
    discount = fields.Float(string="Discount", compute='_compute_discount')
    new_net_premium = fields.Float(string="New Net Premium", copy=True, compute='compute_new_net')
    new_gross_premium = fields.Float(string="Payable Gross Premium", copy=True, compute='compute_gross_net')
    due_comm = fields.Float(string="Paid Brokerage",  required=False, )
    brokerage_net = fields.Float(string="O/S Brokerage",  required=False,compute='get_brokerage_net' )

    # created_on = fields.Datetime(string="", required=False,default=lambda self: fields.datetime.now() )
    basic_brokerage = fields.Float(string="Basic Commission")
    complementary_comm = fields.Float(string="Complementary Commission")
    transportation_comm = fields.Float(string="Transportation Commission")
    total_broker_commissions = fields.Float(string='Total Brokerage', compute='_compute_total_broker_commission',store=True)
    tax = fields.Float(string="Tax",  required=False,readonly=True )

    staff_commissions = fields.Float(string='Staff Commission')
    invoice_button = fields.Boolean()
    dangers = fields.Boolean(compute='_compute_dangers', defualt=False,store=True)
    move_id = fields.One2many('account.move', 'installment_id', string='Moves', readonly=True)

    line_ids_copy = fields.One2many('account.move.line', 'move_id', string='Journal Items',)
    # payment_details_ids = fields.One2many(comodel_name="payment.details", inverse_name="installment_id", string="", required=False, compute='get_disc_party',inverse='set_disc_party',store=True)
    is_sent = fields.Boolean(string="Sent",  )
    deduction_comm = fields.Float(string="Deduction Commission",  required=False, )
    total_due = fields.Float(string="Total",  required=False,compute='get_total_due' )
    is_have_invoice = fields.Boolean(string="",  )
    is_have_bill = fields.Boolean(string="",  )
    is_have_JE = fields.Boolean(string="",  )

    sales_person = fields.Many2one('res.users', string='Salesperson',  copy=True)
    team_id = fields.Many2one('crm.team', string='Sales Channel', oldname='section_id',
                              default=lambda self: self.env['crm.team'].sudo()._get_default_team_id(
                                  user_id=self.env.uid),
                              index=True, track_visibility='onchange',
                              help='When sending mails, the default email address is taken from the sales channel.')
    commission_paid_date = fields.Date(string="Commission Receive Date", required=False, )
    def get_parent_channel(self, channel):
        parent = channel.parent_channel
        return parent

    # @api.depends('staff_ids')
    # def get_staff_comm(self):
    #     x = 0
    #     for rec in snew_gross_premiumelf.staff_ids:
    #         x += rec.total
    #     self.staff_commissions = x
    # @api.one
    # @api.depends('lob')
    # def get_lob_pivot(self):
    #     lob = self.env['insurance.line.business'].search([('id', '=', self.lob.id)]).line_of_business
    #     self.line_ob = lob.id
    #     print(lob)


    @api.onchange('total_premium_due')
    def get_type(self):
        if self.total_premium_due < 0:
            self.issue_type = 'Refund'
        else:
            self.issue_type = 'Issue'
    @api.multi
    def action_invoice_tree_view(self):
        tree = self.env.ref('account.invoice_tree')
        form = self.env.ref('account.invoice_form')
        return {
            'name': ('Invoices'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',  # model name ?yes true ok
            'target': 'current',
            'views': [(tree.id, 'tree'),
                      (form.id, 'form')],
            'type': 'ir.actions.act_window',
            # 'context': {'default_customer': self.id},
            'domain': ['&',('installment_id', '=', self.id),('type','=','out_invoice')]
        }

    @api.multi
    def action_JE_tree_view(self):
        tree = self.env.ref('account.view_move_tree')
        form = self.env.ref('account.view_move_form')
        return {
            'name': ('Journal Entries'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',  # model name ?yes true ok
            'target': 'current',
            'views': [(tree.id, 'tree'),
                      (form.id, 'form')],
            'type': 'ir.actions.act_window',
            # 'context': {'default_customer': self.id},
            'domain': [('installment_id', '=', self.id)]
        }

    @api.multi
    def action_bill_tree_view(self):
        tree = self.env.ref('account.invoice_tree')
        form = self.env.ref('account.invoice_form')
        return {
            'name': ('Invoices'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',  # model name ?yes true ok
            'target': 'current',
            'views': [(tree.id, 'tree'),
                      (form.id, 'form')],
            'type': 'ir.actions.act_window',
            # 'context': {'default_customer': self.id},
            'domain': ['&', ('installment_id', '=', self.id), ('type', '=', 'in_invoice')]
        }
    @api.one
    @api.depends('deduction_comm', 'total_broker_commissions')
    def get_total_due(self):
        self.total_due = self.deduction_comm + self.total_broker_commissions + self.bonus

    # @api.one
    # @api.depends('payment_details_ids')
    # def get_deduct(self):
    #     x = 0
    #     for rec in self.payment_details_ids:
    #         if rec.accname1 == 'Broker Commission':
    #             x += rec.cashier_Out
    #     self.deduction_comm = x * -1

    # @api.one
    # @api.depends('discount')
    # def get_disc_party(self):
    #     print("wla wla wla ")
    #     if self.discount != 0.00:
    #         self.payment_details_ids = [(0, 0, {
    #             "accname1": 'Discounts Expenses',
    #             "DR": "DR",
    #             "CR": "CR",
    #             # "partner1": self.discount_party,
    #             # "partner2": self.discount_party,
    #             "cashier_in": self.discount,
    #             "cashier_Out": self.discount,
    #             "accname2": 'Discounts Expenses',
    #         })]

    # @api.one
    # @api.depends('discount')
    # def get_disc_party(self):
    #     print("wla wla wla ")
    #     if self.discount != 0.00:
    #         self.payment_details_ids=[(0, 0, {
    #             "accname1": 'Discounts Expenses',
    #             "DR":"DR",
    #             "CR":"CR",
    #             # "partner1": self.discount_party,
    #             # "partner2": self.discount_party,
    #             "cashier_in": self.discount,
    #             # "cashier_Out": self.discount,
    #             "accname2": 'Discounts Expenses',
    #         })]
    @api.depends('discount')
    def set_disc_party(self):
        return True

    @api.one
    @api.depends('date', 'state')
    def _compute_dangers(self):
        if self.date:
            date = fields.Datetime.from_string(self.date)
            duration = date + timedelta(days=15)
            if duration <= datetime.today() and self.state == 'outstanding':
                self.dangers = True

    @api.one
    @api.depends('discount', 'gross_premium')
    def compute_gross_net(self):
        # discount = (self.discount / 100) * self.gross_premium
        self.new_gross_premium = self.gross_premium - self.discount
    @api.one
    @api.depends('total_broker_commissions','due_comm','tax')
    def get_brokerage_net(self):
        self.brokerage_net= self.total_broker_commissions - self.due_comm - self.tax
    @api.one
    @api.depends('basic_brokerage', 'complementary_comm', 'transportation_comm')
    def _compute_total_broker_commission(self):
        self.total_broker_commissions = self.basic_brokerage + self.complementary_comm + self.transportation_comm

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('Ins.Seq') or 'New'
        return super(installmentCollection, self).create(vals)

    @api.one
    @api.depends('discount')
    def compute_new_net(self):
        # discount = (self.discount / 100) * self.net_premium
        self.new_net_premium = self.net_premium - self.discount

    @api.one
    @api.depends('discount_party', 'gross_premium')
    def _compute_discount(self):

        today = datetime.today().strftime('%Y-%m-%d')
        discount = self.env['installment.discount.rate'].search(['&','&',('name', '=', self.discount_party.id),('date_from','<=',today),('date_to','>=',today)],
                                                                limit=1).rate
        self.discount = (discount / 100) * self.gross_premium

    # @api.multi
    # def convert_collected(self):
    #     count = 0
    #     for record in self.payment_details_ids:
    #         count += record.cashier_in
    #     print(count)
    #     if self.payment_details_ids and count == self.gross_premium:
    #         self.state = 'collected'
    #         self.pay_date = fields.Date.today()
    #     else:
    #         raise ValidationError(
    #             "You must insert Payment Details Correctly Before Total cashier in  must be  %s !" % self.gross_premium)

    # @api.multi
    # def convert_cashier_out(self):
    #     count = 0
    #     for record in self.payment_details_ids:
    #         count += record.cashier_Out
    #     print(count)
    #     if count == self.gross_premium:
    #         self.state = 'cashier'
    #         self.cashier_out_date = fields.Date.today()
    #     else:
    #         raise ValidationError(
    #             "You must insert Payment Details Correctly Before Total cashier out  must be %s !" % self.gross_premium)

    @api.multi
    def open_rec(self):
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'installment.installment',
            'res_id': self.id,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'flags': {'form': {'action_buttons': True}}

        }

    @api.multi
    def convert_delivered(self):

        if self.delv_date_ins:
            # if self.sales_person:
                # print('i want something just like this !')
                # list = []
                # basic_commission = 0
                # override_commission = 0
                # opj_s = self.env['crm.team'].search([('member_ids', '=', self.sales_person.id)])
                # print(opj_s)
                # for x in opj_s.rules_ids:
                #     if x.line_of_business == self.lob or not x.line_of_business:
                #         if x.insurers == self.insurer or not x.insurers:
                #             if x.date_from < self.delv_date_ins and x.date_to > self.delv_date_ins and x.rule == 'Direct Commission':
                #                 basic_commission = x.value
                #             elif x.date_from < self.delv_date_ins and x.date_to > self.delv_date_ins and x.rule == 'Override Commission':
                #                 override_commission = x.value
                # print(basic_commission)
                # print(override_commission)
                # if basic_commission:
                #     self.update({
                #         'staff_ids': [(0, 0, {'name': self.sales_person.id,
                #                               'total': self.total_broker_commissions * (basic_commission)
                #                               })], })
                #     # print('sssssddddffffjjj')
                # if opj_s.user_id and override_commission:
                #     self.update({
                #         'staff_ids': [(0, 0, {'name': opj_s.user_id.id,
                #                               'total': self.total_broker_commissions * (override_commission)
                #                               })], })
                #
                # object = opj_s.parent_channel
                # while object:
                #     list.append(object)
                #     object = self.get_parent_channel(object)
                # print(list)
                #
                # for rec in list:
                #     override_commission1 = 0
                #     for x in rec.rules_ids:
                #         if x.line_of_business == self.lob or not x.line_of_business:
                #             if x.insurers == self.insurer or not x.insurers:
                #                 if x.date_from < self.delv_date_ins and x.date_to > self.delv_date_ins and x.rule == 'Override Commission':
                #                     override_commission1 = x.value
                #     if rec.user_id:
                #         self.update({
                #             'staff_ids': [(0, 0, {'name': rec.user_id.id,
                #                                   'total': self.total_broker_commissions * (override_commission1)
                #                                   })], })
                # for x in self.staff_ids:
                #      print(x.name)
                #      print(x.total)
            self.state = 'delivered'
        else:
            raise ValidationError('You must insert Paid On date !')
    # @api.multi
    # def convert_received(self):
    #     self.state = 'brokerage_rec'
    #     self.broker_comm_date = fields.Date.today()
    #
    # @api.multi
    # def convert_staff_comm(self):
    #     self.state = 'commission_rec'
    #     self.staff_comm_date = fields.Date.today()

    @api.multi
    def convert_canceled(self):
        self.state = 'canceled'

    @api.multi
    def send_mail_template_collection_tracking(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('smart_collection_tracking.collection_tracking_email_template')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'installment.installment',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }
    #
    # @api.constrains('payment_details_ids')
    # def _constrain_collection_cashier_in(self):
    #     count = 0
    #     for record in self.payment_details_ids:
    #         count += record.cashier_in
    #     print(count)
    #     if self.payment_details_ids and count!= self.gross_premium:
    #         raise ValidationError('Total Check In Amount in Payment Details Must Equal to Gross Premium (%s) !'% self.gross_premium)

    # @api.constrains('payment_details_ids')
    # def _constrain_collection_cashier_out(self):
    #     count = 0
    #     if self.state == 'collected':
    #         for record in self.payment_details_ids:
    #             count += record.cashier_Out
    #         print(count)
    #         if self.payment_details_ids and count != self.gross_premium:
    #             raise ValidationError('Total Check Out Amount in Payment Details Must Equal to Gross Premium (%s) !'% self.gross_premium)


class installmentDisc(models.Model):
    _name = 'installment.discount.rate'
    _rec_name = 'name'

    name = fields.Many2one(comodel_name="res.partner", string="Partner",
                domain="[('insurer_type','=', False),('agent','=', False)]")
    rate = fields.Float(string="Rate", required=False, )
    date_from = fields.Date(string="Date From", required=True, )
    date_to = fields.Date(string="Date To", required=True, )
    @api.constrains('date_to','date_from')
    def validate_date(self):
        if self.date_to < self.date_from:
            raise ValidationError('Error! Date to Should be After Date from')



class AccountInvoiceRelate(models.Model):
    _inherit = 'account.move'

    installment_id = fields.Many2one('installment.installment', string='Installment')

class staff_commission_inst(models.Model):
    _name = 'installment.staff.commission'

    total = fields.Float(string="Total", required=False, )
    name = fields.Many2one('res.partner', string='Name', domain="[('agent','=',1)]", copy=True)
    installment_staff = fields.Many2one(comodel_name="installment.installment", required=False, )


class paymentDetails(models.Model):
    _name = 'payment.details'
    date = fields.Date(string="Date", required=False, default=datetime.today())

    accname1 = fields.Many2one(comodel_name="account.account", string="Account Name", required=False, )
    accname2 = fields.Many2one(comodel_name="account.account", string="Account Name", required=False,compute="get_accname",inverse='set_accname' )

    # accname1 = fields.Selection(string="Account Name", selection=[('Premium Cash', 'Premium Cash'),
    #                                                   ('Premium Cheque', 'Premium Cheque'),
    #                                                               ('Cash','Cash'),
    #                                                               ('Bank','Bank'),
    #                                                   ('Discounts Expenses', 'Discounts Expenses'),
    #                                                   ('Accounts Payables', 'Accounts Payables'),
    #                                                   ('Broker Commission', 'Broker Commission'),
    #                                                   ('Credit to customer', 'Credit to customer'),
    #                                                   ('Credit Payment - Cash', 'Credit Payment - Cash'),
    #                                                   ('Credit Payment - Cheque', 'Credit Payment - Cheque') ], required=False, )
    # accname2 = fields.Selection(string="Account Name", selection=[('Premium Cash', 'Premium Cash'),
    #                                                   ('Premium Cheque', 'Premium Cheque'),
    #                                                               ('Cash', 'Cash'),
    #                                                               ('Bank', 'Bank'),
    #                                                   ('Discounts Expenses', 'Discounts Expenses'),
    #                                                   ('Accounts Payables', 'Accounts Payables'),
    #                                                   ('Broker Commission', 'Broker Commission'),
    #                                                   ('Credit to customer', 'Credit to customer'),
    #                                                   ('Credit Payment - Cash', 'Credit Payment - Cash'),
    #                                                   ('Credit Payment - Cheque', 'Credit Payment - Cheque')],
    #                             required=False,compute="get_accname",inverse='set_accname',store=True )

    # partner1 = fields.Many2one(comodel_name="res.partner", string="Partner", required=False, )
    # partner2 = fields.Many2one(comodel_name="res.partner", string="Partner", required=False, )
    DR = fields.Selection(string="DR/CR", selection=[('DR', 'DR'), ('CR', 'CR'), ], required=False, default='DR')
    CR = fields.Selection(string="DR/CR", selection=[('DR', 'DR'), ('CR', 'CR'), ], required=False, default='CR')
    cashier_in = fields.Float(string="Cashier-In", required=False, )
    cashier_Out = fields.Float(string="Cashier-Out", required=False, )
    check_date = fields.Date(string="Check Date", required=False, )
    check_bank = fields.Text(string="Bank Information", required=False, )
    info = fields.Text(string="Information", required=False, )
    installment_id = fields.Many2one(comodel_name="installment.installment", string="", required=False, )
    net = fields.Float(string="Net", required=False, compute='calculate_net')
    policy_no1 = fields.Char(string="Policy No", required=False, compute='get_policy_no', store=True)
    real_cashier_in = fields.Float(string="Total", required=False, compute='claculate_total')
    real_cashier_out = fields.Float(string="Total", required=False, compute='claculate_total2')
    is_visible = fields.Boolean(string="", )

    @api.one
    @api.depends('installment_id')
    def get_policy_no(self):
        self.policy_no1 = self.installment_id.policy_number

    @api.one
    @api.depends('accname1')
    def get_accname(self):

        if self.accname1:
            print("hisham   djf")
            self.accname2 = self.accname1

    def set_accname(self):
        return True

    @api.one
    @api.depends('cashier_in', 'cashier_Out')
    def calculate_net(self):
        if self.DR == 'DR' and self.CR == 'CR':
            self.net = self.cashier_in + (-1 * self.cashier_Out)
        elif self.DR == 'CR' and self.CR == 'DR':
            self.net = self.cashier_Out + (-1 * self.cashier_in)
        elif self.DR == 'DR' and self.CR == 'DR':
            self.net = self.cashier_Out + self.cashier_in
        elif self.DR == 'CR' and self.CR == 'CR':
            self.net = (-1 * self.cashier_Out) + (-1 * self.cashier_in)

    @api.one
    @api.depends('cashier_in', 'DR')
    def claculate_total(self):
        # opjects=self.env['payment.details'].search([('', '=', self.discount_party.id)])
        if self.DR == 'CR':
            self.real_cashier_in = self.cashier_in * -1
        else:
            self.real_cashier_in = self.cashier_in

    @api.one
    @api.depends('cashier_Out', 'CR')
    def claculate_total2(self):
        # opjects=self.env['payment.details'].search([('', '=', self.discount_party.id)])
        if self.CR == 'CR':
            self.real_cashier_out = self.cashier_Out * -1
        else:
            self.real_cashier_out = self.cashier_Out

class class_name(models.TransientModel):
    _name = 'model.name'

    date_from = fields.Datetime('From')
    date_to = fields.Datetime('To')

    @api.one
    def filter_data(self):
        ### you will get all the defined fields of wizard here.
        from_date = self.date_from
        to_date = self.date_to


        ### add your own logic to pass search criteria to the source model
    ## Define your fields here and add them in xml file and define menu with appropreate action to open that wizard.
