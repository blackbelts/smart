from datetime import timedelta, datetime
import base64

import xlsxwriter
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError
class cashierTransactions(models.Model):
    _name = 'cashier.transaction'
    _rec_name = 'name'
    customer = fields.Many2one('res.partner', string='Customer',)
    name = fields.Char(string='Transaction No', required=True, copy=False, index=True ,default=lambda self: self.env['ir.sequence'].next_by_code('transaction'),)
    cashier_in_no = fields.Char(string='Cashier In No', required=False, copy=False, index=True ,compute='identify_type',store=True)
    cashier_out_no = fields.Char(string='Cashier Out No', required=False, copy=False, index=True,compute='identify_type',store=True )
    cashier_in_from = fields.Selection(string="From", selection=[('Insurer', 'Insurer'), ('Customer', 'Customer'), ], required=False, )
    cashier_out_to = fields.Selection(string="To", selection=[('Insurer', 'Insurer'), ('Customer', 'Customer'), ], required=False, )
    insurer_cashier_in = fields.Many2one('res.partner', string='From Insurer', domain="[('insurer_type','=', True)]")
    customer_cashier_in = fields.Many2one('res.partner', string='From Customer',domain="[('insurer_type','=', False),('agent','=', False)]")
    insurer_cashier_out = fields.Many2one('res.partner', string='To Insurer', domain="[('insurer_type','=', True)]")
    customer_cashier_out = fields.Many2one('res.partner', string='To Customer',domain="[('insurer_type','=', False),('agent','=', False)]")
    transaction_type = fields.Selection(string="Transaction Type", selection=[('Cashier In', 'Cashier In'),('Cashier Out', 'Cashier Out'),('Brokerage Cashier In', 'Brokerage Cashier In'),('Brokerage Cashier Out', 'Brokerage Cashier Out'), ], required=True,readonly=True,default='Cashier In' )
    state = fields.Selection(string="", selection=[('Draft', 'Draft'), ('Paid', 'Approved'),('POST', 'POST'),('canceled', 'Canceled') ], required=False,default='Draft' )
    currency_id = fields.Many2one("res.currency", "Currency", copy=True,required=True,default=lambda self: self.env.user.company_id.currency_id)
    amount = fields.Float(string="Amount",  required=False, )
    transaction_date = fields.Date(string="Transaction Date", required=True, default=datetime.today() )
    cash_chq = fields.Selection(string="Cash/Cheque", selection=[('Cash', 'Cash'), ('Cheque', 'Cheque'), ], required=False,default='Cheque' )
    policy_ids = fields.One2many(comodel_name="policy.details", inverse_name="cashier_transaction_id", string="", required=False,ondelete='cascade' )
    # policy_ids1 = fields.One2many(comodel_name="policy.details", inverse_name="cashier_transaction_id", string="", required=False, )

    # je_details_ids = fields.One2many(comodel_name="je.details", inverse_name="transaction_id",compute='get_records', string="", required=False,store=True )
    je_ids = fields.One2many(comodel_name="je.details", inverse_name="transaction_id", string="", required=False,  )
    summary_ids = fields.One2many(comodel_name="summary.track", inverse_name="transaction_id", string="", required=False,  )
    is_in = fields.Boolean(string="",  )
    is_out = fields.Boolean(string="",  )
    insurer = fields.Many2one('res.partner', string='Insurer',required=True, domain="[('insurer_type','=', True)]")
    discount_party = fields.Many2one('res.partner', string='Distribution Partner')
    issue_type = fields.Selection(string="Type", selection=[('Issue', 'Issue'), ('Refund', 'Refund'), ], required=False, compute='get_type',store=True,)
    total_premium_due = fields.Float(string="Total O/S Premiums",  required=False,compute='get_all_totals' ,digits=(12,2))
    total_payable_due = fields.Float(string="Total Premium Due" ,  required=False,compute='get_all_totals' ,)
    total_discount = fields.Float(string="Total Discount",  required=False,compute='get_all_totals' ,)
    total_brokerage = fields.Float(string="Brokerage After Tax",  required=False,compute='get_all_totals' ,)
    total_staff_comm = fields.Float(string="Total S.Commission",  required=False,compute='get_all_totals' ,)
    collected_by = fields.Many2one('hr.employee', string='Collector',)
    related_transaction = fields.Many2one(comodel_name="cashier.transaction", string="Related Transaction", required=False,readonly=True )
    is_cashier_out = fields.Boolean(string="",  )
    is_paid = fields.Boolean(string="",  )
    # total_net = fields.Float(string="",  required=False, )
    ncomm = fields.Selection(string="Premium/Commission", selection=[('Normal', 'Premium'), ('Commission', 'Commission'), ],default="Normal", required=False, )
    # @api.onchange('je_ids')
    # def total_net(self):
    #     x = 0
    #     for rec in self.je_ids:
    #         x += rec.net
    #     self.total_net = x
    #     print(self.total_net)
    @api.constrains('policy_ids','currency_id','insurer',)
    def transaction_constraint(self):
        if self.policy_ids:
            for rec in self.policy_ids:
                if  rec.insurer and self.insurer :
                    if rec.insurer != self.insurer:
                        raise ValidationError('O/S Premium Insurer Must be the same transaction insurer !')
                # if rec.discount_party and self.discount_party:
                #     if rec.discount_party != self.discount_party:
                #         raise ValidationError('O/S Premium  partner must be the same transaction partner !')
                if rec.currency_id and self.currency_id:
                    if rec.currency_id != self.currency_id:
                        raise ValidationError('O/S Premium Currency must be the same transaction Currency !')





    @api.one
    # @api.model
    @api.depends('policy_ids')
    def get_all_totals(self):
        x = 0
        d = 0
        p = 0
        s = 0
        t = 0
        print("hishishis")
        # if self.ncomm == 'Normal':
        if self.policy_ids :
                for rec in self.policy_ids:
                    x += rec.gross_premium
                    d += rec.discount
                    p += (rec.broker_comm-rec.tax)
                    s += rec.staff_commissions
                    t += rec.payable_gross_premium
                    self.total_premium_due = x
                    self.total_discount = d
                    self.total_brokerage = p
                    self.total_staff_comm = s
                    self.total_payable_due = t

    @api.depends('total_premium_due')
    def get_type(self):
        print(self.total_premium_due)
        if self.total_premium_due < 0 :
            self.issue_type = 'Refund'
        else:
            self.issue_type = 'Issue'
    @api.depends('transaction_type')
    def identify_type(self):
        if self.transaction_type == "Cashier In" or self.transaction_type == "Brokerage Cashier In":
            print('sssssssddffffggg')
            self.is_in = True
            self.is_out = False
            self.cashier_in_no = self.env['ir.sequence'].next_by_code('cashierin')
        elif self.transaction_type == "Cashier Out" or self.transaction_type == "Brokerage Cashier Out":
            self.is_out = True
            self.is_in = False
            self.cashier_out_no = self.env['ir.sequence'].next_by_code('cashierout')
        else:
            pass

    # @api.onchange('je_ids')
    # def get_summary(self):
    #     records = []
    #     summary = []
    #     for x in self.je_ids:
    #         records.append(x.account)
    #     print(records)
    #     print(set(records))
    #     records_set = set(records)
    #
    #     for x in records_set:
    #         amount = 0
    #         for n in self.je_ids:
    #             if n.account == x:
    #                 amount += n.net
    #         if amount > 0:
    #             new = (0, 0, {
    #                 'account': x,
    #                 'dc': 'DR',
    #                 'amount': amount,
    #             })
    #             summary.append(new)
    #         elif amount < 0:
    #             new = (0, 0, {
    #                 'account': x,
    #                 'dc': 'CR',
    #                 'amount': amount,
    #             })
    #             summary.append(new)
    #         else:
    #             pass
    #     print(summary)
    #     self.summary_ids = summary
    @api.multi
    def post_transaction(self):
        self.state = 'POST'

    @api.multi
    def cancel_transaction(self):
        if self.transaction_type == "Cashier In":
            for rec in self.policy_ids:
                rec.installment_transaction.state = 'outstanding'
            # self.policy_ids.unlink()
            self.state = 'canceled'
        elif self.transaction_type == "Cashier Out" and self.is_paid == False:
            self.related_transaction.is_cashier_out = False
            self.related_transaction.related_transaction=''
            for rec in self.policy_ids:
                rec.installment_transaction.state = 'collected'
            # self.policy_ids.unlink()
            self.state = 'canceled'
        elif self.transaction_type == "Cashier Out" and self.is_paid == True:
            for rec in self.policy_ids:
                rec.installment_transaction.state = 'cashier'
            # self.policy_ids.unlink()
            self.state = 'canceled'

    @api.multi
    def paid_transaction(self):
            for rec in self.policy_ids:
                rec.installment_transaction.state = 'delivered'
            self.is_paid = True
    @api.multi
    def cashier_out_return(self):
        form_view = self.env.ref('smart_collection_tracking.form_cashier_transaction')
        policies=[]
        for rec in self.policy_ids:
            object = (
                0, 0, {'installment_transaction': rec.installment_transaction.id,
                       'customer': rec.customer.id, 'discount_party': rec.discount_party.id,
                       'insurer': rec.insurer.id, 'gross_premium': rec.gross_premium,
                       'payable_gross_premium': rec.payable_gross_premium,
                       'due_comm':rec.due_comm,
                       'net': rec.net,
                       'discount': rec.discount,
                       'currency_id': rec.currency_id.id, 'broker_comm': rec.broker_comm, 'state': rec.state,
                       })
            policies.append(object)
        self.is_cashier_out = True

        return {
            'name': ('Transaction Cashier Out'),
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(form_view.id, 'form')],
            'res_model': 'cashier.transaction',
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {
                # 'default_total_premium_due':self.total_premium_due,
                # 'default_total_discount': self.total_discount,
                # 'default_total_brokerage': self.total_brokerage,
                'default_transaction_type': 'Cashier Out',
                'default_currency_id': self.currency_id.id,
                'default_customer': self.customer.id,
                'default_insurer': self.insurer.id,
                'default_discount_party': self.discount_party.id,
                'default_collected_by': self.collected_by.id,
                'default_related_transaction': self.id,
                'default_policy_ids': policies,
            },
        }

    @api.multi
    def cashier_in_print(self):
        return self.env.ref('smart_collection_tracking.cashier_in_transaction_report').report_action(self)

    @api.multi
    def cashier_out_print(self):
        return self.env.ref('smart_collection_tracking.cashier_out_transaction_report').report_action(self)
    @api.multi
    def confirm_transaction(self):
        # amount=0
        # count=len(self.policy_ids)
        #
        # # for x in self.policy_ids:
        # #     count += 1
        # for rec in self.je_ids:
        #     if rec.account.name == 'Broker Commission Revenue':
        #         amount += rec.amount
        # x = amount/count
        # print (amount)
        # print (count)
        # print (x)
        #
        # if amount > 0 :
        #     for rec in self.policy_ids:
        #         if rec.brokerage_net < x :
        #             rec.due_comm = x
        #         else:
        #             raise ValidationError('The Broker Commission Revenue account amount must be less than or equal the installment brokerage net')

        if self.transaction_type == 'Cashier In' and self.ncomm == "Normal":
            for rec in self.policy_ids:
                if rec.installment_transaction.state == 'outstanding':
                    rec.installment_transaction.state='collected'
                else:
                    raise ValidationError('All Selected Installment Must be in outstanding state !')
        elif self.transaction_type == 'Cashier Out' and self.ncomm == "Normal":
            for rec in self.policy_ids:
                if rec.installment_transaction.state == 'collected':
                    rec.installment_transaction.state='cashier'
                else:
                    raise ValidationError('All Selected Installment Must be in Cashier In state !')
            self.related_transaction.related_transaction = self.id
        elif self.transaction_type == 'Cashier In' and self.ncomm == "Commission":
            for rec in self.policy_ids:
                if rec.installment_transaction.state == 'delivered' and rec.due_comm == 0.00:
                    # rec.installment_transaction.state='cashier'
                    pass
                else:
# <<<<<<< HEAD
#                     raise ValidationError('All Selected Installment Must be in Paid state !')
# =======
                    raise ValidationError('All Selected Installment Must be in Paid state And Broker Commission not Paid !')
        x = 0
        t = 0
        d = 0
        b = 0
        no_of_disc = 0
        no_of_brok = 0
        if self.ncomm == 'Normal':
            for rec in self.je_ids:
                if rec.account.name == 'Discount Expenses':
                    no_of_disc += 1
                    if rec.dc == 'DR':
                        d += rec.amount
                    else:
                        d -= rec.amount
                if rec.account.name == 'O/S Premium Deposit':
                    # print("hahahahah")
                    if rec.dc == 'DR':
                        x += rec.amount
                    else:
                        x -= rec.amount
                if rec.account.name == 'Broker Commission Revenue':
                    no_of_brok += 1
                    if rec.dc == 'DR':
                        b += rec.amount
                    else:
                        b -= rec.amount
            if (self.transaction_type == 'Cashier In' and self.issue_type == 'Refund') or (
                    self.transaction_type == 'Cashier Out' and self.issue_type == 'Issue'):
                if (b + self.total_brokerage) != 0.00 and self.total_brokerage > 0.00 and no_of_brok > 0:
                    raise ValidationError('Check Your Brokerage Balance !')

                elif (b + self.total_brokerage) == 0.00 and self.total_brokerage > 0.00 and no_of_brok > 0:
                    for rec in self.policy_ids:
                        rec.installment_transaction.due_comm = rec.installment_transaction.total_broker_commissions
                        rec.commission_paid_date=datetime.today().strftime('%Y-%m-%d')



            elif (self.transaction_type == 'Cashier Out' and self.issue_type == 'Refund') or (
                    self.transaction_type == 'Cashier In' and self.issue_type == 'Issue'):
                if no_of_brok > 0:
                    raise ValidationError('You Should not insert Broker Commission in this state !')
            if (self.transaction_type == 'Cashier In' and self.issue_type == 'Issue') or (
                    self.transaction_type == 'Cashier Out' and self.issue_type == 'Refund'):
                if (d - self.total_discount) != 0.00 and self.total_discount > 0.00 and no_of_disc > 0:
                    raise ValidationError('Check Your Discount Expenses !')
            elif (self.transaction_type == 'Cashier Out' and self.issue_type == 'Issue') or (
                    self.transaction_type == 'Cashier In' and self.issue_type == 'Refund'):
                if no_of_disc > 0:
                    raise ValidationError('You Should not insert Discount Expenses in this state !')
            # print(x)
            print(self.total_premium_due)
            # print(self.issue_type)
            # print(self.transaction_type)
            if (x + self.total_premium_due) != 0.00:
                if (self.transaction_type == 'Cashier In') and (self.issue_type == 'Issue'):
                    raise ValidationError('Payment Should Match Premium Due !')
                if (self.transaction_type == 'Cashier Out') and (self.issue_type == 'Refund'):
                    raise ValidationError('Payment Should Match Premium Due !')

            if self.total_premium_due != x:
                if (self.transaction_type == 'Cashier Out' and self.issue_type == 'Issue') or (
                        self.transaction_type == 'Cashier In' and self.issue_type == 'Refund'):
                    raise ValidationError('Payment Should Match Premium Due !')
            for rec in self.je_ids:
                t += rec.net
            if t != 0:
                raise ValidationError('Accounts Should Balance !')
# <<<<<<< HEAD
#         self.state='Paid'
#
#
#
#
# =======
        else:
            b=0
            for rec in self.je_ids:
                if rec.account.name == 'Broker Commission Revenue':
                    if rec.dc == 'DR':
                        b += rec.amount
                    else:
                        b -= rec.amount
            if b == self.total_brokerage:
                for x in self.policy_ids:
                    x.installment_transaction.due_comm = x.installment_transaction.total_broker_commissions
                    today = datetime.today().strftime('%Y-%m-%d')
                    x.installment_transaction.commission_paid_date = fields.Date.today()
                    # fields.Date.today()
            else:
                raise ValidationError('Broker Commission Should Balance ')
        self.state='Paid'


    # @api.onchange('je_ids')
    # def broker_comm_calculate(self):

    # @api.constrains('je_ids','transaction_type')
    # def _compute_total_policies_constraint(self):
        # x = 0
        # t = 0
        # d = 0
        # b = 0
        # no_of_disc = 0
        # no_of_brok = 0
        # if self.ncomm == 'Normal':
        #     for rec in self.je_ids:
        #         if rec.account.name == 'Discount Expenses':
        #             no_of_disc += 1
        #             if rec.dc == 'DR':
        #                 d += rec.amount
        #             else:
        #                 d -= rec.amount
        #         if rec.account.name == 'O/S Premium Deposit':
        #             # print("hahahahah")
        #             if rec.dc == 'DR':
        #                 x += rec.amount
        #             else:
        #                 x -= rec.amount
        #         if rec.account.name == 'Broker Commission Revenue':
        #             no_of_brok += 1
        #             if rec.dc == 'DR':
        #                 b += rec.amount
        #             else:
        #                 b -= rec.amount
        #     if (self.transaction_type == 'Cashier In' and self.issue_type == 'Refund') or (self.transaction_type == 'Cashier Out' and self.issue_type == 'Issue'):
        #         if (b + self.total_brokerage) != 0.00 and self.total_brokerage > 0.00 and no_of_brok > 0 :
        #             raise ValidationError('Check Your Brokerage Balance !')
        #     elif (self.transaction_type == 'Cashier Out' and self.issue_type == 'Refund') or (self.transaction_type == 'Cashier In' and self.issue_type == 'Issue'):
        #         if no_of_brok > 0:
        #             raise ValidationError ('You Should not insert Broker Commission in this state !')
        #     if (self.transaction_type == 'Cashier In' and self.issue_type == 'Issue') or (self.transaction_type == 'Cashier Out' and self.issue_type == 'Refund'):
        #         if (d - self.total_discount)!= 0.00 and self.total_discount > 0.00 and no_of_disc > 0 :
        #             raise ValidationError('Check Your Discount Expenses !')
        #     elif (self.transaction_type == 'Cashier Out' and self.issue_type == 'Issue') or (self.transaction_type == 'Cashier In' and self.issue_type == 'Refund'):
        #         if no_of_disc > 0:
        #             raise ValidationError ('You Should not insert Discount Expenses in this state !')
        #     # print(x)
        #     print(self.total_premium_due)
        #     # print(self.issue_type)
        #     # print(self.transaction_type)
        #     if (x+self.total_premium_due) != 0.00:
        #         if (self.transaction_type == 'Cashier In' )and (self.issue_type == 'Issue') :
        #             raise ValidationError('Payment Should Match Premium Due !')
        #         if (self.transaction_type == 'Cashier Out') and (self.issue_type == 'Refund'):
        #             raise ValidationError('Payment Should Match Premium Due !')
        #
        #     if self.total_premium_due != x:
        #         if (self.transaction_type == 'Cashier Out' and self.issue_type == 'Issue') or (self.transaction_type == 'Cashier In' and self.issue_type == 'Refund'):
        #             raise ValidationError('Payment Should Match Premium Due !')
        #     for rec in self.je_ids:
        #         t += rec.net
        #     if t != 0 :
        #         raise ValidationError('Accounts Should Balance !')

class policyDetails(models.Model):
    _name = 'policy.details'
    # policy_id = fields.Many2one('policy.broker',string='Policy Number')
    installment_transaction = fields.Many2one("installment.installment", string="Installment Description",)
    customer = fields.Many2one('res.partner', string='Customer',readonly=True,related='installment_transaction.customer',store=True)
    discount_party = fields.Many2one('res.partner', 'Distribution Partner', readonly=True,related='installment_transaction.discount_party',store=True)
    insurer = fields.Many2one('res.partner', string='Insurer', readonly=True,related='installment_transaction.insurer',store=True)
    gross_premium = fields.Float(string="Gross Premium", readonly=True,related='installment_transaction.gross_premium' ,store=True)
    payable_gross_premium = fields.Float(string="Payable Gross Premium", readonly=True,related='installment_transaction.new_gross_premium' ,store=True)
    cashier_transaction_id = fields.Many2one(comodel_name="cashier.transaction", string="", required=False, )
    broker_comm = fields.Float(string="Brokerage", readonly=True,related='installment_transaction.total_broker_commissions' ,store=True)
    discount = fields.Float(string="Discount", readonly=True,related='installment_transaction.discount' ,store=True)
    staff_commissions = fields.Float(string="Staff Comm", readonly=True, required=False,related='installment_transaction.staff_commissions' )
    currency_id = fields.Many2one("res.currency", "Currency",readonly=True, copy=True,related='installment_transaction.currency_id')
    state = fields.Selection([('outstanding', 'Outstanding'),
                              ('collected', 'Cashier In'), ('cashier', 'Cashier Out'), ('delivered', 'Paid'),

                              ('canceled', 'Canceled')],

                             'State', required=True, default='outstanding',copy=True,related='installment_transaction.state')
    due_comm = fields.Float(string="Paid Brokerage",  required=False,readonly=True, copy=True,related='installment_transaction.due_comm' )
    net = fields.Float(string="Brokerage Due",  required=False,readonly=True, copy=True,related='installment_transaction.brokerage_net' )
    tax = fields.Float(string="Tax",  required=False,readonly=True, copy=True,related='installment_transaction.tax' )
class JE_Details(models.Model):
    _name = 'je.details'
    account = fields.Many2one(comodel_name="account.account", string="Account", required=False, )
    chq_date = fields.Date(string="CHQ Date", required=False, )
    chq_no = fields.Text(string="CHQ Details", required=False, )
    dc = fields.Selection(string="DR/CR", selection=[('DR', 'DR'), ('CR', 'CR'), ], required=False, )
    amount = fields.Float(string="Amount",  required=False, )
    net = fields.Float(string="Net", required=False, compute='calculate_net' , store=True)
    transaction_id = fields.Many2one(comodel_name="cashier.transaction", string="", required=False, )
    partner = fields.Many2one('res.partner', string='Partner',)
    is_in = fields.Boolean(string="",  )
    info = fields.Text(string="Another Info", required=False, )
    name = fields.Char(string="Trx ID", readonly=True,related='transaction_id.name' ,store=True)
    transaction_date = fields.Date(string="Transaction Date", readonly=True,related='transaction_id.transaction_date' ,store=True)
    related_transaction = fields.Many2one("cashier.transaction",string="Related Trx", readonly=True,related='transaction_id.related_transaction' ,store=True)
    collector = fields.Many2one("hr.employee",string="Collector", readonly=True,related='transaction_id.collected_by' ,store=True)
    currency_id = fields.Many2one("res.currency",string="Cur", readonly=True,related='transaction_id.currency_id' ,store=True)
    state = fields.Selection(selection=[('Draft', 'Draft'), ('Paid', 'Approved'), ],string= "State", copy=True,related='transaction_id.state')
    type = fields.Selection( selection=[('Cashier In', 'Cashier In'),('Cashier Out', 'Cashier Out'),('Brokerage Cashier In', 'Brokerage Cashier In'),('Brokerage Cashier Out', 'Brokerage Cashier Out'), ],string= "Type", copy=True,related='transaction_id.transaction_type' )
    insurer = fields.Many2one('res.partner', string='Insurer',readonly=True,related='transaction_id.insurer' ,store=True)

    @api.one
    @api.depends('dc','amount')
    def calculate_net(self):
        if self.dc == 'DR':
            self.net = self.amount
        else :
            self.net = self.amount * -1

class summary_track(models.Model):
    _name = 'summary.track'
    account = fields.Many2one(comodel_name="account.account", string="Account", required=False, )
    dc = fields.Selection(string="DR/CR", selection=[('DR', 'DR'), ('CR', 'CR'), ], required=False, )
    amount = fields.Float(string="Amount", required=False, )
    transaction_id = fields.Many2one(comodel_name="cashier.transaction", string="", required=False, )
class transaction_JE(models.Model):
    _inherit = 'account.move'

    transaction_id = fields.Many2one('cashier.transaction', string='Installment')
