
from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import xlsxwriter
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from io import BytesIO
import io


class CollectedBy(models.TransientModel):
    _name = "installments.collected"
    _description = "collected the selected installments"

    collected_emp = fields.Many2one('hr.employee', string='Collected By')

    @api.multi
    def print_authorization(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for record in self.env['installment.installment'].browse(active_ids):
            if record.state != 'outstanding':
                raise UserError(_(
                    "Selected installment(s) cannot be Collected by as they are not in 'outstanding' state.ins number %s" % record.name))
            record.write({'collected_by': self.collected_emp.id})
        return self.env.ref('smart_collection_tracking.authorization_report').report_action(active_ids)

    @api.multi
    def collected_by(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            if record.state != 'outstanding':
                raise UserError(_(
                    "Selected installment(s) cannot be Collected by as they are not in 'outstanding' state.ins number %s" % record.name))
            record.write({'collected_by': self.collected_emp.id})
            if not record.receipt_No:
                record.write({'receipt_No': self.env['ir.sequence'].next_by_code('receipt') or 'New'})
        return self.env.ref('smart_collection_tracking.Receipt_report').report_action(active_ids)


class CollectionConfirm(models.TransientModel):
    _name = "installments.collected.confirm"

    @api.multi
    def confirm_collected(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            if record.state != 'outstanding' or not record.payment_details_ids:
                raise UserError(_(
                    "Selected installment(s) cannot be confirmed as they are not in 'outstanding' state And you must insert payment details before cashier in ,, ins number %s" % record.name))
            record.write({'pay_date': fields.Date.today()})
            record.convert_collected()
        return self.env.ref('smart_collection_tracking.cashier_in_report').report_action(active_ids)


class excelReport(models.Model):
    _name = "excel.report"
    _rec_name = 'excel_sheet_name'
    is_confirmed = fields.Boolean(string="", compute='is_confirm')
    my_file = fields.Binary(string="Get Your File", )
    excel_sheet_name = fields.Char(string='Name', size=64)

    @api.one
    @api.depends('my_file')
    def is_confirm(self):
        print("sldlldkslk,,,f,f,")
        if self.my_file:
            self.is_confirmed = True

    @api.multi
    def get_excel_sheet(self):
        file_url = "http://127.0.0.1:8069/web/content?model=<module_name>&field=<field_name>&filename_field=<field_filename>&id=<object_id>"


    @api.multi
    def generate_excel(self):
        # try:

        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        # s=str(self.path)+'/'+'installment.xlsx'
        # print(s)
        self.excel_sheet_name = 'installment.xlsx'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)

        worksheet = workbook.add_worksheet()
        worksheet.write('A1', 'Policy Number')
        worksheet.write('B1', 'Endorsement No')
        worksheet.write('G1', 'Chassis No')
        worksheet.write('C1', 'Renewal/New')
        worksheet.write('D1', 'Customer')
        worksheet.write('E1', 'Insurer')
        worksheet.write('H1', 'Effective Date')
        worksheet.write('I1', 'Issue Date')
        worksheet.write('K1', 'Net Premium')
        worksheet.write('L1', 'Gross Premium')
        worksheet.write('M1', 'Discount')
        worksheet.write('N1', 'Payable Gross Premium')
        worksheet.write('J1', 'Sum Insured')
        worksheet.write('F1', 'In Favour Of')
        worksheet.write('O1', 'Beneficiary')

        # file_name = 'temp'
        i = 2
        for record in self.env['installment.installment'].browse(active_ids):
            policy = self.env['policy.broker'].search([('std_id', '=', record.policy_number)], order='id desc', limit=1)
            chassis = ''

            if policy:

                if policy.new_risk_ids:
                    for x in policy.new_risk_ids:
                       if x.chassis_no:
                        chassis +=str('-' + x.chassis_no)
                        print("1")
                else:
                    chassis = '-'

            else:
                chassis = '-'
                print("111")

            if record.is_renewal == True:
                r = 'Renewal'
            else:
                r = 'New'

            worksheet.set_column('A:A', 30)
            worksheet.write('A' + str(i), str(record.policy_number))
            if record.endorsement_no:
                worksheet.write('B' + str(i), str(record.endorsement_no))
            else:
                worksheet.write('B' + str(i), "Blank")
            worksheet.write('G' + str(i), str(chassis))
            worksheet.write('C' + str(i), str(r))
            worksheet.write('D' + str(i), str(record.customer.name))
            worksheet.write('E' + str(i), str(record.insurer.name))
            worksheet.write('H' + str(i), str(record.start_date))
            worksheet.write('I' + str(i), str(policy.issue_date))
            worksheet.write('K' + str(i), str(record.net_premium))
            worksheet.write('L' + str(i), str(record.gross_premium))
            worksheet.write('M' + str(i), str(record.discount))
            worksheet.write('N' + str(i), str(record.new_gross_premium))
            worksheet.write('J' + str(i), str(policy.total_sum_insured))
            worksheet.write('F' + str(i), str(policy.in_favour.name))
            worksheet.write('O' + str(i), str(policy.benefit))

            i += 1
        workbook.close()

        output.seek(0)
        self.write({'my_file': base64.encodestring(output.getvalue())})

        return {
            "type": "ir.actions.do_nothing",
        }


