from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class deliverBy(models.TransientModel):
    _name = "installments.delivered"
    _description = "deliver the selected installments"

    deliver_emp = fields.Many2one('hr.employee', string='Delivered By')



class CollectionConfirm(models.TransientModel):
    _name = "installments.delivered.confirm"
    _rec_name='receipt_no'
    payment_date = fields.Date(string="Payment Date", required=True, )
    receipt_no = fields.Char(string="Receipt No", required=False, )
    state = fields.Selection(string="Cash/Cheque", selection=[('Cheque', 'Cheque'), ('Cash', 'Cash'), ], required=False,default='Cheque' )
    receipt_att = fields.Binary(string="Receipt Attachment",  )
    installments_ids = fields.Many2many(comodel_name="installment.installment", string="", )
    @api.multi
    def confirm_collected(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['installment.installment'].browse(active_ids):
            if record.state != 'cashier' and record.state != 'outstanding':
                raise UserError(_("Selected installment(s) cannot be Paid as they are not in 'Cashier out' or  Outstanding ins number %s" % record.name))
            record.write({'delv_date_ins':self.payment_date})
            # record.write({'delv_date_ins':fields.Date.today()})
            record.convert_delivered()
        self.installments_ids =active_ids
        return {'type': 'ir.actions.act_window_close'}
class CollectionunConfirm(models.TransientModel):
    _name = "installments.undelivered.confirm"

    @api.multi
    def confirm_uncollected(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        installments=[]
        objects = self.env['cashier.transaction'].search(
            [  ('is_paid', '=' , True ), ],
            )
        for x in objects:
            for rec in x.policy_ids:
                installments.append(rec.installment_transaction.id)
        print(installments)
        for record in self.env['installment.installment'].browse(active_ids):
            if  record.state != 'delivered':
                raise UserError(_("Selected installment(s) cannot be UnPaid  as they are not in 'Paid' ins number %s" % record.name))
            # record.write({'delv_date_ins':fields.Date.today()})
            elif record.state == 'delivered' and record.id in installments:
                record.state ='cashier'
            elif record.state == 'delivered' and record.id not in installments:
                record.state = 'outstanding'
        return {'type': 'ir.actions.act_window_close'}