# -*- coding: utf-8 -*-
{
    'name': "Collection Tracking",
    'summary': """Collection Tracking Management & Operations""",
    'description': """Insurance Broker System """,
    'author': "Black Belts Egypt",
    'website': "www.blackbelts-egypt.com",
    'category': 'Collection',
    'version': '0.1',
    'license': 'AGPL-3',
    # any module necessary for this one to work correctly

    'depends': ['base', 'account', 'hr',],

    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/installments_view.xml',
        'views/discount_party_rate.xml',
        'reports/receipt.xml',
        'wizard/excel_report.xml',
        'reports/cashier_out.xml',
        'reports/authorization.xml',
        'reports/cashier_in_report.xml',
        'reports/cashier_in_transaction.xml',
        'reports/cashier_out_transaction.xml',
        'reports/JE_report.xml',
        'views/partner.xml',
        'views/mail_temp.xml',
        'wizard/assign_to _collector.xml',
        'wizard/assign_to_deliver.xml',
        'wizard/convert_transaction.xml',
        'wizard/generate_receipt.xml',
        'wizard/cashier_out.xml',
        'wizard/reports.xml',
        'views/cashier_tracking.xml',
        'views/commission_trackking.xml',
        'views/cashier_transaction.xml',
        'views/installment_menu.xml',

        'reports/report.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
