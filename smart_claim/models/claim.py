from odoo import models, fields, api ,_
from datetime import timedelta, datetime
from odoo.exceptions import UserError

from odoo.exceptions import ValidationError
from xlrd import open_workbook
import base64
import io
class claimPolicy(models.Model):
    _name ="insurance.claim"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string='Claim Number', required=True, copy=False, index=True ,default=lambda self: self.env['ir.sequence'].next_by_code('claim'),)
    intimation_date=fields.Date(string='Intimation Date',default=lambda self: fields.Datetime.now())
    dateofloss=fields.Date(string='Date of Loss',default=lambda self: fields.Datetime.now())
    causeofloss=fields.Many2one('insurance.setup',string='Cause of Loss',domain="[('setup_key','=','closs')]")
    lossdesc=fields.Text(string='Loss Description.')
    remarks=fields.Char(string='Close/Open Remarks')
    totalloss=fields.Boolean(string='Total Loss')
    totalsettled=fields.Float(string='Total Settled',compute='_compute_totalsettled',store=True)
    totalunpaid = fields.Float(string='Total Unpaid')
    insurer_claim_no = fields.Text(string="Insurer Claim Number", required=False, )
    policy_number = fields.Many2one('policy.broker',string='Policy Number')
    endorsement= fields.Char(related='policy_number.endorsement_no',string='Endorsement Number',store=True)
    related_policy=fields.Char(related='policy_number.std_id',readonly=True,store=True)
    customer_policy=fields.Many2one(related='policy_number.customer',string='Customer',store=True)
    beneficiary = fields.Char(related='policy_number.benefit',string='Beneficiary',readonly=True,store=True)
    currency = fields.Many2one(related='policy_number.currency_id',string="Currency",store=True)
    lob = fields.Many2one('insurance.line.business', string='Line of Business',store=True)
    insured=fields.Selection(related='lob.object',string='Insured',readonly=True,store=True)
    product = fields.Many2one(related='policy_number.product_policy', string='Product',readonly=True,store=True)
    insurer = fields.Many2one(related='policy_number.company', string='Insurer',store=True)
    insurer_branch = fields.Many2one(related='policy_number.branch', string='Insurer Branch')
    risk_ids = fields.One2many(related='policy_number.new_risk_ids' )
    installment_ids = fields.One2many(related='policy_number.rella_installment_id' )
    workshop = fields.Char(string="WorkShop", required=False, )
    insurer_contact = fields.Many2one('res.partner',string='Insurer Contact')
    settle_history = fields.One2many('settle.history','claimheader',string='Settlement/Payment')
    # payment_history = fields.One2many('payment.history','header_payment',string='Payment History')
    claim_action = fields.One2many('claim.document.action','claim_id',string='Action Document',readonly=False)
    # policy_docs = fields.One2many('policy.document','claim_id',string='Policy Document',compute='onchange_policy_document',store=True,readonly=False)
    state = fields.Selection([('new', 'New'),
                            ('init_est', 'Init Est'),
                            ('insurer', 'Insurer'),
                            ('settled', 'Invoiced'),
                            ('paid', 'Paid'),
                            ('closed', 'Closed'), ],string='State',required=True, default='new', group_expand='_expand_states',
       track_visibility='onchange', help='Status of the contract',)
    def _expand_states(self, states, domain, order):
        return [key for key, val in type(self).state.selection]
    chassis_no = fields.Char(string='Chassis No.')
    closed_reason = fields.Text(string='Closed Reason')
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    attachment_number_claim_docs = fields.Integer(compute='_compute_attachment_number_claim_docs', string='Number of Attachments')
    is_have_docs = fields.Boolean(string="",  )
    justification_ids = fields.One2many(comodel_name="difference.justification", inverse_name="claim_id", string="Difference Justification", required=False, )
    totalvalue = fields.Float(string="",  required=False,compute='get_totalvalue' ,store=True)
    totalinvoiced = fields.Float(string="",  required=False,compute='calculate_total_invoice' )
    totalpaid = fields.Float(string="",  required=False, compute='calculate_total_paid')
    @api.onchange('lob')
    def get_diffrence_justification(self):
        print("mgtsh leh !")
        objects= self.env['insurance.setup'].search([('setup_key','=','clmitem')])
        if self.insured == 'vehicle':
            records = []
            for rec  in objects:
                new = (0, 0, {'item': rec.id})
                records.append(new)
            self.justification_ids = records
    @api.one
    @api.depends('settle_history')
    def calculate_total_invoice(self):
        if self.settle_history:
            self.totalinvoiced = 0.0
            for record in self.settle_history:
                self.totalinvoiced += record.invoice_amount

    @api.one
    @api.depends('settle_history')
    def calculate_total_paid(self):
        if self.settle_history:
            self.totalpaid = 0.0
            for record in self.settle_history:
                self.totalpaid += record.paid_amount
    @api.constrains('justification_ids')
    def _compute_totalsettled_constraint(self):
        if self.justification_ids:
            if self.totalvalue != self.totalsettled:
                raise ValidationError('Total Value of Difference Justification Must be %s'%self.totalsettled)
    @api.one
    @api.depends('policy_number')
    def _compute_attachment_number(self):
        if self.policy_number:
            s = self.env['ir.attachment'].search(['&', ('res_id', '=', self.policy_number.id), ('res_model', '=', 'policy.broker')])
            self.attachment_number = len(s)
            if len(s) > 0:
                self.is_have_docs = True

    @api.one
    @api.depends('claim_action')
    def _compute_attachment_number_claim_docs(self):
        if self.claim_action:
            x=0
            for rec in self.claim_action:
                if rec.attachment :
                    x+=1
            self.attachment_number_claim_docs=x




    @api.multi
    def action_get_attachment_tree_view(self):
        action = self.env.ref('base.action_attachment').read()[0]
        action['context'] = {
            'default_res_model': self._name,
            'default_res_id': self.ids[0]
        }
        # action['search_view_id'] = (self.env.ref('ir.attachment').id,)
        action['domain'] = ['&', ('res_model', '=', 'policy.broker'), ('res_id', '=', self.policy_number.id)]
        return action

    @api.multi
    def action_get_attachment_tree_view_claim(self):
        tree_view_id=self.env.ref('smart_claim.claim_action_docs_tree').id
        form_view = self.env.ref('smart_claim.claim_action_docs_form')
        return {
            'name': ('Claim Docs'),
            # 'view_type': 'tree',
            'view_mode': 'tree,form',
            'res_model': 'claim.document.action',  # model name ?yes true ok
            'target': 'current',
            'views': [(tree_view_id, 'tree'), (form_view.id, 'form')],
            'type': 'ir.actions.act_window',
            'domain': [('claim_id', '=', self.id)],
            'flags': {'tree': {'action_buttons': True}, 'form': {'action_buttons': True},},

        }


    @api.onchange('lob')
    def onchange_action_document(self):
        print("hya de hya")
        values = []
        if self.lob:
            for action in self.lob.claim_action:
                values.append(action.action)
        records= []
        for val in values:
            new = (0, 0, {'action':val,
                          'claim_id':self.id})
            records.append(new)
        self.claim_action = records

    @api.multi
    @api.onchange('chassis_no')
    def _compute_policy(self):
        if self.chassis_no and not self.policy_number:
            date1=datetime.today().strftime('%d-%m-%Y')
            print(date1)
            try:
                policy=self.env['policy.risk'].search([('chassis_no','=',self.chassis_no),('policy_risk_id.start_date','<=',self.dateofloss),('policy_risk_id.active','=',True),('policy_risk_id.end_date','>',date1)]).policy_risk_id

                self.policy_number =policy
            except:
                raise UserError(_(
                    'Error Chaise Has More One Active Policy  OR   Has no Policy \n'
                    'Please Check Insured History '))

    # @api.one
    # def _set_policy(self):
    #     if self.chassis_no != " " and self.insured !='vehicle':
    #         return True
    #     else:
    #         return self._compute_policy



    @api.multi
    def convert_init_est(self):
        self.state = 'init_est'
    @api.multi
    def convert_insurer(self):
        self.state = 'insurer'
    @api.multi
    def convert_settled(self):
        self.state = 'settled'
    @api.multi
    def convert_paid(self):
        self.state = 'paid'
    @api.multi
    def convert_closed(self):
        self.state = 'closed'


    @api.multi
    def print_claim(self):
        return self.env.ref('smart_claim.insurance_claim').report_action(self)

    @api.multi
    def send_mail_template_claim(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('smart_claim.claim_email_template')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'insurance.claim',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


    @api.onchange('insurer')
    def _onchange_insurer_contact(self):
        if self.insurer:
            return {'domain': {'insurer_contact': [('parent_id','=',self.insurer.id)]}}




    @api.onchange('total_paid_amount','totalclaimexp')
    def _onchange_total_unpaid(self):
        if self.total_paid_amount and self.totalclaimexp:
            self.totalunpaid = self.totalclaimexp - self.total_paid_amount

    #
    @api.one
    @api.depends('settle_history')
    def _compute_totalsettled(self):
        if self.settle_history:
            self.totalsettled = 0.0
            for record in self.settle_history:
                self.totalsettled += record.difference

    @api.one
    @api.depends('justification_ids')
    def get_totalvalue(self):
        if self.justification_ids:
            self.totalvalue = 0.0
            for record in self.justification_ids:
                self.totalvalue += record.value

    # attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    #
    # @api.one
    # def _compute_attachment_number(self):
    #     s = self.env['ir.attachment'].search(['&', ('res_id', '=', self.id), ('res_model', '=', 'insurance.claim')])
    #     self.attachment_number = len(s)
    #     # pass
    # @api.multi
    # def action_get_attachment_tree_view(self):
    #     print("ssss")
    #     action = self.env.ref('base.action_attachment').read()[0]
    #     action['context'] = {
    #         'default_res_model': self._name,
    #         'default_res_id': self.ids[0]
    #     }
    #     # action['search_view_id'] = (self.env.ref('ir.attachment').id,)
    #     action['domain'] = ['&', ('res_model', '=', 'insurance.claim'), ('res_id', '=', self.id)]
    #     return action

class claimActionDocument(models.Model):
    _name = 'claim.document.action'
    _rec_name='action'
    action = fields.Char('Claim Action')
    date =fields.Datetime('Date')
    attachment =fields.Binary('Attachment')
    claim_id = fields.Many2one('insurance.claim', ondelete='cascade', index=True)
    is_true = fields.Boolean(string="",compute='get_null' ,store=True )
    file_name = fields.Char(string="", required=False, )
    doc_ids = fields.One2many(comodel_name="claim.document.details", inverse_name="action_id", string="", required=False, )

    @api.one
    @api.depends('attachment')
    def get_null(self):
        if not self.attachment:
            self.is_true = True
class claimActionDocument2(models.Model):
    _name = 'claim.document.details'
    # _rec_name='action'
    # action = fields.Char('Claim Action')
    # date =fields.Datetime('Date')
    attachment =fields.Binary('Attachment')
    action_id = fields.Many2one('claim.document.action', ondelete='cascade', index=True)
    is_true = fields.Boolean(string="",compute='get_null' ,store=True )
    file_name = fields.Char(string="", required=False, )

    @api.one
    @api.depends('attachment')
    def get_null(self):
        if not self.attachment:
            self.is_true = True




class settleHistory(models.Model):
    _name ="settle.history"

    risk_type = fields.Selection(related='claimheader.insured',string='Risk Type',readonly=True,store=True)
    policy_settle_id = fields.Many2one(related='claimheader.policy_number',store=True)
    risk_id = fields.Many2one('policy.risk',string='Risk',domain="[('policy_risk_id','=',policy_settle_id)]")
    risk = fields.Char(related='risk_id.risk_description')

    #Vehicle details
    vcar_type = fields.Many2one(related='risk_id.car_tybe')
    vbrande = fields.Many2one(related='risk_id.Man')
    vmodel = fields.Many2one(related='risk_id.model')
    state_type = fields.Selection(related='risk_id.state_type')
    year_of_made = fields.Char(related='risk_id.year_of_made')
    country_id = fields.Many2one(related='risk_id.country_id')
    motor_cc = fields.Char(related='risk_id.motor_cc')
    chassis_no = fields.Char(related='risk_id.chassis_no')
    plate_no = fields.Char(related='risk_id.plate_no')
    engine = fields.Char(related='risk_id.engine')


    #Person details
    name = fields.Char(related='risk_id.name')
    DOB = fields.Date(related='risk_id.DOB')
    job = fields.Many2one(related='risk_id.job')
    family_member = fields.Selection(related='risk_id.family_member')

    #Cargo details
    cargo_type = fields.Many2one(related='risk_id.cargo_type')
    From = fields.Char(related='risk_id.From')
    To = fields.Char(related='risk_id.To')

    # location
    type = fields.Many2one(related='risk_id.type')
    address = fields.Char(related='risk_id.address')



    sum_insured = fields.Float(related='risk_id.sum_insured',string='Sum Insured',store=True,readonly=True)
    invoice_amount = fields.Float(string="Invoiced Amount",  required=False, )
    sent_on_date = fields.Date(string="Invoice Sent On", required=False, )
    paid_amount = fields.Float(string="Paid Amount",  required=False, )
    payment_date = fields.Date(string="Payment Date", required=False, )
    difference = fields.Float(string="Difference",  required=False,compute='get_different' )
    check_number=fields.Char(string='Cheque Number')
    check_date = fields.Date(string="Cheque Date", required=False, )
    check_bank=fields.Many2one('res.bank',string='Bank')
    bank_transfer = fields.Text(string="Bank Transfer", required=False, )
    claimheader = fields.Many2one(comodel_name="insurance.claim", string="", required=False, )

    @api.constrains('payment_date','sent_on_date')
    def _compute_date_constraint(self):
        if self.payment_date and self.sent_on_date:
            if self.sent_on_date > self.payment_date:
                raise ValidationError('Payment Date Must be After Invoice date')
    @api.multi
    def name_get(self):
        result = []
        for s in self:
            result.append((s.id, s.risk))
        return result
    @api.one
    @api.depends('paid_amount','invoice_amount')
    def get_different(self):
        print("zazazaza")
        self.difference=self.invoice_amount - self.paid_amount


class diffJustify(models.Model):
    _name = 'difference.justification'
    item = fields.Many2one(comodel_name="insurance.setup", string="Item", required=False, domain="[('setup_key','=','clmitem')]")
    value = fields.Float(string="Value",  required=False, )
    description = fields.Text(string="Description", required=False, )
    claim_id = fields.Many2one(comodel_name="insurance.claim", string="", required=False, )