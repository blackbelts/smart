import logging
import math
from datetime import timedelta, time
from datetime import datetime
from odoo import api, fields, models
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools import float_compare
from odoo.tools.translate import _
from dateutil.relativedelta import relativedelta

# from odoo import xlrd
from odoo import models, fields, api, exceptions, _


import xlrd
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


import time



from xlrd import open_workbook
from tempfile import TemporaryFile
import base64


_logger = logging.getLogger(__name__)

HOURS_PER_DAY = 8


class inhertAttendance(models.Model):
    _inherit = 'hr.attendance'


    file = fields.Binary(string='Attendance File')

    requests=fields.Many2one('hr.holidays',string='Requests',compute='giiit')
    vacation=fields.Boolean(string='Absent')
    check_in = fields.Datetime(string="Check In", default=False, required=False)

    late =fields.Char(string='Late')
    Early=fields.Char(string='Early')
    OT=fields.Char(string='OT')
    date = fields.Date(string="Date", required=False, )

    @api.multi
    def name_get(self):
        result = []
        for attendance in self:
            if not attendance.check_out:
                result.append((attendance.id, _("%(empl_name)s") % {
                    'empl_name': attendance.employee_id.name,
                }))
            else:
                result.append((attendance.id, _("%(empl_name)s") % {
                    'empl_name': attendance.employee_id.name,
                }))
        return result


    @api.depends('check_in', 'check_out')
    def _compute_worked_hours(self):
        for attendance in self:
            if attendance.check_out and attendance.check_in:
                delta = datetime.strptime(attendance.check_out, DEFAULT_SERVER_DATETIME_FORMAT) - datetime.strptime(
                    attendance.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                attendance.worked_hours = delta.total_seconds() / 3600.0

    # @api.depends('check_in', 'check_out')
    # def _compute_worked_hours(self):
    #     for attendance in self:
    #         if attendance.check_out and attendance.check_in:
    #             delta = datetime.strptime(attendance.check_out, DEFAULT_SERVER_DATETIME_FORMAT) - datetime.strptime(
    #                 attendance.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
    #             attendance.worked_hours = delta.total_seconds() / 3600.0

    @api.constrains('check_in', 'check_out', 'employee_id')
    def _check_validity(self):
        """ Verifies the validity of the attendance record compared to the others from the same employee.
            For the same employee we must have :
                * maximum 1 "open" attendance record (without check_out)
                * no overlapping time slices with previous employee records
        """
        # for attendance in self:
        #     # we take the latest attendance before our check_in time and check it doesn't overlap with ours
        #     last_attendance_before_check_in = self.env['hr.attendance'].search([
        #         ('employee_id', '=', attendance.employee_id.id),
        #         ('check_in', '<=', attendance.check_in),
        #         ('id', '!=', attendance.id),
        #     ], order='check_in desc', limit=1)


    @api.model
    def giiit(self):
        if self.check_in==False:
            self.vacation=True

    # @api.one
    # @api.onchange('check_out')
    # def generate(self):
    #     print(self.employee_id.is_absent_totay)
    #     print(555555555555555555555)
    #     ids=self.env['hr.holidays'].search([('employee_id', '=', self.employee_id.id),('type','=','remove'),('state','=','validate'),('date_from','<=',self.check_in),('date_to','>=',self.check_out)]).id
    #     self.requests=ids

    @api.one
    def giiit(self):
        if self.check_in==False:
            self.vacation=True
        print("sdkmsdkd")
        ids=self.env['hr.holidays'].search([('employee_id', '=', self.employee_id.id),('type','=','remove'),('state','=','validate'),('date_from','<=',self.date),('date_to','>=',self.date)]).id
        print(ids)
        self.requests=ids


    @api.multi
    def import_file(self):
        wb = open_workbook(file_contents=base64.decodestring(self.file))
        sheet = wb.sheets()[0]

        for s in wb.sheets():


            values = []
            vals=[]

            date_in=datetime
            date_out=datetime
            vacation=False

            for row in range(1, s.nrows):

                col_value = []

                for col in range(s.ncols):

                    value = (s.cell(row, col).value)
                    try:
                        value = str(int(value))
                    except:
                        pass
                    col_value.append(value)

                if col_value[3] =="" and col_value[4]=="":
                     super(inhertAttendance, self).create({
                        'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),'date':datetime.strptime(col_value[2], '%d/%m/%Y'),
                        'vacation':True})
                     print('777777777777777777777777777')
                elif col_value[4] == "" and col_value[3]!="":
                    super(inhertAttendance, self).create({
                        'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),'check_in':datetime.strptime(col_value[2]+" "+col_value[3],'%d/%m/%Y %H:%M'),'date':datetime.strptime(col_value[2], '%d/%m/%Y'),
                        })
                    print(']]]jjjjjjjjjjjjjjjjj')
                elif col_value[4] != "" and col_value[3] == "":
                    super(inhertAttendance, self).create({
                        'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),
                        'check_out': datetime.strptime(col_value[2] + " " + col_value[4], '%d/%m/%Y %H:%M'),'date':datetime.strptime(col_value[2], '%d/%m/%Y')
                    })
                    print(']]]jjjjjjjjjjjjjjjjj')
                # if col_value[3] =="" and col_value[4]=="":
                #      super(inhertAttendance, self).create({
                #         'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),
                #         'vacation':True})
                #      print('4444444444444444')
                #     col_value[3]="00:00"
                # if col_value[4] == "":
                #     col_va    lue[4] = "00:00"
                # print(datetime.strptime(col_value[2]+" "+col_value[3],'%d/%m/%Y %H:%M'))
                # print(col_value[5]+']]]jjjjjjjjjjjjjjjjj')
                # print((self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id))

                else :
                    print('55555555555555555555555555555')
                    super(inhertAttendance, self).create({
                        'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),'check_in':datetime.strptime(col_value[2]+" "+col_value[3],'%d/%m/%Y %H:%M'),'check_out':datetime.strptime(col_value[2]+" "+col_value[4],'%d/%m/%Y %H:%M')
                        ,'late':col_value[5],'Early':col_value[6],'OT':col_value[8],'date':datetime.strptime(col_value[2], '%d/%m/%Y')})

            # values.append(col_value)
            print(values)


                # if col_value[3] =="" and col_value[4]=="":
                #     date1 = datetime.strptime(col_value[2], '%d/%m/%Y')
                #     date_in=''
                #     date_out=''
                #     vacation=True
                # elif col_value[4] == "" and col_value[3]!="":
                #     date1 = datetime.strptime(col_value[2], '%d/%m/%Y')
                #     date_in=datetime.strptime(col_value[2] + " " + col_value[3], '%d/%m/%Y %H:%M')
                #     date_out=''
                # elif col_value[3]=="" and col_value[4]!="":
                #     date1 = datetime.strptime(col_value[2], '%d/%m/%Y')
                #     print("sssss")
                #
                #     date_in=''
                #     date_out=datetime.strptime(col_value[2] + " " + col_value[4], '%d/%m/%Y %H:%M')
                #
                #     print(col_value[4])
                #
                # else:
                #     date1 = datetime.strptime(col_value[2], '%d/%m/%Y')
                #     date_in=datetime.strptime(col_value[2] + " " + col_value[3], '%d/%m/%Y %H:%M')
                #     date_out=datetime.strptime(col_value[2] + " " + col_value[4], '%d/%m/%Y %H:%M')
                # super(inhertAttendance, self).create({
                #                     'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),
                #                     'check_in': date_in,
                #                     'check_out':date_out,
                #                     'late': col_value[5],
                #                     'Early': col_value[6],
                #                     'OT': col_value[8],
                #                     'date': date1,
                #                     'vacation': vacation})
                # 'employee_id': (self.env['hr.employee'].search([('employee_id', '=', a[i][0])]).id),
                # 'check_in': datetime.strptime(a[i][2] + " " + a[i][3], '%d/%m/%Y %H:%M'),
                # 'check_out': datetime.strptime(a[i][2] + " " + a[i][4], '%d/%m/%Y %H:%M'),
                # 'late': a[i][5],
                # 'Early': a[i][6],
                # 'OT': a[i][8],
                # 'date': datetime.strptime(a[i][2], '%d/%m/%Y'),})
                # i += 1
                # a.clear()
                # print(a)





#
#
#
#
#
#
#
#
# import logging
# import math
# from datetime import timedelta, time
# from datetime import datetime
# from odoo import api, fields, models
# from odoo.exceptions import UserError, AccessError, ValidationError
# from odoo.tools import float_compare
# from odoo.tools.translate import _
# from dateutil.relativedelta import relativedelta
# # from odoo import xlrd
# from odoo import models, fields, api, exceptions, _
# import time
#
# import xlrd
# # from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
#
# from xlrd import open_workbook
# from tempfile import TemporaryFile
# import base64
#
#
# _logger = logging.getLogger(__name__)
#
# HOURS_PER_DAY = 8
#
#
# class inhertAttendance(models.Model):
#     _inherit = 'hr.attendance'
#
#
#     file = fields.Binary(string='Attendance File')
#     requests=fields.Many2one('hr.holidays',string='Requests',compute='giiit')
#     vacation=fields.Boolean(string='Absent')
#     check_in = fields.Datetime(string="Check In", default=False, required=False)
#
#     late =fields.Char(string='Late')
#     Early=fields.Char(string='Early')
#     OT=fields.Char(string='OT')
#     date = fields.Date(string="Date", required=False, )
#     @api.multi
#     def name_get(self):
#         result = []
#         for attendance in self:
#             if not attendance.check_out:
#                 result.append((attendance.id, _("%(empl_name)s") % {
#                     'empl_name': attendance.employee_id.name,
#                 }))
#             else:
#                 result.append((attendance.id, _("%(empl_name)s") % {
#                     'empl_name': attendance.employee_id.name,
#                 }))
#         return result
#
#     # @api.depends('check_in', 'check_out')
#     # def _compute_worked_hours(self):
#     #     for attendance in self:
#     #         if attendance.check_out and attendance.check_in:
#     #             delta = datetime.strptime(attendance.check_out, DEFAULT_SERVER_DATETIME_FORMAT) - datetime.strptime(
#     #                 attendance.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
#     #             attendance.worked_hours = delta.total_seconds() / 3600.0
#
#     @api.constrains('check_in', 'check_out', 'employee_id')
#     def _check_validity(self):
#         """ Verifies the validity of the attendance record compared to the others from the same employee.
#             For the same employee we must have :
#                 * maximum 1 "open" attendance record (without check_out)
#                 * no overlapping time slices with previous employee records
#         """
#         # for attendance in self:
#         #     # we take the latest attendance before our check_in time and check it doesn't overlap with ours
#         #     last_attendance_before_check_in = self.env['hr.attendance'].search([
#         #         ('employee_id', '=', attendance.employee_id.id),
#         #         ('check_in', '<=', attendance.check_in),
#         #         ('id', '!=', attendance.id),
#         #     ], order='check_in desc', limit=1)
#
#     @api.one
#     def giiit(self):
#         if self.check_in==False:
#             self.vacation=True
#         print("sdkmsdkd")
#         ids=self.env['hr.holidays'].search([('employee_id', '=', self.employee_id.id),('type','=','remove'),('state','=','validate'),('date_from','<=',self.date),('date_to','>=',self.date)]).id
#         print(ids)
#         self.requests=ids
#
#
#     @api.multi
#     def import_file(self):
#         wb = open_workbook(file_contents=base64.decodestring(self.file))
#         sheet = wb.sheets()[0]
#
#         for s in wb.sheets():
#             i=0
#             values = []
#             a = []
#             for row in range(1, s.nrows):
#
#                 col_value = []
#
#                 for col in range(s.ncols):
#
#                     value = (s.cell(row, col).value)
#                     try:
#                         value = str(int(value))
#                     except:
#                         pass
#                     col_value.append(value)
#
#                 if col_value[3] =="" and col_value[4]=="":
#                      super(inhertAttendance, self).create({
#                         'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),
#                         'vacation':True})
#                 elif col_value[4] == "" and col_value[3]!="":
#                     super(inhertAttendance, self).create({
#                         'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),
#                         'check_in':datetime.strptime(col_value[2]+" "+col_value[3],'%d/%m/%Y %H:%M' ,),
#                         'date':datetime.strptime(col_value[2],'%d/%m/%Y')
#                         })
#                 elif col_value[4] != "" and col_value[3] == "":
#                     super(inhertAttendance, self).create({
#                         'employee_id': (self.env['hr.employee'].search([('employee_id', '=', col_value[0])]).id),
#                         'check_out': datetime.strptime(col_value[2] + " " + col_value[4], '%d/%m/%Y %H:%M'),
#                         'date':datetime.strptime(col_value[2],'%d/%m/%Y')
#                     })
#                 else:
#                     a.append(col_value)
#                     date1 = datetime.strptime(a[i][2], '%d/%m/%Y')
#                     print(a)
#                 super(inhertAttendance, self).create({
#                 'employee_id': (self.env['hr.employee'].search([('employee_id', '=', a[i][0])]).id),
#                 'check_in': datetime.strptime(a[i][2] + " " + a[i][3], '%d/%m/%Y %H:%M'),
#                 'check_out': datetime.strptime(a[i][2] + " " + a[i][4], '%d/%m/%Y %H:%M'),
#                 'late': a[i][5],
#                 'Early': a[i][6],
#                 'OT': a[i][8],
#                 'date': date1,})
#                 i += 1
#                 # a.clear()
#                 # print(a)