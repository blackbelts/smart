from odoo import models, fields, api
from odoo.exceptions import ValidationError


class inhertResPartner(models.Model):
    _inherit = 'res.partner'

    opp_count = fields.Integer(compute='_compute_opp_count')

    @api.one
    def _compute_opp_count(self):
        if self.customer == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.opp_count = self.env['crm.lead'].search_count(
                    [('partner_id', '=', self.id)])

        elif self.insurer_type == 1:
            for partner in self:
                proposal = self.env['proposal.opp.bb'].search([('Company', '=', self.id)]).ids
                partner.opp_count = self.env['crm.lead'].search_count(
                    [('proposal_opp', 'in', proposal)])
        elif self.agent == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.opp_count = self.env['crm.lead'].search_count(
                    [('user_id.partner_id', '=', self.id)])
        elif self.key_account == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.opp_count = self.env['crm.lead'].search_count(
                    [('discount_party', '=', self.id)])

    @api.multi
    def show_partner_needs(self):
        tree_view = self.env.ref('Smart_crm.customer_needs_tree')
        # form_view = self.env.ref('Smart_crm.crm__lead_form_view')
        if self.customer == 1:
            return {
                'name': ('Customer Needs'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'customer.needs',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_customer_id': self.id},
                'domain': [('customer_id', '=', self.id)]
            }
    @api.multi
    def show_partner_opp(self):
        tree_view = self.env.ref('Smart_crm.ibs_crm_case_tree_view_oppor')
        form_view = self.env.ref('Smart_crm.crm__lead_form_view')
        if self.customer == 1:
            return {
                'name': ('Opportunity'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'crm.lead',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_partner_id': self.id},
                'domain': [('partner_id', '=', self.id)]
            }
        elif self.insurer_type == 1:
            return {
                'name': ('Opportunity'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'crm.lead',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'domain': [('proposal_opp.Company', '=', self.id)],

            }
        elif self.agent == 1:
            return {
                'name': ('Opportunity'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'crm.lead',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'domain': [('user_id.partner_id', '=', self.id)],
            }
        elif self.key_account == 1:
            return {
                'name': ('Opportunity'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'crm.lead',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'domain': [('discount_party', '=', self.id)],
            }

    @api.multi
    def partner_report_opp(self):
        if self.insurer_type:
            proposal = self.env['proposal.opp.bb'].search([('Company', '=', self.id)]).ids
            opp = self.env['crm.lead'].search([('proposal_opp', 'in', proposal)])
            return opp
        elif self.customer:
            opp = self.env['crm.lead'].search([('partner_id', '=', self.id)])
            return opp
        elif self.agent:
            print("***************")
            opp = self.env['crm.lead'].search([('user_id.partner_id', '=', self.id)])
            return opp
    # @api.one
    # def _compute_claim_count(self):
    #     if self.customer == 1:
    #         for partner in self:
    #             operator = 'child_of' if partner.is_company else '='
    #             partner.claim_count = self.env['insurance.claim'].search_count(
    #                 [('customer_policy', operator, partner.id)])
    #     elif self.insurer_type == 1:
    #         for partner in self:
    #             operator = 'child_of' if partner.is_company else '='
    #             partner.claim_count = self.env['insurance.claim'].search_count(
    #                 [('insurer', operator, partner.id)])
    #     elif self.agent == 1:
    #         for partner in self:
    #             operator = 'child_of' if partner.is_company else '='
    #             policy = self.env['policy.broker'].search(
    #                 [('salesperson', operator, partner.id)]).ids
    #             partner.claim_count = self.env['insurance.claim'].search_count(
    #                 [('policy_number', operator, policy)])
    #
    #
    #
    # @api.multi
    # def show_partner_claim(self):
    #     if self.customer == 1:
    #         return {
    #             'name': ('Claim'),
    #             'view_type': 'form',
    #             'view_mode': 'tree,form',
    #             'res_model': 'insurance.claim',  # model name ?yes true ok
    #             'target': 'current',
    #             'type': 'ir.actions.act_window',
    #             'context': {'default_customer_policy': self.id},
    #             'domain': [('customer_policy', '=', self.id)]
    #         }
    #     elif self.insurer_type == 1:
    #         return {
    #             'name': ('Claim'),
    #             'view_type': 'form',
    #             'view_mode': 'tree,form',
    #             'res_model': 'insurance.claim',  # model name ?yes true ok
    #             'target': 'current',
    #             'type': 'ir.actions.act_window',
    #             'context': {'default_insurer': self.id},
    #             'domain': [('insurer', '=', self.id)]
    #         }
    #     elif self.agent == 1:
    #         return {
    #             'name': ('Claim'),
    #             'view_type': 'form',
    #             'view_mode': 'tree,form',
    #             'res_model': 'insurance.claim',  # model name ?yes true ok
    #             'target': 'current',
    #             'type': 'ir.actions.act_window',
    #             # 'context': {'default_agent': self.id},
    #             'domain': [('policy_number.salesperson', '=', self.id)]
    #         }
class customerneeds(models.Model):
    _name = 'customer.needs'
    LOB = fields.Many2one('insurance.line.business', string='Line of business',required=True,)
    note = fields.Text(string="Note", required=False, )
    customer_id = fields.Many2one(comodel_name="res.partner", string="", required=False, )

    @api.multi
    def convert_to_opp(self):
        print("skdjsjhfjkdshkhjdj5555555555555")
        return {
            'name': ('opp'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'view_id': [(self.env.ref('crm.crm_case_form_view_oppor').id), 'form'],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': {'default_partner_id': self.customer_id.id,
                        'default_LOB': self.LOB.id
                        },

            # 'domain': [('id', '=', self.policy_risk_id.id)],
            # 'res_id': self.policy_risk_id.id,

        }

    # class customerneeds(models.Model):
    #     _name = 'customer.needs'
    #     needs_id = fields.One2many(comodel_name="needs.list", inverse_name="customer_id", string="", required=False, )
    #     customer_id = fields.Many2one(comodel_name="res.partner", string="", required=False, )
