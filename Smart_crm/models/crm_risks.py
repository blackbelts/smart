from odoo import models, fields, api
from odoo.exceptions import ValidationError


class New_Risks(models.Model):
    _inherit = "policy.risk"
    _rec_name = 'risk_description_crm'

    @api.one
    @api.depends('risks_crm')
    def _compute_risk_description_crm(self):
        if self.risks_crm:
            if self.type_risk == "person":
                self.risk_description_crm = (str(self.job.setup_id) if self.job.setup_id else " " + "_") + "   " + (
                    str(self.name) + " - " if self.name else " " + "_") + "   " + (
                                                str(self.DOB) + " - " if self.DOB else " " + "_") + "   " + "   " + (
                                                str(self.family_member) if self.family_member else " " + "_")

            if self.type_risk == "vehicle":
                self.risk_description_crm = (str(
                    self.car_tybe.setup_id) + ' - ' if self.car_tybe.setup_id else " " + "_") + "   " + (
                                                str(
                                                    self.Man.setup_id) + " - " if self.Man.setup_id else " " + "_") + "  " + (
                                                str(
                                                    self.model.setup_id) + " - " if self.model.setup_id else " " + "_") + "   " + (
                                                str(
                                                    self.country_id.setup_id) + " - " if self.country_id.setup_id else " " + "_") + "   " + (
                                                str(
                                                    self.year_of_made) + " - " if self.year_of_made else " " + "_") + "  " + "VCC: " + (
                                                str(self.motor_cc) if self.motor_cc else " " + "_") + "  " + "PN: " + (
                                                str(
                                                    self.plate_no) + ' - ' if self.plate_no else " " + "_") + "  " + "CH: " + (
                                                str(
                                                    self.chassis_no) + ' - ' if self.chassis_no else " " + "_") + "  " + "EN: " + (
                                                str(
                                                    self.engine) + ' - ' if self.engine else " " + "_") + "   " + (
                                                str(
                                                    self.license_expire_date) + " - " if self.license_expire_date else " " + "_") + "  " + "   " + "Type: " + (
                                                str(
                                                    self.state_type) + " - " if self.state_type else " " + "_")

            if self.type_risk == "cargo":
                self.risk_description_crm = (str(
                    self.cargo_type.setup_id) + " - " if self.cargo_type.setup_id else " " + "_") + "  " + "FRM: " + (
                                                str(self.From) + " - " if self.From else " " + "_") + "   " + "TO: " + (
                                                str(self.To) + " - " if self.To else " " + "_")

            if self.type_risk == "location":
                self.risk_description_crm = (
                                                str(
                                                    self.type.setup_id) if self.type.setup_id else " " + "_") + "  " + "ADD: " + (
                                                str(self.address) + " - " if self.address else " " + "_")

            if self.type_risk == 'Group':
                self.risk_description_crm = (str(
                    self.group_category) + " - " if self.group_category else " " + "_") + "   " + (
                                                str(self.group_count) + " - " if self.group_count else " " + "_")
            if self.type_risk == 'Project':
                self.risk_description_crm = (str(
                    self.proj_name) + " - " if self.proj_name else " " + "_") + "   " + (
                                                str(self.machine_id) + " - " if self.machine_id else " " + "_")
        else:
            self.risk_description_crm = "something is wrong"

    risks_crm = fields.Many2one("crm.lead", string='Risks')
    type_risk = fields.Char(related='risks_crm.test')
    risk_description_crm = fields.Char("Risk Description", compute="_compute_risk_description_crm", store=True)
