from datetime import timedelta, datetime
import base64
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from odoo import api, tools, fields, models, _


class crm_leads(models.Model):
    _inherit = "crm.lead"

    planned_revenue = fields.Float('Expected Premium in Company Currency', track_visibility='always')
    c_type = fields.Many2one('res.currency', string='Expected Premium in Currency',
                             default=lambda self: self.env.user.company_id.currency_id)
    ammount = fields.Float(string='Ammount', digits=(12, 2))
    create_date = fields.Date("created on")

    # user_id = fields.Many2one('res.users', string='Lead Operator', index=True, track_visibility='onchange',
    #                           default=lambda self: self.env.user )
    create_uid = fields.Many2one('res.users', string='Lead Generator')

    # lead_source = fields.Selection([('facebook', 'Facebook'), ('conference', 'Conference'), ('ads', 'Ads')],
    #                                string='Lead Source')
    lead_source = fields.Many2one('insurance.setup', string='Lead Source', domain="[('setup_key','=','leadsource')]")

    create_uid1 = fields.Many2one('res.users', string='Lead Generator',
                                  default=lambda self: self.env['res.users'].search([('id', '=', self.user_id.id)]))

    @api.multi
    def _convert_opportunity_data(self, customer, team_id=False):
        """ Extract the data from a lead to create the opportunity
            :param customer : res.partner record
            :param team_id : identifier of the sales channel to determine the stage
        """
        if self.phone:
            if not team_id:
                team_id = self.team_id.id if self.team_id else False
            value = {
                'planned_revenue': self.planned_revenue,
                'probability': self.probability,
                'name': self.name,
                'partner_id': customer.id if customer else False,
                'type': 'opportunity',
                'date_open': fields.Datetime.now(),
                'email_from': customer and customer.email or self.email_from,
                'phone': customer and customer.phone or self.phone,
                'date_conversion': fields.Datetime.now(),
                'create_uid1': self.create_uid.id,
            }
            if not self.stage_id:
                stage = self._stage_find(team_id=team_id)
                value['stage_id'] = stage.id
                if stage:
                    value['probability'] = stage.probability
            return value
        else:
            raise ValidationError('You have to insert Phone Number !')

    # default = lambda self: self.env['res.users'].search([('id', '=', self.user_id.id)])

    # @api.one
    # @api.depends('user_id', 'create_uid1')
    # def _get_default(self):
    #     self.create_uid1 = self.user_id
    #
    # @api.one
    # def _set_default(self):
    #     if self.create_uid1 == self.user_id:
    #         print(self.user_id)
    #         print(self.create_uid1)
    #         return self._get_default
    #     else:
    #         return True

    policy_number = fields.Char(string='Policy Number')

    discount_party = fields.Many2one('res.partner', 'Discount Party',
                                     domain="[('insurer_type','=', False),('agent','=', False),('customer','=', False),('key_account','=', True)]",
                                     copy=True)
    branch = fields.Many2one('insurance.setup', string="Branch",
                             domain="[('setup_key','=','branch'),('partner_id','=',partner_id)]",
                             copy=True)
    insurance_type = fields.Selection([('Life', 'Life'),
                                       ('General', 'General'),
                                       ('Health', 'Health'), ],
                                      'Insurance Type', track_visibility='onchange', copy=True, default='General')
    ins_type = fields.Selection([('Individual', 'Individual'),
                                 ('Group', 'Group'), ],
                                'I&G', track_visibility='onchange', default='Individual')
    risks_method = fields.Selection([('count', 'Count'),
                                     ('members', 'Members'), ],
                                    'Risk Method', track_visibility='onchange', copy=True)
    duration_no = fields.Integer('Policy Duration Number', default='1')
    duration_type = fields.Selection([('day', 'Day'),
                                      ('month', 'Month'),
                                      ('year', 'Year'), ],
                                     'Policy Duration Type', track_visibility='onchange', default='year')
    term = fields.Char(string='Term', compute='_compute_term', force_save=True)

    validate_basic_mark_opp = fields.Boolean(copy=False, default=True)
    validate_risk_mark_opp = fields.Boolean(copy=False)
    validate_prop = fields.Boolean(copy=False)
    validate_prop_line = fields.Boolean(copy=False)
    validate_commission = fields.Boolean(copy=False)
    validate_underwr = fields.Boolean(copy=False)
    validate_contact = fields.Boolean(copy=False)
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')

    @api.one
    def _compute_attachment_number(self):
        s = self.env['ir.attachment'].search(['&', ('res_id', '=', self.id), ('res_model', '=', 'crm.lead')])
        self.attachment_number = len(s)
        # pass

    @api.multi
    def action_get_attachment_tree_view(self):
        print("ssss")
        action = self.env.ref('base.action_attachment').read()[0]
        action['context'] = {
            'default_res_model': self._name,
            'default_res_id': self.ids[0]
        }
        action['domain'] = ['&', ('res_model', '=', 'crm.lead'), ('res_id', '=', self.id)]
        return action

    @api.multi
    def validate_basic_opp(self):
        self.validate_basic_mark_opp = True
        self.validate_risk_mark_opp = False
        self.validate_prop = False
        self.validate_prop_line = False
        self.validate_underwr = False
        self.validate_contact = False
        self.validate_commission = False
        print(self.validate_basic_mark_opp)

        return True

    @api.one
    @api.constrains('stage_id')
    def stage_id_constraint(self):
        if self.stage_id.name == 'Qualified':
            if not self.partner_id or not self.LOB:
                raise ValidationError('You have to insert Customer and Line of Business')
        elif self.stage_id.name == 'Proposition':
            if not self.partner_id or not self.objectrisks or not self.LOB or not self.proposal_opp:
                raise ValidationError(
                    'You have to insert Customer and Line of Business and Insured and at least one proposal')
        elif self.stage_id.name == 'Won':
            if not self.partner_id or not self.objectrisks or not self.LOB or not self.proposal_opp or not self.selected_coverage or not self.policy_number:
                raise ValidationError(
                    'You have to insert Customer and Line of Business and Insured and at least one proposal and '
                    'Policy number and select the final proposal !')

    @api.multi
    def validate_risk_opp(self):
        if self.LOB:
            self.validate_basic_mark_opp = False
            self.validate_risk_mark_opp = True
            self.validate_prop = False
            self.validate_prop_line = False
            self.validate_underwr = False
            self.validate_contact = False
            self.validate_commission = False
            print(self.validate_basic_mark_opp)
            return True

    @api.multi
    def validate_proposal(self):
        if self.objectrisks:
            self.validate_basic_mark_opp = False
            self.validate_risk_mark_opp = False
            self.validate_prop = True
            self.validate_prop_line = False
            self.validate_underwr = False
            self.validate_contact = False
            self.validate_commission = False
            return True

    @api.multi
    def validate_share_commission(self):
        self.validate_basic_mark_opp = False
        self.validate_risk_mark_opp = False
        self.validate_prop = False
        self.validate_prop_line = False
        self.validate_underwr = False
        self.validate_contact = False
        self.validate_commission = True
        return True

    @api.multi
    def validate_proposal_line(self):
        if self.objectrisks and self.proposal_opp:
            self.validate_basic_mark_opp = False
            self.validate_risk_mark_opp = False
            self.validate_prop = False
            self.validate_prop_line = True
            self.validate_underwr = False
            self.validate_contact = False
            self.validate_commission = False
            return True

    @api.multi
    def validate_underwritting(self):
        self.validate_basic_mark_opp = False
        self.validate_risk_mark_opp = False
        self.validate_prop = False
        self.validate_prop_line = False
        self.validate_underwr = True
        self.validate_contact = False
        self.validate_commission = False

    @api.multi
    def validate_continfo(self):
        self.validate_basic_mark_opp = False
        self.validate_risk_mark_opp = False
        self.validate_prop = False
        self.validate_prop_line = False
        self.validate_underwr = False
        self.validate_contact = True
        self.validate_commission = False

    @api.one
    def _compute_term(self):
        if self.duration_no and self.duration_type:
            self.term = str(self.duration_no) + '-' + str(self.duration_type)

    LOB = fields.Many2one('insurance.line.business', string='Line of business',
                          domain="[('insurance_type','=',insurance_type)]", required=True,
                          default=lambda self: self.env['insurance.line.business'].search(
                              [('line_of_business', '=', 'Motor')]))

    oppor_type = fields.Char(
        string='Opportunity type',
        compute='_changeopp',
        store=False,
        compute_sudo=True,
    )

    # pol=fields.Many2one(related='Policy_type.insured_type' , string='insured type')
    test = fields.Char(compute='_compute_comment')
    group = fields.Boolean('Groups')
    individual = fields.Boolean('Item by Item')
    test1 = fields.Boolean(readonly=True)

    objectrisks = fields.One2many('policy.risk', 'risks_crm', string=' Insured',
                                  copy=True)  # where you are using this fiedl ? in xml
    object_share_com = fields.One2many('share', 'share_commission_crm', string=' Share Commission',
                                       copy=True)
    # objectgroup = fields.One2many('group.group.opp', 'object_group_crm', string='Group')

    proposal_opp = fields.One2many('proposal.opp.bb', 'proposal_crm', string='proposla')

    coverage_line = fields.One2many('coverage.line', 'covers_crm', 'Coverage lines')

    selected_proposal = fields.One2many('proposal.opp.bb', 'select_crm', compute='proposalselected')
    prop_id = fields.Integer('', readonly=True)
    my_notes = fields.Text('Under writting')

    # covers=fields.One2many(related='selected_proposal.proposals_covers')

    # policy_opp=fields.Many2one('policy.broker')
    selected_coverage = fields.Many2one('proposal.opp.bb', domain="[('id','in',proposal_opp)]", string='Final Proposal')
    set_covers = fields.Boolean('')
    test_computed = fields.Char('', compute='testcom')
    btn_invisiable = fields.Boolean()

    @api.multi
    def action_schedule_meeting(self):
        """ Open meeting's calendar view to schedule meeting on current opportunity.
            :return dict: dictionary value for created Meeting view
        """
        self.ensure_one()
        action = self.env.ref('calendar.action_calendar_event').read()[0]
        partner_ids = self.user_id.partner_id.ids
        if self.partner_id:
            partner_ids.append(self.partner_id.id)
        action['context'] = {
            'default_opportunity_id': self.id if self.type == 'opportunity' else False,
            'default_partner_id': self.partner_id.id,
            'default_partner_ids': partner_ids,
            'default_team_id': self.team_id.id,
            'default_name': self.name,
        }
        return action

    @api.model
    def testwer(self, meeting):
        print(555555555555555555555555555555555555555555)
        for lead in meeting:
            lead.partner_ids = [(4, self.user_id.partner_id.id)]
            # lead.write({'partner_ids': [self.user_id.partner_id.id]})
            for rec in lead.partner_ids:
                print(rec.name)

    # @api.multi
    # @api.oncfhange('user_id')
    update_sech = fields.Boolean('')

    @api.onchange('user_id')
    def changesecudule(self):
        if self.user_id:
            self.update_sech = True
            print(self.update_sech)

    def changeattendense(self):
        # print(self._origin.id)
        # print(555555555555555555555555555555555555555555)
        if self.user_id and self.type == 'opportunity':
            self.update_sech = False
            meeting_data = self.env['calendar.event'].search([('opportunity_id', '=', self.id)])
            #     # meeting = self.env['calendar.event'].read_group([('opportunity_id', 'in', self.ids)],
            #     #                                                      ['opportunity_id'], ['opportunity_id'])
            # mapped_data = {m['opportunity_id'][0]: m['opportunity_id_count'] for m in meeting}
            print(meeting_data)
            #
            # value={}
            # print(self.name)
            # self.testwer(meeting_data)
            for lead in meeting_data:
                # self.resolve_2many_commands(lead.partner_ids,[4,self.user_id.partner_id.id])
                lead.partner_ids = [(6, 0, [self.user_id.partner_id.id])]
                for rec in lead.partner_ids:
                    print(rec.name)

                # print (lead.opportunity_id)

            # meeting_data = self.env['calendar.event'].search([('opportunity_id','in',self.ids)])
            # print(meeting_data)
            #
            # for rec in meeting_data:
            #     print(self.user_id.partner_id)
            #     rec.write({'partner_ids': self.user_id.partner_id.ids})

    @api.depends('ins_type')
    def testcom(self):
        self.test_computed = 'Islam'

    def proposalselected(self):
        print('5555555')
        ids = self.env['proposal.opp.bb'].search([('id', '=', self.prop_id)]).ids
        self.selected_proposal = [(6, 0, ids)]

    @api.multi
    def covers_button(self):
        self.set_covers = True
        # self.coverage_line.covers_crm=self.id
        return True
        # form_view = self.env.ref('insurance_broker_system_blackbelts.Risks_form')

        # return {
        #     'name': ('Risk Details'),
        #     'view_type': 'form',
        #     'view_mode': 'form',
        #     'views': [(form_view.id, 'form')],
        #     'res_model': 'risks.opp',
        #     'target': 'inline',
        #     'type': 'ir.actions.act_window',
        #     'context': {'default_risks_crm': self.id},
        #     'flags': {'form': {'action_buttons': True}}

    @api.multi
    def proposal_button(self):
        form_view = self.env.ref('insurance_broker_system_blackbelts.form_proposal_opp')

        return {
            'name': ('Proposals'),
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(form_view.id, 'form')],
            'res_model': 'proposal.opp.bb',
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {'default_proposal_crm': self.id},
        }

    # objectcar_selected = fields.Many2one('car.object', string='car')
    # objectperson_selected = fields.Many2one('person.object', string='Person')
    # objectcargo_selected = fields.Many2one('cargo.object', string='cargo')
    # objectgroup_selected = fields.Many2one('group.group', string='Group')

    # @api.onchange('user_id')
    # def get_car_proposal_crm(self):
    #     for lead in self:
    #         proposal_ids = []
    #         for car in self.objectcar:
    #             if car.btn1:
    #                 proposal_ids = proposal_ids+car.proposal_car.ids
    #         lead.prop_car = [(6,0, proposal_ids)]

    # @api.multi
    # def button_action(self):
    #     return {
    #         'type': 'ir.actions.act_url',
    #         'url': 'http://167.99.243.240/moodle/login/index.php?username=%{0}&password=Admin%40123&Submit=Login' .format(self.env.user.name),
    #         'target': 'self',
    #         'res_id': self.id,
    #     }

    # prop_car=fields.One2many(related='objectcar')
    # prop_person = fields.One2many(related='objectperson_selected.proposal_person')
    # prop_cargo = fields.One2many(related='objectcargo_selected.proposal_cargo')
    # prop_group = fields.One2many(related='objectgroup_selected.proposal_group')

    # @api.multi
    # def create_policy(self):
    #     # self.user_id.partner_id.agent = True
    #     form_view = self.env.ref('smart_policy.policy_form_view')
    #     print('sereheh')
    #     recordrisks = self.env['policy.risk'].search([('id', 'in', self.objectrisks.ids)])
    #     records_risks = []
    #     for rec in recordrisks:
    #         records_risks.append(rec.id)
    #     print(records_risks)
    #
    #     recordproposal = self.env['proposal.opp.bb'].search([('id', '=', self.selected_coverage.id)])
    #     print(recordproposal.id)
    #     recordcovers = self.env['coverage.line'].search([('proposal_id', '=', recordproposal.id)])
    #
    #     records_covers = []
    #     for rec in recordcovers:
    #         coversline = (
    #             0, 0,
    #             {'riskk': rec.risk_id_cover.id, 'insurerd': rec.insurer.id,
    #              'prod_product': rec.product.id, 'name1': rec.covers.id, 'sum_insure': rec.sum_insured,
    #              'deductible': rec.deductible, 'limitone': rec.limitone, 'limittotal': rec.limittotal,
    #              'net_perimum': rec.net_premium, 'rate': rec.rate})
    #         print(coversline)
    #     # records_covers.append(coversline)
    #     print(records_covers)
    #
    # if self.policy_number and self.selected_coverage:
    #     self.btn_invisiable = True
    #     print('eddddeee')
    #     return {
    #         'name': ('Policy'),
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'views': [(form_view.id, 'form')],
    #         'res_model': 'policy.broker',
    #         'target': 'current',
    #         'type': 'ir.actions.act_window',
    #         'context': {
    #             'default_std_id': self.policy_number,
    #             'default_company': self.selected_coverage.Company.id,
    #             'default_ins_type': self.ins_type,
    #             'default_line_of_bussines': self.LOB.id,
    #             'default_product_policy': self.selected_coverage.product_pol.id,
    #             'default_insurance_type': self.insurance_type,
    #             'default_customer': self.partner_id.id,
    #             'default_salesperson': self.user_id.partner_id.id,
    #             'default_new_risk_ids': [(6, 0, records_risks)],
    #             'default_total_sum_insured': recordproposal.total_sum_insured,
    #             'default_rate': recordproposal.rate,
    #             'default_t_permimum': recordproposal.net_premium,
    #             'default_gross_perimum': recordproposal.gross_premium,
    #
    #         },
    #     }
    #     else:
    #         raise ValidationError(
    #             ('You Must Enter the Policy Number '
    #              'OR select final proposal  .'))

    def action_set_won(self):
        share = self.env['share'].search([('id', 'in', self.object_share_com.ids)])
        records_share = []
        for x in share:
            obj = (0, 0, {'sales_person1': x.sales_person1.id,
                          'seller_commission': x.seller_commission,

                          })
            records_share.append(obj)

        risk = self.env["policy.risk"].search([('id', 'in', self.objectrisks.ids)])
        records_risk = []
        for rec in risk:
            object = (0, 0, {'risk_description': rec.risk_description_crm,
                             'car_tybe': rec.car_tybe.id, 'motor_cc': rec.motor_cc, 'year_of_made': rec.year_of_made,
                             'model': rec.model.id, 'Man': rec.Man.id, 'chassis_no': rec.chassis_no,
                             'state_type': rec.state_type,
                             'plate_no': rec.plate_no, 'engine': rec.engine,
                             'name': rec.name, 'DOB': rec.DOB, 'job': rec.job.id, 'family_member': rec.family_member,
                             'From': rec.From, 'To': rec.To, 'cargo_type': rec.cargo_type.id,
                             'address': rec.address, 'type': rec.type.id,
                             'group_category': rec.group_category, 'group_count': rec.group_count,
                             'sum_insured': rec.sum_insured, 'limitone': rec.limitone,
                             'limittotal': rec.limittotal, 'proj_name': rec.proj_name,
                             'machine_id': rec.machine_id,
                             'proj_start_date': rec.proj_start_date,
                             'proj_end_date': rec.proj_end_date, 'proj_description': rec.proj_description,
                             })
            records_risk.append(object)

        recordproposal = self.env['proposal.opp.bb'].search([('id', '=', self.selected_coverage.id)])

        if self.policy_number and self.selected_coverage:

            insured_object = ''
            if self.ins_type == 'Group':
                insured_object = 'Group'
            else:
                insured_object = self.LOB.object
            inv_obj = self.env['policy.broker']
            inv_obj.create({
                'std_id': self.policy_number,
                'company': self.selected_coverage.Company.id,
                'customer': self.partner_id.id,
                'discount_party': self.discount_party.id,
                'ins_type': self.ins_type,
                'check_item': insured_object,
                'line_of_bussines': self.LOB.id,
                'product_policy': self.selected_coverage.product_pol.id,
                'insurance_type': self.insurance_type,
                'sales_person1': self.user_id.id,
                'new_risk_ids': records_risk,
                'sharecommission_policy': records_share,
                'total_sum_insured': self.selected_coverage.total_sum_insured,
                'rate': self.selected_coverage.rate,
                't_permimum': self.selected_coverage.net_premium,
                'gross_perimum': self.selected_coverage.gross_premium,
                'c_type': self.c_type,
                'saleschannel': self.team_id.id,

            })
            self.btn_invisiable = True
            super(crm_leads, self).action_set_won()

        else:
            raise ValidationError(
                ('You Must Enter the Policy Number '
                 'OR select final proposal.'))

    @api.onchange('user_id')
    def meetings_get(self):
        print('bos bos')
        self.ensure_one()
        action = self.env.ref('calendar.action_calendar_event').read()[0]
        partner_ids = self.env.user.partner_id.ids
        if self.partner_id:
            partner_ids.append(self.partner_id.id)
        action['context'] = {
            'default_opportunity_id': self.id if self.type == 'opportunity' else False,
            'default_partner_id': self.partner_id.id,
            'default_partner_ids': partner_ids,
            'default_team_id': self.team_id.id,
            'default_name': self.name,
        }
        print(action)
        meeting_data = self.env['calendar.event'].search([('opportunity_id', '=', self.id)])
        print(meeting_data)
        # meeting_data = self.env['calendar.event'].read_group([('opportunity_id', 'in', self.ids)], ['opportunity_id'],
        #                                                      ['opportunity_id'])
        # mapped_data = {m['opportunity_id'][0]: m['opportunity_id_count'] for m in meeting_data}
        # for lead in self:
        #     lead.meeting_count = mapped_data.get(lead.id, 0)

        # min_date = fields.Datetime.now()
        # max_date = fields.Datetime.to_string(datetime.now() + timedelta(days=8))
        # meetings_domain = [
        #     ('start', '>=', min_date),
        #     ('start', '<=', max_date),
        #     ('partner_ids', 'in', [self.env.user.partner_id.id])
        # ]
        meetings = self.env['calendar.event'].search([])
        # print(mapped_data)

    @api.one
    @api.depends('LOB', 'ins_type')
    def _compute_comment(self):
        if self.ins_type == 'Group':
            self.test = 'Group'
        else:
            self.test = self.LOB.object

    # @api.onchange('user_id')
    # def onchange_user_id(self):
    #   if self.user_id and self.env.uid != 1 :
    #        return {'domain':{'user_id': [('id','in',[self.env.uid,1])]}}

    @api.onchange('user_id', 'create_uid1')
    def _changeopp(self):
        for record in self:
            if record.create_uid1:
                if record.create_uid1 == record.user_id:
                    record['oppor_type'] = 'Own'

                else:
                    record['oppor_type'] = 'Network'
            else:
                record.create_uid1 = self.env.uid

    @api.multi
    def print_opp(self):
        return self.env.ref('Smart_crm.crm_report').report_action(self)

    @api.multi
    def print_opp_insurer(self):
        return self.env.ref('Smart_crm.crm_report_insurer').report_action(self)

    @api.multi
    def send_mail_template(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('Smart_crm.opp_email_template')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'crm.lead',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }
        # You can also find the e-mail template like this:
        # template = self.env['ir.model.data'].get_object('mail_template_demo', 'example_email_template')

        # Send out the e-mail template to the user
        template_id.send_mail(self.ids[0], force_send=True)
        # self.env['mail.template'].browse(template.id).send_mail(self.id)

    @api.multi
    def send_mail_template_insurer(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('Smart_crm.opp_email_template_insurer')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'crm.lead',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }
        # You can also find the e-mail template like this:
        # template = self.env['ir.model.data'].get_object('mail_template_demo', 'example_email_template')

        # Send out the e-mail template to the user
        template_id.send_mail(self.ids[0], force_send=True)
        # self.env['mail.template'].browse(template.id).send_mail(self.id)

    @api.onchange('ammount', 'c_type')
    def _change(self):
        if self.c_type.id:
            self.planned_revenue = self.ammount / self.c_type.rate
            print(self.c_type.rate)
            print(self.user_id.id)

    attachment = fields.Binary(string='Excel File')
    group_ids = fields.One2many('risk.groups', 'crm_id', string='Group Members')

    @api.multi
    def import_excel(self):
        if self.attachment:
            wb = open_workbook(file_contents=base64.decodestring(self.attachment))
            values = []
            for s in wb.sheets():
                for row in range(1, s.nrows):
                    col_value = []
                    for col in range(s.ncols):
                        value = (s.cell(row, col).value)
                        try:
                            value = str(int(value))
                        except:
                            pass
                        col_value.append(value)
                    values.append(col_value)
            return values
        else:
            raise ValidationError('please import your Excel Sheet !')

    @api.multi
    def create_risk(self):
        dict = {}
        for elem in self.import_excel():
            if elem[5] not in dict:
                dict[elem[5]] = []
            dict[elem[5]].append(elem[0:])

        for key, value in dict.items():
            self.update({
                'objectrisks': [(0, 0, {'group_category': key,
                                        'group_count': len(value)})], })

            for item in value:
                self.update({
                    'group_ids': [(0, 0, {'member_id': item[0],
                                          'name': item[1],
                                          'member_payed': item[2],
                                          'sum_insured': item[3],
                                          'group_name': item[5], })], })


class crm_leads_currency(models.Model):
    _inherit = 'res.currency'
    # currency_type=fields.One2many('crm.lead','currency_type ',string='currency')
    c = fields.One2many('crm.lead', 'c_type', string='currency')


class crm_leads_report(models.Model):
    _name = 'lead.report'
    date_from = fields.Date(string="From Date", required=True, default=datetime.today())
    date_to = fields.Date(string="To Date", required=True, default=datetime.today())
    LOB = fields.Many2one('insurance.line.business', string='Line of business',
                          default=lambda self: self.env['insurance.line.business'].search(
                              [('line_of_business', '=', 'Motor')]))
    stage_id = fields.Many2one('crm.stage', string='Stage',
                               domain="['|', ('team_id', '=', False), ('team_id', '=', team_id)]")
    team_id = fields.Many2one(
        'crm.team', 'Sales Channel',
        help='If set, this sales channel will be used for sales and assignations related to this partner')

    @api.multi
    def lead_report(self):
        if self.LOB and self.stage_id:
            active_ids = self.env["crm.lead"].search(
                [('create_date', '>=', self.date_from),
                 ('create_date', '<=', self.date_to),
                 ('LOB', '=', self.LOB.line_of_business),
                 ('stage_id', '=', self.stage_id.id), ('team_id.user_id', '=', self.env.user.id)]).ids
        elif self.LOB and not self.stage_id:
            active_ids = self.env["crm.lead"].search(
                [('create_date', '>=', self.date_from),
                 ('create_date', '<=', self.date_to),
                 ('LOB', '=', self.LOB.line_of_business), ('team_id.user_id', '=', self.env.user.id)]).ids
        elif self.stage_id and not self.LOB:
            active_ids = self.env["crm.lead"].search(
                [('create_date', '>=', self.date_from),
                 ('create_date', '<=', self.date_to),
                 ('stage_id', '=', self.stage_id.id), ('team_id.user_id', '=', self.env.user.id)]).ids
        else:
            active_ids = self.env["crm.lead"].search(
                [('create_date', '>=', self.date_from),
                 ('create_date', '<=', self.date_to), ('team_id.user_id', '=', self.env.user.id)]).ids
        print(active_ids)
        return self.env['crm.lead'].browse(active_ids)

    @api.multi
    def print_lead_report(self):
        return self.env.ref('Smart_crm.lead_team_analysis_report').report_action(self)


class NewModule(models.Model):
    _name = 'assign.salesperson'

    name = fields.Char()


class ShareCommission(models.Model):
    _inherit = "share"
    _rec_name = 'share_commission_crm'

    share_commission_crm = fields.Many2one('crm.lead', string='Share Commission')

# class PivotReportCRM(models.Model):
#     _name = "crm.opportunity.pivot.report"
#     _auto = False
#
#     stage_id = fields.Many2one('crm.stage', string='Stage', readonly=True,
#                                domain="['|', ('team_id', '=', False), ('team_id', '=', team_id)]")
#     lead_source = fields.Many2one('insurance.setup', string='Lead Source', domain="[('setup_key','=','leadsource')]",
#                                   readonly=True)
#
#     def _select(self):
#         select_str = """
#         SELECT
#
#             c.stage_id,
#             c.lead_source,
#         """
#         return select_str
#
#     def _from(self):
#         from_str = """
#             FROM
#                     "crm_lead" c
#         """
#         return from_str
#
#     def _join(self):
#         join_str = """
#             LEFT JOIN "crm_stage" stage ON stage.id = c.stage_id
#         """
#         return join_str
#
#     def _where(self):
#         where_str = """
#         """
#         return where_str
#
#     def _group_by(self):
#         group_by_str = """
#             GROUP BY stage.name
#         """
#         return group_by_str
#
#     @api.model_cr
#     def init(self):
#         tools.drop_view_if_exists(self.env.cr, self._table)
#         self.env.cr.execute("""CREATE VIEW %s AS (
#             %s
#             %s
#             %s
#             %s
#             %s
#         )""" % (self._table, self._select(), self._from(), self._join(), self._where(), self._group_by()))
